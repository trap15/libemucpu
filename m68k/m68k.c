/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "emucpu.h"

#include "m68k.h"
#include "m68kops.h"

EMUCPU_INTERRUPT( m68k )
{
	/* TODO: Implement */
	m68kctx_t* ctx = cpu_ctx->data;
}

EMUCPU_DASM( m68k )
{

}

EMUCPU_EMULATE( m68k )
{
	u16 opcode;
	int runcycles = 0;
	m68kctx_t* ctx = cpu_ctx->data;
	for(cpu_ctx->cycles = cycles; cpu_ctx->cycles > 0; cpu_ctx->cycles -= runcycles) {
		if(cpu_ctx->halt)
			break;
		opcode = ctx->EMUCPU_READ_RUN(read32, ctx->pc++, 0x0000FFFF);
		/* TODO: Implement decoder */
	}
	return cycles - cpu_ctx->cycles;
}

EMUCPU_RESET( m68k )
{
	m68kctx_t* ctx = cpu_ctx->data;
	ctx->pc = 0;
}

EMUCPU_SAVE( m68k )
{
	m68kctx_t* ctx = cpu_ctx->data;
	u8* outdata = calloc(sizeof(m68kctx_t), 1);
	memcpy(outdata, ctx, sizeof(m68kctx_t));
	return outdata;
}

EMUCPU_RESTORE( m68k )
{
	m68kctx_t* ctx = cpu_ctx->data;
	memcpy(ctx, data, sizeof(m68kctx_t));
}

EMUCPU_GET_INFO( m68k )
{
	m68kctx_t* ctx = cpu_ctx->data;
	return EMUCPU_INFO_RET_NO_HANDLE;
}

EMUCPU_SET_INFO( m68k )
{
	m68kctx_t* ctx = cpu_ctx->data;
	return EMUCPU_INFO_RET_NO_HANDLE;
}

EMUCPU_INIT( m68k )
{
	cpu_ctx_t* cpu_ctx;
	m68kctx_t* ctx;
	cpu_ctx = malloc(sizeof(cpu_ctx_t));
	if(cpu_ctx == NULL)
		return NULL;
	ctx = malloc(sizeof(m68kctx_t));
	if(ctx == NULL) {
		free(cpu_ctx);
		return NULL;
	}
	cpu_ctx->data = ctx;
	ctx->EMUCPU_READ_NAME( read32 ) = NULL;
	ctx->EMUCPU_WRITE_NAME(write32) = NULL;
	return cpu_ctx;
}

