/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 The Lemon Man (Giuseppe)
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "emucpu.h"

#include "i4004.h"
#include "i4004ops.h"

EMUCPU_INTERRUPT( i4004 )
{

}

EMUCPU_DASM( i4004 )
{

}

EMUCPU_EMULATE( i4004 )
{
	i4004ctx_t* ctx = cpu_ctx->data;
	
	for(cpu_ctx->cycles = cycles; cpu_ctx->cycles > 0;) {
		if(cpu_ctx->halt)
			break;
		u8 opCode = PIPELINE_FETCH8(ctx);
		cpu_ctx->cycles -= opTbl[opCode].cycles;
		opTbl[opCode].execute(ctx, opCode);
	}
}

EMUCPU_RESET( i4004 )
{
	i4004ctx_t* ctx = cpu_ctx->data;
	memset(&ctx->GPR, 0, sizeof(u8) * 0x10);
	ctx->A = ctx->CARRY = ctx->TEST = 0x00;
	ctx->PC = 0x0000;
}

EMUCPU_SAVE( i4004 )
{
	i4004ctx_t* ctx = cpu_ctx->data;
	u8* outdata = calloc(sizeof(i4004ctx_t), 1);
	memcpy(outdata, ctx, sizeof(i4004ctx_t));
	return outdata;
}

EMUCPU_RESTORE( i4004 )
{
	i4004ctx_t* ctx = cpu_ctx->data;
	memcpy(ctx, data, sizeof(i4004ctx_t));
}

EMUCPU_GET_INFO( i4004 )
{
	i4004ctx_t* ctx = cpu_ctx->data;
	return EMUCPU_INFO_RET_NO_HANDLE;
}

EMUCPU_SET_INFO( i4004 )
{
	i4004ctx_t* ctx = cpu_ctx->data;
	return EMUCPU_INFO_RET_NO_HANDLE;
}

EMUCPU_INIT( i4004 )
{
	cpu_ctx_t* cpu_ctx;
	i4004ctx_t* ctx;
	
	cpu_ctx = malloc(sizeof(cpu_ctx_t));
	
	if(cpu_ctx == NULL)
		return NULL;
		
	ctx = malloc(sizeof(i4004ctx_t));
	
	if(ctx == NULL) {
		free(cpu_ctx);
		return NULL;
	}
	
	ctx->EMUCPU_READ_NAME (read8  ) = NULL;
	ctx->EMUCPU_WRITE_NAME(write8 ) = NULL;
	
	cpu_ctx->data = ctx;
}

