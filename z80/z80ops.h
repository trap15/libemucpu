/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 The Lemon Man (Giuseppe)
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 
 
#ifndef Z80OPS_H
#define Z80OPS_H

#include "emucpu.h"

#include "z80.h"

#define Z80OPCODE(op)				INLINE void i4004op_##op(z80ctx_t* ctx, u8 opcode)
#define z80OPCODE_RUN(op, ctx, code)		i4004op_##op(ctx, code)

#define PIPELINE_FETCH8(ctx)			ctx->EMUCPU_READ_RUN(read8, ctx->PC++, 0xFF)

#define GET_X_INDEXED(ctx,val)			val + ctx->IX
#define GET_Y_INDEXED(ctx,val)			val + ctx->IY

inline u16 getRegister (z80ctx_t* ctx, int index, int paired, int lastSP)
{
	switch (index) {
		case 0:
			return (paired) ? ctx->BC : ctx->B;
		case 1:
			return (paired) ? ctx->DE : ctx->C;
		case 2:
			return (paired) ? ctx->HL : ctx->D;
		case 3:
			return (paired) ? ((lastSP) ? ctx->SP : ctx->AF) : ctx->E;
		case 4:
			return ctx->H;
		case 5:
			return ctx->L;			
		case 6:
			return ctx->A;
		default:
			return 0xFF;
	}
}

inline u16 setRegister (z80ctx_t* ctx, int index, u16 value, int paired, int lastSP)
{
	switch (index) {
		case 0:
			if (paired) {
				ctx->BC = value;
			} else {
				ctx->B  = value & 0xFF;
			}
		case 1:
			if (paired) {
				ctx->DE = value;
			} else {
				ctx->C  = value & 0xFF;
			}
		case 2:
			if (paired) {
				ctx->HL = value;
			} else {
				ctx->D  = value & 0xFF;
			}
		case 3:
			if (paired) {
				if (lastSP) {
					ctx->SP = value;
				} else {
					ctx->AF = value;
				}
			} else {
				ctx->E  = value & 0xFF;
			}
		case 4:
			ctx->H = value & 0xFF;			
		case 5:
			ctx->L = value & 0xFF;	
		case 6:
			ctx->A = value & 0xFF;	
		default:
			return 0xFF;
	}
}

#define FALSE 0
#define TRUE  1

Z80OPCODE ( LD_REG_REG )
{
	u8 tmp = getRegister(ctx, opcode & 0x7, FALSE, FALSE);
	setRegister(ctx, (opcode >> 3) & 0x7, tmp, FALSE, FALSE);
}

Z80OPCODE ( LD_REG_IMM )
{
	setRegister(ctx, (opcode >> 3) & 0x7, PIPELINE_FETCH8(ctx), FALSE, FALSE);
}

Z80OPCODE ( LD_REG_MEM )
{
	setRegister(ctx, (opcode >> 3) & 0x7, ctx->EMUCPU_READ_RUN(read8, ctx->HL, 0xFF), FALSE, FALSE);
}

Z80OPCODE ( LD_REG_MEM_IX )
{
	setRegister(ctx, (opcode >> 3) & 0x7, GET_X_INDEXED(ctx, ctx->EMUCPU_READ_RUN(read8, ctx->HL, 0xFF)), FALSE, FALSE);
}

Z80OPCODE ( LD_REG_MEM_IY )
{
	setRegister(ctx, (opcode >> 3) & 0x7, GET_Y_INDEXED(ctx, ctx->EMUCPU_READ_RUN(read8, ctx->HL, 0xFF)), FALSE, FALSE);
}

Z80OPCODE ( LD_MEM_REG )
{
	ctx->EMUCPU_WRITE_RUN(write8, ctx->HL, getRegister(ctx, opcode & 0x7, FALSE, FALSE), 0xFF);
}

Z80OPCODE ( LD_MEM_IX_REG )
{
	ctx->EMUCPU_WRITE_RUN(write8, GET_X_INDEXED(ctx, PIPELINE_FETCH8(ctx)), (opcode >> 3) & 0x7, 0xFF);
}

Z80OPCODE ( LD_MEM_IY_REG )
{
	ctx->EMUCPU_WRITE_RUN(write8, GET_Y_INDEXED(ctx, PIPELINE_FETCH8(ctx)), (opcode >> 3) & 0x7, 0xFF);
}

#endif

