/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 TheLemonMan (Giuseppe)
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef _LIBEMU_Z80_H
#define _LIBEMU_Z80_H

#include "emucpu.h"

typedef struct {
	union {
		struct {
			u8 F;
			u8 A;
			u8 C;
			u8 B;
			u8 E;
			u8 D;
			u8 L;
			u8 H;
		};
		struct {
			u16 AF;
			u16 BC;
			u16 DE;
			u16 HL;
		};
	};
	
	u8 I;
	
	u16 IX;
	u16 IY;
	
	u16 PC;
	u16 SP;
	
	EMUCPU_READ_CB (portIn,    8, 8);
	EMUCPU_WRITE_CB(portOut,   8, 8);	
	EMUCPU_READ_CB (read8,     8, 16);
	EMUCPU_WRITE_CB(write8,    8, 16);	
} z80ctx_t;

EMUCPU_EMULATE( z80 );
EMUCPU_INTERRUPT( z80 );
EMUCPU_RESET( z80 );
EMUCPU_INIT( z80 );
EMUCPU_SAVE( z80 );
EMUCPU_RESTORE( z80 );
EMUCPU_GET_INFO( z80 );
EMUCPU_SET_INFO( z80 );
EMUCPU_DASM( z80 );

#endif

