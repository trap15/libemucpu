FULLMAKE	:= 1
OBJECTS		:= 
OUTLIBS		:= 
include Makeinfo
include i8080/Makeinfo
include m68k/Makeinfo
include avr/Makeinfo
include score/Makeinfo
include i4004/Makeinfo
include z80/Makeinfo
include v850/Makeinfo

.PHONY: all
all: $(OUTLIBS)

.PHONY: clean
clean:
	$(RM) $(OUTLIBS) $(OBJECTS)

%.o: %.c
	$(CC) $(CFLAGS) $< -c -o $@
%.o: %.s
	$(AS) $(SFLAGS) $< -o $@

include i8080/Makefile
include m68k/Makefile
include avr/Makefile
include score/Makefile
include i4004/Makefile
include z80/Makefile
include v850/Makefile

