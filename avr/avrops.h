/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef AVROPS_H
#define AVROPS_H

#include "emucpu.h"

#include "avr.h"

#define AVROPCODE(op)	INLINE void avrop_##op(avrctx_t* ctx, u16 opcode)
#define AVROPCODE_RUN(op, ctx, code)	avrop_##op(ctx, code)

#define FLAG_I	(1 << 7)	/* Interrupt Enable */
#define FLAG_T	(1 << 6)	/* T bit */
#define FLAG_H	(1 << 5)	/* Half Carry */
#define FLAG_S	(1 << 4)	/* Sign */
#define FLAG_V	(1 << 3)	/* Overflow */
#define FLAG_N	(1 << 2)	/* Negative */
#define FLAG_Z	(1 << 1)	/* Zero */
#define FLAG_C	(1 << 0)	/* Carry */

AVROPCODE( ADC )
{
	int rd, rr;
	u16 res;
	s16 sres;
	rd = (opcode >> 4) & 0x1F;
	rr = (opcode & 0xF) | ((opcode >> 5) & 0x10);
	res = ctx->r.r[rd] + ctx->r.r[rr] + (ctx->sreg & FLAG_C);
	sres = (s8)ctx->r.r[rd] + (s8)ctx->r.r[rr] + (s8)(ctx->sreg & FLAG_C);
	ctx->sreg &= ~(FLAG_H | FLAG_S | FLAG_V | FLAG_N | FLAG_Z | FLAG_C);
	if((res % 0x100) & 0x80)
		ctx->sreg |= FLAG_N;
	if(res == 0)
		ctx->sreg |= FLAG_Z;
	if(res & 0x100)
		ctx->sreg |= FLAG_C;
	if((ctx->r.r[rd] & 0xF) > (res & 0xF))
		ctx->sreg |= FLAG_H;
	if((sres < -128) || (sres > 127))
		ctx->sreg |= FLAG_V;
	if((ctx->sreg & FLAG_N) ^ (ctx->sreg & FLAG_V))
		ctx->sreg |= FLAG_S;
	ctx->r.r[rd] = res;
}

AVROPCODE( ADD )
{
	int rd, rr;
	u16 res;
	s16 sres;
	rd = (opcode >> 4) & 0x1F;
	rr = (opcode & 0xF) | ((opcode >> 5) & 0x10);
	res = ctx->r.r[rd] + ctx->r.r[rr];
	sres = (s8)ctx->r.r[rd] + (s8)ctx->r.r[rr];
	ctx->sreg &= ~(FLAG_H | FLAG_S | FLAG_V | FLAG_N | FLAG_Z | FLAG_C);
	if((res % 0x100) & 0x80)
		ctx->sreg |= FLAG_N;
	if(res == 0)
		ctx->sreg |= FLAG_Z;
	if(res & 0x100)
		ctx->sreg |= FLAG_C;
	if((ctx->r.r[rd] & 0xF) > (res & 0xF))
		ctx->sreg |= FLAG_H;
	if((sres < -128) || (sres > 127))
		ctx->sreg |= FLAG_V;
	if((ctx->sreg & FLAG_N) ^ (ctx->sreg & FLAG_V))
		ctx->sreg |= FLAG_S;
	ctx->r.r[rd] = res;
}

AVROPCODE( ADIW )
{
	int rd;
	u16 imm, src, res;
	rd = (opcode >> 4) & 0x3;
	imm = (opcode & 0xF) | ((opcode >> 2) & 0x30);
	src = ctx->r.r[rd * 2 + 24] | (ctx->r.r[rd * 2 + 25] << 8);
	res = src + imm;
	ctx->sreg &= ~(FLAG_S | FLAG_V | FLAG_N | FLAG_Z | FLAG_C);
	if((res % 0x80) & 0x8000)
		ctx->sreg |= FLAG_N;
	if(res == 0)
		ctx->sreg |= FLAG_Z;
	if((src & 0x8000) && !(res & 0x8000))
		ctx->sreg |= FLAG_C;
	if(!(src & 0x8000) && (res & 0x8000))
		ctx->sreg |= FLAG_V;	
	if((ctx->sreg & FLAG_N) ^ (ctx->sreg & FLAG_V))
		ctx->sreg |= FLAG_S;
	ctx->r.r[rd * 2 + 24] = res & 0xFF;
	ctx->r.r[rd * 2 + 25] = (res >> 8) & 0xFF;
}

AVROPCODE( AND )
{
	int rd, rr;
	u8 res;
	rd = (opcode >> 4) & 0x1F;
	rr = (opcode & 0xF) | ((opcode >> 5) & 0x10);
	res = ctx->r.r[rd] & ctx->r.r[rr];
	ctx->sreg &= ~(FLAG_S | FLAG_V | FLAG_N | FLAG_Z);
	if(res & 0x80)
		ctx->sreg |= FLAG_N;
	if(res == 0)
		ctx->sreg |= FLAG_Z;
	if((ctx->sreg & FLAG_N) ^ (ctx->sreg & FLAG_V))
		ctx->sreg |= FLAG_S;
	ctx->r.r[rd] = res;
}

AVROPCODE( ANDI )
{
	int rd;
	u8 res, imm;
	rd = (opcode >> 4) & 0xF;
	imm = (opcode & 0xF) | ((opcode >> 4) & 0xF0);
	res = ctx->r.r[rd + 16] & imm;
	ctx->sreg &= ~(FLAG_S | FLAG_V | FLAG_N | FLAG_Z);
	if(res & 0x80)
		ctx->sreg |= FLAG_N;
	if(res == 0)
		ctx->sreg |= FLAG_Z;
	if((ctx->sreg & FLAG_N) ^ (ctx->sreg & FLAG_V))
		ctx->sreg |= FLAG_S;
	ctx->r.r[rd + 16] = res;
}

AVROPCODE( ASR )
{
	int rd;
	u8 res;
	rd = (opcode & 0xF) | ((opcode >> 5) & 0x10);
	res = (ctx->r.r[rd] & 0x80) | (ctx->r.r[rd] >> 1);
	ctx->sreg &= ~(FLAG_S | FLAG_V | FLAG_N | FLAG_Z | FLAG_C);
	ctx->sreg |= ctx->r.r[rd] & 1;	/* For Carry */
	if(res & 0x80)
		ctx->sreg |= FLAG_N;
	if(res == 0)
		ctx->sreg |= FLAG_Z;
	if((ctx->sreg & FLAG_N) ^ (ctx->sreg & FLAG_C))
		ctx->sreg |= FLAG_V;
	if((ctx->sreg & FLAG_N) ^ (ctx->sreg & FLAG_V))
		ctx->sreg |= FLAG_S;
	ctx->r.r[rd] = res;
}

AVROPCODE( BCLR )
{
	int flg;
	flg = (opcode >> 4) & 0x7;
	ctx->sreg &= ~(1 << flg);
}

typedef struct 
{
	void	(*execute)(avrctx_t* ctx, u16 opcode);
	u16	mask;
	u16	value;
	int	cycles;
} avrops_t;

#define AVRENTRY(op, mask, value, cyc) { avrop_##op , mask, value, cyc }
static avrops_t srcTbl[0x100] = {
AVRENTRY(ADC,		0xFC00, 0x1C00, 1 ),
AVRENTRY(ADD,		0xFC00, 0x0C00, 1 ),
AVRENTRY(ADIW,		0xFF00, 0x9600, 2 ),
AVRENTRY(AND,		0xFC00, 0x2000, 1 ),
AVRENTRY(ANDI,		0xF000, 0x7000, 1 ),
AVRENTRY(ASR,		0xFE0F, 0x9405, 1 ),
AVRENTRY(BCLR,		0xFF0F, 0x9408, 1 ),
#if 0
AVRENTRY(BLD,		0xFE08, 0xF800, 1 ),
AVRENTRY(BRBC,		0xFC00, 0xF400, 1 ),
AVRENTRY(BRBS,		0xFC00, 0xF000, 1 ),
AVRENTRY(BREAK,		0xFFFF, 0x9598, 1 ),
AVRENTRY(BSET,		0xFF8F, 0x9408, 1 ),
AVRENTRY(BST,		0xFE08, 0xFA00, 1 ),
AVRENTRY(CALL,		0xFE0E, 0x940E, 4 ),
AVRENTRY(CBI,		0xFF00, 0x9800, 1 ),
AVRENTRY(CLC,		0xFFFF, 0x9488, 1 ),
AVRENTRY(COM,		0xFE0F, 0x9400, 1 ),
AVRENTRY(CP,		0xFC00, 0x1400, 1 ),
AVRENTRY(CPC,		0xFC00, 0x0400, 1 ),
AVRENTRY(CPI,		0xF000, 0x3000, 1 ),
AVRENTRY(CPSE,		0xFC00, 0x1000, 1 ),
AVRENTRY(DEC,		0xFE0F, 0x940A, 1 ),
AVRENTRY(DES,		0xFF0F, 0x940B, 1 ),
AVRENTRY(EICALL,	0xFFFF, 0x9519, 4 ),
AVRENTRY(EIJMP,		0xFFFF, 0x9419, 2 ),
AVRENTRY(ELPM,		0xFFFF, 0x95D8, 3 ),
AVRENTRY(ELPM,		0xFE0F, 0x9006, 3 ),
AVRENTRY(ELPM,		0xFE0F, 0x9007, 3 ),
AVRENTRY(EOR,		0xFC00, 0x2400, 1 ),
AVRENTRY(FMUL,		0xFF88, 0x0308, 2 ),
AVRENTRY(FMULS,		0xFF88, 0x0380, 2 ),
AVRENTRY(FMULSU,	0xFF88, 0x0388, 2 ),
AVRENTRY(ICALL,		0xFFFF, 0x9509, 3 ),
AVRENTRY(IJMP,		0xFFFF, 0x9409, 2 ),
AVRENTRY(IN,		0xF800, 0xB000, 1 ),
AVRENTRY(INC,		0xFE0F, 0x9403, 1 ),
AVRENTRY(JMP,		0xFE0E, 0x940C, 3 ),
AVRENTRY(LD,		0xFE0F, 0x900C, 1 ),
AVRENTRY(LD,		0xFE0F, 0x900D, 2 ),
AVRENTRY(LD,		0xFE0F, 0x900E, 3 ),
AVRENTRY(LDD,		0xFE0F, 0x8008, 1 ),
AVRENTRY(LDD,		0xFE0F, 0x9009, 2 ),
AVRENTRY(LDD,		0xFE0F, 0x900A, 3 ),
AVRENTRY(LDD,		0xFE0F, 0x8000, 1 ),
AVRENTRY(LDD,		0xFE0F, 0x9001, 2 ),
AVRENTRY(LDD,		0xFE0F, 0x9002, 3 ),
AVRENTRY(LDI,		0xF000, 0xE000, 1 ),
AVRENTRY(LDS,		0xFE0F, 0x9000, 2 ),
AVRENTRY(LDS16,		0xF800, 0xA000, 1 ),
AVRENTRY(LPM,		0xFFFF, 0x95C8, 3 ),
AVRENTRY(LPM,		0xFE0F, 0x9004, 3 ),
AVRENTRY(LPM,		0xFE0F, 0x9005, 3 ),
AVRENTRY(LSL,		0xFC00, 0x0C00, 1 ),
AVRENTRY(LSR,		0xFE0F, 0x9406, 1 ),
AVRENTRY(MOV,		0xFC00, 0x2C00, 1 ),
AVRENTRY(MOVW,		0xFF00, 0x0100, 1 ),
AVRENTRY(MUL,		0xFC00, 0x9C00, 2 ),
AVRENTRY(MULS,		0xFF00, 0x0200, 2 ),
AVRENTRY(MULSU,		0xFF88, 0x0300, 2 ),
AVRENTRY(NEG,		0xFE0F, 0x9401, 1 ),
AVRENTRY(NOP,		0xFFFF, 0x0000, 1 ),
AVRENTRY(OR,		0xFC00, 0x2801, 1 ),
AVRENTRY(ORI,		0xF000, 0x6000, 1 ),
AVRENTRY(OUT,		0xF800, 0xB800, 1 ),
AVRENTRY(POP,		0xFE0F, 0x900F, 2 ),
AVRENTRY(PUSH,		0xFE0F, 0x920F, 2 ),
AVRENTRY(RCALL,		0xF000, 0xD000, 3 ),
AVRENTRY(RET,		0xFFFF, 0x9508, 4 ),
AVRENTRY(RETI,		0xFFFF, 0x9518, 4 ),
AVRENTRY(RJMP,		0xF000, 0xC000, 2 ),
AVRENTRY(ROL,		0xFC00, 0x1C00, 1 ),
AVRENTRY(ROR,		0xFE0F, 0x9407, 1 ),
AVRENTRY(SBC,		0xFC00, 0x0800, 1 ),
AVRENTRY(SBCI,		0xF000, 0x4000, 1 ),
AVRENTRY(SBI,		0xFF00, 0x9A00, 2 ),
AVRENTRY(SBIC,		0xFF00, 0x9900, 1 ),
AVRENTRY(SBIS,		0xFF00, 0x9B00, 1 ),
AVRENTRY(SBIW,		0xFF00, 0x9700, 2 ),
AVRENTRY(SBR,		0xF000, 0x6000, 1 ),
AVRENTRY(SBRC,		0xFE08, 0xFC00, 1 ),
AVRENTRY(SBRS,		0xFE08, 0xFE00, 1 ),
AVRENTRY(SLEEP,		0xFFFF, 0x9588, 1 ),
AVRENTRY(SPM,		0xFFEF, 0x95E8, 0 ),
AVRENTRY(ST,		0xFE0F, 0x920C, 1 ),
AVRENTRY(ST,		0xFE0F, 0x920D, 1 ),
AVRENTRY(ST,		0xFE0F, 0x920E, 2 ),
AVRENTRY(STD,		0xFE0F, 0x8208, 1 ),
AVRENTRY(STD,		0xFE0F, 0x9209, 1 ),
AVRENTRY(STD,		0xFE0F, 0x920A, 2 ),
AVRENTRY(STD,		0xD208, 0x8208, 2 ),
AVRENTRY(STD,		0xFE0F, 0x8200, 1 ),
AVRENTRY(STD,		0xFE0F, 0x9201, 1 ),
AVRENTRY(STD,		0xFE0F, 0x9202, 2 ),
AVRENTRY(STD,		0xD208, 0x8200, 2 ),
AVRENTRY(STS,		0xFE0F, 0x9200, 2 ),
AVRENTRY(STS16,		0xF800, 0xA800, 1 ),
AVRENTRY(SUB,		0xFC00, 0x1800, 1 ),
AVRENTRY(SUBI,		0xF000, 0x5000, 1 ),
AVRENTRY(SWAP,		0xFE0F, 0x9402, 1 ),
AVRENTRY(TST,		0xFC00, 0x2000, 1 ),
AVRENTRY(WDR,		0xFFFF, 0x95A8, 1 ),
#endif
};

#endif

