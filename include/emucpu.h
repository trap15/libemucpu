/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef _LIBEMUCPU_H_
#define _LIBEMUCPU_H_

#include <stdint.h>

typedef uint8_t		u8;
typedef uint16_t	u16;
typedef uint32_t	u32;
typedef uint64_t	u64;
typedef int8_t		s8;
typedef int16_t		s16;
typedef int32_t		s32;
typedef int64_t		s64;

typedef struct {
	void*	ptr;
	s64	i;
} cpu_info_t;

#define EMUCPU_INFO_RET_SUCCESS			(0)
#define EMUCPU_INFO_RET_NO_HANDLE		(1)
#define EMUCPU_INFO_RET_ERROR			(2)

#define EMUCPU_EMULATE_NAME(core)		emucpu_emulate_##core
#define EMUCPU_EMULATE(core)			int EMUCPU_EMULATE_NAME(core)(cpu_ctx_t* cpu_ctx, int cycles)
#define EMUCPU_EMULATE_RUN(core, ctx, cyc)	EMUCPU_EMULATE_NAME(core)(ctx, cyc)

#define EMUCPU_DASM_NAME(core)			emucpu_dasm_##core
#define EMUCPU_DASM(core)			int EMUCPU_DASM_NAME(core)(cpu_ctx_t* cpu_ctx, char *buf, u64 pc)
#define EMUCPU_DASM_RUN(core, ctx, buf, pc)	EMUCPU_DASM_NAME(core)(ctx, buf, pc)

#define EMUCPU_INTERRUPT_NAME(core)		emucpu_interrupt_##core
#define EMUCPU_INTERRUPT(core)			void EMUCPU_INTERRUPT_NAME(core)(cpu_ctx_t* cpu_ctx, u64 trap)
#define EMUCPU_INTERRUPT_RUN(core, ctx, trap)	EMUCPU_INTERRUPT_NAME(core)(ctx, trap)

#define EMUCPU_RESET_NAME(core)			emucpu_reset_##core
#define EMUCPU_RESET(core)			void EMUCPU_RESET_NAME(core)(cpu_ctx_t* cpu_ctx)
#define EMUCPU_RESET_RUN(core, ctx)		EMUCPU_RESET_NAME(core)(ctx)

#define EMUCPU_INIT_NAME(core)			emucpu_init_##core
#define EMUCPU_INIT(core)			cpu_ctx_t* EMUCPU_INIT_NAME(core)()
#define EMUCPU_INIT_RUN(core)			EMUCPU_INIT_NAME(core)()

#define EMUCPU_SAVE_NAME(core)			emucpu_save_##core
#define EMUCPU_SAVE(core)			u8* EMUCPU_SAVE_NAME(core)(cpu_ctx_t* cpu_ctx)
#define EMUCPU_SAVE_RUN(core, ctx)		EMUCPU_SAVE_NAME(core)(ctx)

#define EMUCPU_RESTORE_NAME(core)		emucpu_restore_##core
#define EMUCPU_RESTORE(core)			void EMUCPU_RESTORE_NAME(core)(cpu_ctx_t* cpu_ctx, u8* data)
#define EMUCPU_RESTORE_RUN(core, ctx, data)	EMUCPU_RESTORE_NAME(core)(ctx, data)

#define EMUCPU_GET_INFO_NAME(core)		emucpu_get_info_##core
#define EMUCPU_GET_INFO(core)			int EMUCPU_GET_INFO_NAME(core)(cpu_ctx_t* cpu_ctx, int type, cpu_info_t* info)
#define EMUCPU_GET_INFO_RUN(core, ctx, t, nfo)	EMUCPU_GET_INFO_NAME(core)(ctx, t, nfo)

#define EMUCPU_SET_INFO_NAME(core)		emucpu_set_info_##core
#define EMUCPU_SET_INFO(core)			int EMUCPU_SET_INFO_NAME(core)(cpu_ctx_t* cpu_ctx, int type, cpu_info_t info)
#define EMUCPU_SET_INFO_RUN(core, ctx, t, nfo)	EMUCPU_SET_INFO_NAME(core)(ctx, t, nfo)

#define EMUCPU_INFO_TYPE_REG_COUNT		(0x001)
#define EMUCPU_INFO_TYPE_REG_SIZE		(0x002)
#define EMUCPU_INFO_TYPE_ENDIANNESS		(0x003) /* 0 for big-endian, 1 for little */
#define EMUCPU_INFO_TYPE_NAME			(0x004)
#define EMUCPU_INFO_TYPE_FAMILY			(0x005)
#define EMUCPU_INFO_TYPE_VERSION		(0x006)
#define EMUCPU_INFO_TYPE_CREDITS		(0x007)
#define EMUCPU_INFO_TYPE_REG_VALUE		(0x008) /* Lasts for 0x100 entries */
#define EMUCPU_INFO_TYPE_REG_NAME		(0x108) /* Lasts for 0x100 entries */

/* ds is Data Size. This is how big the data is in bits (8, 16, 32, and 64 are usable)
 * as is Address Size. This is how big the address space is in bits (same values as ds)
 */

#define EMUCPU_READ_NAME(name)			emucpu_read_##name
#define EMUCPU_READ(name, ds, as)		u##ds EMUCPU_READ_NAME(name)(u##as addr, u##ds mask)
#define EMUCPU_READ_CB(name, ds, as)		u##ds (*EMUCPU_READ_NAME(name))(u##as addr, u##ds mask)
#define EMUCPU_READ_RUN(name, addr, mask)	EMUCPU_READ_NAME(name)(addr, mask)

#define EMUCPU_WRITE_NAME(name)			emucpu_write_##name
#define EMUCPU_WRITE(name, ds, as)		void EMUCPU_WRITE_NAME(name)(u##as addr, u##ds val, u##ds mask)
#define EMUCPU_WRITE_CB(name, ds, as)		void (*EMUCPU_WRITE_NAME(name))(u##as addr, u##ds val, u##ds mask)
#define EMUCPU_WRITE_RUN(name, a, v, mask)	EMUCPU_WRITE_NAME(name)(a, v, mask)

#ifndef INLINE
#define INLINE					static inline
#endif

/* Only works with GCC. I should probably make this more portable :S */
#ifndef BIG_ENDIAN
#define BIG_ENDIAN				__BIG_ENDIAN__
#endif
#ifndef LITTLE_ENDIAN
#define LITTLE_ENDIAN				__LITTLE_ENDIAN__
#endif

/* Little hack so we can specify binary. 4bits. */
#define Ob4(x)					(((0##x & 01)) | \
						 ((0##x & 010) >> 2) | \
						 ((0##x & 0100) >> 4) | \
						 ((0##x & 01000) >> 6))

/* Larger multi-chunk binary thing. 4 chunks of 4bits = 16bits*/
#define Ob8(x1,x2)				((Ob4(x1) <<  4) | (Ob4(x2) <<  0))
#define Ob16(x1,x2,x3,x4)			((Ob4(x1) << 12) | (Ob4(x2) <<  8) | \
						 (Ob4(x3) <<  4) | (Ob4(x4) <<  0))
#define Ob32(x1,x2,x3,x4,x5,x6,x7,x8)		((Ob4(x1) << 28) | (Ob4(x2) << 24) | \
						 (Ob4(x3) << 20) | (Ob4(x4) << 16) | \
						 (Ob4(x5) << 12) | (Ob4(x6) <<  8) | \
						 (Ob4(x7) <<  4) | (Ob4(x8) <<  0))
#define Ob(x1,x2,x3,x4)				(Ob16(x1,x2,x3,x4))

/* Fast exchange macro */
#define FAST_EXCHANGE(a, b) do { \
	if(a == b) \
		break; \
	a ^= b; \
	b ^= a; \
	a ^ b; \
} while(0)

#define INRANGE(x, y, z)	((y <= x) && (x < (y + z)))

typedef struct {
	s32		cycles; /* Cycles left */
	u8		halt;
	void*		data;
} cpu_ctx_t;

#endif

