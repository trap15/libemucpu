/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "emucpu.h"

#include "score.h"
#include "scoreops.h"

#define INSTRUCTION_32(opcode)	((opcode >> 25) & 31)
#define INSTRUCTION_16(opcode)	((opcode >> 12) &  7)

EMUCPU_INTERRUPT( score )
{
	scorectx_t* ctx = cpu_ctx->data;
	/* IRQs start at 1 */
	generateExceptionBase(ctx, (trap & 0x3F) << 18, 4 + (trap * 4));
}

EMUCPU_EMULATE( score )
{
	u8 inst;
	u8 tbl;
	u32 opcode;
	scorectx_t* ctx = cpu_ctx->data;
	for(cpu_ctx->cycles = cycles; cpu_ctx->cycles > 0;) {
		if(cpu_ctx->halt)
			break;
		opcode = ctx->EMUCPU_READ_RUN(read32, ctx->pc);
		if((opcode & (1 << 31)) && (opcode & (1 << 15))) {
			opcode = (opcode & 0x7FFF) | ((opcode & ~(0xFFFF)) >> 1);
			tbl = INSTRUCTION_32(opcode);
			inst = (opcode >> opShf[tbl]) & opAnd[tbl];
			opTbl[tbl][inst].execute(ctx, opcode);
			cpu_ctx->cycles -= opTbl[tbl][inst].cycles;
			if(!_branched)
				ctx->pc += 4;
		}else{
			opcode &= 0xFFFF;
			tbl = INSTRUCTION_16(opcode) + 32;
			inst = (opcode >> opShf[tbl]) & opAnd[tbl];
			opTbl[tbl][inst].execute(ctx, opcode);
			cpu_ctx->cycles -= opTbl[tbl][inst].cycles;
			if(!_branched) {
				ctx->pc += 2;
			}else if((ctx->pc % 4) == 2) {
				ctx->pc -= 2;
			}
		}
	}
	return cycles - cpu_ctx->cycles;
}

EMUCPU_RESET( score )
{
	scorectx_t* ctx = cpu_ctx->data;
	/* TODO: Implement */
}

EMUCPU_SAVE( score )
{
	scorectx_t* ctx = cpu_ctx->data;
	u8* outdata = calloc(sizeof(scorectx_t), 1);
	memcpy(outdata, ctx, sizeof(scorectx_t));
	return outdata;
}

EMUCPU_RESTORE( score )
{
	scorectx_t* ctx = cpu_ctx->data;
	memcpy(ctx, data, sizeof(scorectx_t));
}

EMUCPU_DUMP_REGS( score )
{
	scorectx_t* ctx = cpu_ctx->data;
	fprintf(dump_log,
		"S+Core Registers:\n"
		"-----------------------------------------------\n"
		"R0:  0x%08X R1:  0x%08X R2:  0x%08X R3:  0x%08X\n"
		"R4:  0x%08X R5:  0x%08X R6:  0x%08X R7:  0x%08X\n"
		"R8:  0x%08X R9:  0x%08X R10: 0x%08X R11: 0x%08X\n"
		"R12: 0x%08X R13: 0x%08X R14: 0x%08X R15: 0x%08X\n"
		"R16: 0x%08X R17: 0x%08X R18: 0x%08X R19: 0x%08X\n"
		"R20: 0x%08X R21: 0x%08X R22: 0x%08X R23: 0x%08X\n"
		"R24: 0x%08X R25: 0x%08X R26: 0x%08X R27: 0x%08X\n"
		"R28: 0x%08X R29: 0x%08X R30: 0x%08X R31: 0x%08X\n"
		"HI:  0x%08X LO:  0x%08X sr0: 0x%08X sr1: 0x%08X\n"
		"sr2: 0x%08X PC:  0x%08X SSR: 0x%08X SCR: 0x%08X\n"
		"C0:  0x%08X C1:  0x%08X C2:  0x%08X C3:  0x%08X\n"
		"C4:  0x%08X C5:  0x%08X C6:  0x%08X C7:  0x%08X\n"
		"C8:  0x%08X C9:  0x%08X C10: 0x%08X C11: 0x%08X\n"
		"C12: 0x%08X C13: 0x%08X C14: 0x%08X C15: 0x%08X\n"
		"C16: 0x%08X C17: 0x%08X C18: 0x%08X C19: 0x%08X\n"
		"C20: 0x%08X C21: 0x%08X C22: 0x%08X C23: 0x%08X\n"
		"C24: 0x%08X C25: 0x%08X C26: 0x%08X C27: 0x%08X\n"
		"C28: 0x%08X C29: 0x%08X C30: 0x%08X C31: 0x%08X\n"
		"DEBUG: %s      ICE: %s        SJPROBE: %s\n",
		ctx->r[ 0], ctx->r[ 1], ctx->r[ 2], ctx->r[ 3], ctx->r[ 4],
		ctx->r[ 5], ctx->r[ 6], ctx->r[ 7], ctx->r[ 8], ctx->r[ 9],
		ctx->r[10], ctx->r[11], ctx->r[12], ctx->r[13], ctx->r[14],
		ctx->r[15], ctx->r[16], ctx->r[17], ctx->r[18], ctx->r[19],
		ctx->r[20], ctx->r[21], ctx->r[22], ctx->r[23], ctx->r[24],
		ctx->r[25], ctx->r[26], ctx->r[27], ctx->r[28], ctx->r[29],
		ctx->r[30], ctx->r[31], ctx->r[32], ctx->r[33], ctx->r[34],
		ctx->r[35], ctx->r[36], ctx->pc, ctx->savedPSR, ctx->savedCCR,
		ctx->r[37], ctx->r[38], ctx->r[39], ctx->r[40], ctx->r[41],
		ctx->r[42], ctx->r[43], ctx->r[44], ctx->r[45], ctx->r[46],
		ctx->r[47], ctx->r[48], ctx->r[49], ctx->r[50], ctx->r[51],
		ctx->r[52], ctx->r[53], ctx->r[54], ctx->r[55], ctx->r[56],
		ctx->r[57], ctx->r[58], ctx->r[59], ctx->r[60], ctx->r[61],
		ctx->r[62], ctx->r[63], ctx->r[64], ctx->r[65], ctx->r[66],
		ctx->r[67], ctx->r[68], ctx->r[69],
		ctx->inDebug ? "YES" : "NO", ctx->iceEnabled ? "YES" : "NO", 
		ctx->sjProbeEnabled ? "YES" : "NO");
}

EMUCPU_INIT( score )
{
	cpu_ctx_t* cpu_ctx;
	scorectx_t* ctx;
	cpu_ctx = malloc(sizeof(cpu_ctx_t));
	if(cpu_ctx == NULL)
		return NULL;
	ctx = malloc(sizeof(scorectx_t));
	if(ctx == NULL) {
		free(cpu_ctx);
		return NULL;
	}
	cpu_ctx->data = ctx;
	ctx->EMUCPU_READ_NAME( read8  ) = NULL;
	ctx->EMUCPU_WRITE_NAME(write8 ) = NULL;
	ctx->EMUCPU_READ_NAME( read16 ) = NULL;
	ctx->EMUCPU_WRITE_NAME(write16) = NULL;
	ctx->EMUCPU_READ_NAME( read32 ) = NULL;
	ctx->EMUCPU_WRITE_NAME(write32) = NULL;
	return cpu_ctx;
}

