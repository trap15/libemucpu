/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef SCOREOPS_H
#define SCOREOPS_H

#include "emucpu.h"

#include "score.h"

#define CORE7_BIG_INSTR_NAME(name)			core7_big_##name
#define CORE7_BIG_INSTR_EXEC(name)			CORE7_BIG_INSTR_NAME(name)(ctx, opcode);
#define CORE7_BIG_INSTRUCTION(name)			static core7Instruction CORE7_BIG_INSTR_NAME(name) (scorectx_t* ctx, u32 opcode)
#define BIG_INST_ENTRY(name, args, val, delay, cyc)	{ CORE7_BIG_INSTR_NAME(name), #name args, delay, cyc } 
#define CORE7_SMALL_INSTR_NAME(name)			core7_small_##name
#define CORE7_SMALL_INSTR_EXEC(name)			CORE7_SMALL_INSTR_NAME(name)(cpu, opcode);
#define CORE7_SMALL_INSTRUCTION(name)			static core7Instruction CORE7_SMALL_INSTR_NAME(name) (scorectx_t* ctx, u32 opcode)
#define SMALL_INST_ENTRY(name, args, val, delay, cyc)	{ CORE7_SMALL_INSTR_NAME(name), #name args, delay, cyc } 
#define NULL_INST_ENTRY					{ NULL, "", 0, 0 }

/* TODO: These are not the correct ones, but the SunPlus doc doesn't say what
 *       they should be, so I just improvised some. Should work fine though. */
#define CPUFLAG_C		(1 << 0)
#define CPUFLAG_N		(1 << 1)
#define CPUFLAG_Z		(1 << 2)
#define CPUFLAG_V		(1 << 3)
#define CPUFLAG_T		(1 << 4)

INLINE void generateExceptionCore(scorectx_t* ctx, u32 exception, u32 vec)
{
	/* TODO: Verify this */
	ctx->r[REGISTER_EPC] = ctx->pc;			/* Set the exception PC */
	ctx->r[REGISTER_ECR] = exception;		/* Set the exception cause register */
	/* Save the IE and KU bits */
	ctx->savedPSR = ctx->r[REGISTER_PSR] & 0x00000001; /* TODO: KU bit saving */
	/* Save the N, Z, C, V and T bits */
	ctx->savedCCR = ctx->r[REGISTER_CCR] & (CPUFLAG_N | CPUFLAG_Z | CPUFLAG_C | CPUFLAG_V | CPUFLAG_T);
	ctx->pc = vec - 4;	/* We have to subtract 4, since it would increase the PC after this */
}

INLINE void generateExceptionBase(scorectx_t* ctx, u32 exception, u32 spoff)
{
	u32 vec = ctx->r[REGISTER_EXCPVEC];	/* Get the exception vector */
	generateExceptionCore(ctx, exception, vec + 0x1FC + spoff);
}

INLINE void generateException(scorectx_t* ctx, u32 exception, u32 param)
{
	/* TODO: Get/verify the offsets for the various exceptions */
	u32 off = 0;
	switch(exception) {
		case EXCEPTION_BREAKPOINT:
			break;
		case EXCEPTION_SYSCALL:
		case EXCEPTION_TRAP:
			off += 4;
			break;
		default:
			off += 4;
			break;
	}
	generateExceptionBase(ctx, ((exception & 0x3F) << 18) | param, off);
}

#define FIELD_SIZE(x)	((((u64)1 << ((x) - 1)) - 1) | ((u64)1 << ((x) - 1)))
#define MAXMASK		FIELD_SIZE

#define _signextend(in, os, ns)	((in) | (((in) >> ((os) - 1)) ? (FIELD_SIZE((ns) - (os)) << (os)) : 0))
#define _zeroextend(in, os, ns) ((unsigned typeof((in)))(in))

#define HANDLE_HIT_SWITCH(type, foo) {												\
	switch(foo) {														\
		case 0:														\
			if((ctx->r[38] & CPUFLAG_C)) hit = 1;									\
			break;													\
		case 1:														\
			if(!(ctx->r[38] & CPUFLAG_C)) hit = 1;									\
			break;													\
		case 2:														\
			if((ctx->r[38] & CPUFLAG_C) && !(ctx->r[38] & CPUFLAG_Z)) hit = 1;					\
			break;													\
		case 3:														\
			if(!(ctx->r[38] & CPUFLAG_C) || (ctx->r[38] & CPUFLAG_Z)) hit = 1;					\
			break;													\
		case 4:														\
			if((ctx->r[38] & CPUFLAG_Z)) hit = 1;									\
			break;													\
		case 5:														\
			if(!(ctx->r[38] & CPUFLAG_Z)) hit = 1;									\
			break;													\
		case 6:														\
			if((!(ctx->r[38] & CPUFLAG_Z)) && ((ctx->r[38] & CPUFLAG_N) == (ctx->r[38] & CPUFLAG_V))) hit = 1;		\
			break;													\
		case 7:														\
			if(((ctx->r[38] & CPUFLAG_Z)) || ((ctx->r[38] & CPUFLAG_N) != (ctx->r[38] & CPUFLAG_V))) hit = 1;		\
			break;													\
		case 8:														\
			if((ctx->r[38] & CPUFLAG_N) == (ctx->r[38] & CPUFLAG_V)) hit = 1;						\
			break;													\
		case 9:														\
			if((ctx->r[38] & CPUFLAG_N) != (ctx->r[38] & CPUFLAG_V)) hit = 1;						\
			break;													\
		case 10:													\
			if((ctx->r[38] & CPUFLAG_N)) hit = 1;									\
			break;													\
		case 11:													\
			if(!(ctx->r[38] & CPUFLAG_N)) hit = 1;									\
			break;													\
		case 12:													\
			if((ctx->r[38] & CPUFLAG_V)) hit = 1;									\
			break;													\
		case 13:													\
			if(!(ctx->r[38] & CPUFLAG_V)) hit = 1;									\
			break;													\
		case 14:													\
			if(!type)												\
				break;												\
			if(ctx->r[REGISTER_CNT] > 0) hit = 1;									\
			if(type != 2)												\
				ctx->r[REGISTER_CNT]--;										\
			break;													\
		case 15:													\
			hit = 1;												\
			break;													\
	}															\
}

#define GET_SPECIAL_FORM {				\
	rD = (opcode >> 20) & FIELD_SIZE(5);		\
	rA = (opcode >> 15) & FIELD_SIZE(5);		\
	rB = (opcode >> 10) & FIELD_SIZE(5);		\
	CU = (opcode >>  0) & FIELD_SIZE(1);		\
}

#define GET_SYSCALL_FORM {				\
	imm15 = (opcode >> 10) & FIELD_SIZE(15);	\
}

#define GET_CENEW_FORM {				\
	USD1 = (opcode >> 20) & FIELD_SIZE(5);		\
	rA   = (opcode >> 15) & FIELD_SIZE(5);		\
	rB   = (opcode >> 10) & FIELD_SIZE(5);		\
	USD2 = (opcode >>  5) & FIELD_SIZE(5);		\
}

#define GET_COP_FORM {					\
	code20 = (opcode >>  5) & FIELD_SIZE(20);	\
	cop    = (opcode >>  3) & FIELD_SIZE( 2);	\
}

#define GET_J_FORM {					\
	disp24 = (opcode >>  1) & FIELD_SIZE(24);	\
	link   = (opcode >>  0) & FIELD_SIZE( 1);	\
}

#define GET_I_FORM {					\
	rD    = (opcode >> 20) & FIELD_SIZE( 5);	\
	imm16 = (opcode >>  1) & FIELD_SIZE(16);	\
	CU    = (opcode >>  0) & FIELD_SIZE( 1);	\
}

#define GET_RI_FORM {					\
	rD    = (opcode >> 20) & FIELD_SIZE( 5);	\
	rA    = (opcode >> 15) & FIELD_SIZE( 5);	\
	imm14 = (opcode >>  1) & FIELD_SIZE(14);	\
	CU    = (opcode >>  0) & FIELD_SIZE( 1);	\
}

#define GET_LS_FORM {					\
	rD    = (opcode >> 20) & FIELD_SIZE( 5);	\
	rA    = (opcode >> 15) & FIELD_SIZE( 5);	\
	imm15 = (opcode >>  0) & FIELD_SIZE(15);	\
}

#define GET_RIX_FORM {					\
	rD    = (opcode >> 20) & FIELD_SIZE( 5);	\
	rA    = (opcode >> 15) & FIELD_SIZE( 5);	\
	imm12 = (opcode >>  3) & FIELD_SIZE(12);	\
}

#define GET_BC_FORM {					\
	cond    = (opcode >> 10) & FIELD_SIZE( 5);	\
	link    = (opcode >>  0) & FIELD_SIZE( 1);	\
	imm19   = (opcode >> 15) & FIELD_SIZE(10);	\
	imm19 <<= 9;					\
	imm19  |= (opcode >>  1) & FIELD_SIZE( 9);	\
}

/* Add. */
CORE7_BIG_INSTRUCTION( ADD )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s32 val = ctx->r[rA] + ctx->r[rB];
	ctx->r[rD] = val;
	if(CU) {
		s64 tmp1 = ctx->r[rA] + ctx->r[rB];
		u64 tmp2 = (u64)ctx->r[rA] + (u64)ctx->r[rB];
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z | CPUFLAG_C | CPUFLAG_V);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
		ctx->r[38] |= tmp2 & 0x100000000LL ? CPUFLAG_C : 0;
		ctx->r[38] |= (tmp1 < (s64)-2147483648) || (tmp1 > (s64)2147483647) ? CPUFLAG_V : 0;
	}
	return 0;
}

/* Add with Carry. */
CORE7_BIG_INSTRUCTION( ADDC )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s32 val = ctx->r[rA] + ctx->r[rB] + ctx->r[38] & CPUFLAG_C;
	ctx->r[rD] = val;
	if(CU) {
		s64 tmp1 = ctx->r[rA] + ctx->r[rB] + ctx->r[38] & CPUFLAG_C;
		u64 tmp2 = (u64)ctx->r[rA] + (u64)ctx->r[rB] + ctx->r[38] & CPUFLAG_C;
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z | CPUFLAG_C | CPUFLAG_V);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
		ctx->r[38] |= tmp2 & 0x100000000LL ? CPUFLAG_C : 0;
		ctx->r[38] |= (tmp1 < (s64)-2147483648) || (tmp1 > (s64)2147483647) ? CPUFLAG_V : 0;
	}
	return 0;
}

/* Add with immediate. */
CORE7_BIG_INSTRUCTION( ADDI )
{
	s32 rD, CU;
	s16 imm16;
	GET_I_FORM
	s32 val = ctx->r[rD] + imm16;
	ctx->r[rD] = val;
	if(CU) {
		s64 tmp1 = ctx->r[rD] + imm16;
		u64 tmp2 = (u64)ctx->r[rD] + (u64)imm16;
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z | CPUFLAG_C | CPUFLAG_V);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
		ctx->r[38] |= tmp2 & 0x100000000LL ? CPUFLAG_C : 0;
		ctx->r[38] |= (tmp1 < (s64)-2147483648) || (tmp1 > (s64)2147483647) ? CPUFLAG_V : 0;
	}
	return 0;
}

/* Add with immediate shifted. */
CORE7_BIG_INSTRUCTION( ADDIS )
{
	s32 rD, CU;
	s16 imm16;
	GET_I_FORM
	s32 val = ctx->r[rD] + (imm16 << 16);
	ctx->r[rD] = val;
	if(CU) {
		s64 tmp1 = ctx->r[rD] + (imm16 << 16);
		u64 tmp2 = (u64)ctx->r[rD] + ((u64)imm16 << 16);
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z | CPUFLAG_C | CPUFLAG_V);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
		ctx->r[38] |= tmp2 & 0x100000000LL ? CPUFLAG_C : 0;
		ctx->r[38] |= (tmp1 < (s64)-2147483648) || (tmp1 > (s64)2147483647) ? CPUFLAG_V : 0;
	}
	return 0;
}

/* Add register with immediate. */
CORE7_BIG_INSTRUCTION( ADDRI )
{
	s32 rD, rA, CU;
	s16 imm14;
	GET_RI_FORM
	s32 val = ctx->r[rA] + _signextend(imm14, 14, 16);
	ctx->r[rD] = val;
	if(CU) {
		s64 tmp1 = ctx->r[rA] + _signextend(imm14, 14, 16);
		u64 tmp2 = (u64)ctx->r[rA] + (u64)_signextend(imm14, 14, 16);
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z | CPUFLAG_C | CPUFLAG_V);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
		ctx->r[38] |= tmp2 & 0x100000000LL ? CPUFLAG_C : 0;
		ctx->r[38] |= (tmp1 < (s64)-2147483648) || (tmp1 > (s64)2147483647) ? CPUFLAG_V : 0;
	}
	return 0;
}

/* Logical And. */
CORE7_BIG_INSTRUCTION( AND )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s32 val = ctx->r[rA] & ctx->r[rB];
	ctx->r[rD] = val;
	if(CU) {
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
	}
	return 0;
}

/* Logical And with immediate. */
CORE7_BIG_INSTRUCTION( ANDI )
{
	s32 rD, CU;
	s16 imm16;
	GET_I_FORM
	s32 val = ctx->r[rD] & imm16;
	ctx->r[rD] = val;
	if(CU) {
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
	}
	return 0;
}

/* Logical And with immediate shifted. */
CORE7_BIG_INSTRUCTION( ANDIS )
{
	s32 rD, CU;
	s16 imm16;
	GET_I_FORM
	s32 val = ctx->r[rD] & (imm16 << 16);
	ctx->r[rD] = val;
	if(CU) {
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
	}
	return 0;
}

/* Logical And register with immediate. */
CORE7_BIG_INSTRUCTION( ANDRI )
{
	s32 rD, rA, CU;
	s16 imm14;
	GET_RI_FORM
	s32 val = ctx->r[rA] & _signextend(imm14, 14, 16);
	ctx->r[rD] = val;
	if(CU) {
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
	}
	return 0;
}

/* Branch on condition cc. */
CORE7_BIG_INSTRUCTION( Bcc )
{
	s32 cond, link;
	s32 imm19;
	GET_BC_FORM
	int hit = 0;
	HANDLE_HIT_SWITCH(1, cond)
	if(hit) {
		if(link)
			ctx->r[3] = getPC(cpu) + 4;
		imm19 <<= 1;
		ctx->pc += _signextend(imm19, 19, 32);
		_branched = 1;
	}
	return 0;
}

/* Bit Clear in Register. */
CORE7_BIG_INSTRUCTION( BITCLR )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s32 val = ctx->r[rA] & ~(1 << rB); /* TODO: Verify this is correct */
	ctx->r[rD] = val;
	if(CU) {
		/* TODO: Make sure these are the correct flags */
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
	}
	return 0;
}

/* Bit Set in Register. */
CORE7_BIG_INSTRUCTION( BITSET )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s32 val = ctx->r[rA] | (1 << rB); /* TODO: Verify this is correct */
	ctx->r[rD] = val;
	if(CU) {
		/* TODO: Make sure these are the correct flags */
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
	}
	return 0;
}

/* Bit Toggle in Register. */
CORE7_BIG_INSTRUCTION( BITTGL )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s32 val = ctx->r[rA] ^ (1 << rB); /* TODO: Verify this is correct */
	ctx->r[rD] = val;
	if(CU) {
		/* TODO: Make sure these are the correct flags */
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
		ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
	}
	return 0;
}

/* Bit Test in Register. */
CORE7_BIG_INSTRUCTION( BITTST )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s32 val = ctx->r[rA];
	ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
	ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
	ctx->r[38] |= (val & (1 << rB)) == 0 ? CPUFLAG_Z : 0;
	return 0;
}

/* Branch Register on condition cc. */
CORE7_BIG_INSTRUCTION( BRcc )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	int hit = 0;
	HANDLE_HIT_SWITCH(1, rB)
	if(hit) {
		if(CU)
			ctx->r[3] = getPC(cpu) + 4;
		ctx->pc = ctx->r[rA];
		_branched = 1;
	}
	return 0;
}

/* Cache Operation. */
CORE7_BIG_INSTRUCTION( CACHE )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction %s is unhandled\n", __FUNCTION__);
#endif
	return 0;
}

/* Custom Engine user defined Instruction. */
CORE7_BIG_INSTRUCTION( CEINST )
{
	s32 rA, rB, USD1, USD2;
	GET_CENEW_FORM
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction %s is unhandled\n", __FUNCTION__);
#endif
	return 0;
}

/* Compare. */
CORE7_BIG_INSTRUCTION( CMPcc )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s32 val = ctx->r[rA] - ctx->r[rB];
	s64 tmp1 = ctx->r[rA] - ctx->r[rB];
	u64 tmp2 = (u64)ctx->r[rA] - (u64)ctx->r[rB];
	ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z | CPUFLAG_C | CPUFLAG_V | CPUFLAG_T);
	ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
	ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
	ctx->r[38] |= tmp2 & 0x100000000LL ? 0 : CPUFLAG_C;
	ctx->r[38] |= (tmp1 < (s64)-2147483648) || (tmp1 > (s64)2147483647) ? CPUFLAG_V : 0;
	if(rD == 0)
		ctx->r[38] |= val == 0 ? CPUFLAG_T : 0;
	if(rD == 1)
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_T : 0;
	return 0;
}

/* Compare with Immediate. */
CORE7_BIG_INSTRUCTION( CMPI )
{
	s32 rD, CU;
	s16 imm16;
	GET_I_FORM
	s32 val = ctx->r[rD] - imm16;
	s64 tmp1 = ctx->r[rD] - imm16;
	u64 tmp2 = (u64)ctx->r[rD] - (u64)imm16;
	ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z | CPUFLAG_C | CPUFLAG_V);
	ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
	ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
	ctx->r[38] |= tmp2 & 0x100000000LL ? 0 : CPUFLAG_C;
	ctx->r[38] |= (tmp1 < (s64)-2147483648) || (tmp1 > (s64)2147483647) ? CPUFLAG_V : 0;
	return 0;
}

/* Compare with Zero. */
CORE7_BIG_INSTRUCTION( CMPZcc )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s32 val = ctx->r[rA];
	ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z | CPUFLAG_C | CPUFLAG_V | CPUFLAG_T);
	ctx->r[38] |= val & (1 << 31) ? CPUFLAG_N : 0;
	ctx->r[38] |= val == 0 ? CPUFLAG_Z : 0;
	if(rD == 0)
		ctx->r[38] |= val == 0 ? CPUFLAG_T : 0;
	if(rD == 1)
		ctx->r[38] |= val & (1 << 31) ? CPUFLAG_T : 0;
	return 0;
}

/* Coprocessor User-defined Instruction. */
CORE7_BIG_INSTRUCTION( COPx )
{
	s32 cop, code20;
	GET_COP_FORM
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction %s is unhandled\n", __FUNCTION__);
#endif
	return 0;
}

/* Divide. */
CORE7_BIG_INSTRUCTION( DIV )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	ctx->r[REGISTER_LO] = ctx->r[rA] / ctx->r[rB];
	ctx->r[REGISTER_HI] = ctx->r[rA] % ctx->r[rB];
	return 0;
}

/* Divide Unsigned. */
CORE7_BIG_INSTRUCTION( DIVU )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	ctx->r[REGISTER_LO] = (u32)ctx->r[rA] / (u32)ctx->r[rB];
	ctx->r[REGISTER_HI] = (u32)ctx->r[rA] % (u32)ctx->r[rB];
	return 0;
}

/* Return from Debug Exception. */
CORE7_BIG_INSTRUCTION( DRTE )
{
	ctx->pc = ctx->r[REGISTER_DEPC];
	ctx->inDebug = 0;
	return 0;
}

/* Extend Sign of Byte. */
CORE7_BIG_INSTRUCTION( EXTSB )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s8 val = ctx->r[rA] & 0xFF;
	ctx->r[rD] = val; /* I don't have to extend sign because C is awesome :o */
	if(CU) {
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
		ctx->r[38] |= val & (1 << 7) ? CPUFLAG_N : 0;
	}
	return 0;
}

/* Extend Sign of Half-word. */
CORE7_BIG_INSTRUCTION( EXTSH )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s16 val = ctx->r[rA] & 0xFFFF;
	ctx->r[rD] = val; /* I don't have to extend sign because C is awesome :o */
	if(CU) {
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
		ctx->r[38] |= val & (1 << 15) ? CPUFLAG_N : 0;
	}
	return 0;
}

/* Extend Zero of Byte. */
CORE7_BIG_INSTRUCTION( EXTZB )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	u8 val = ctx->r[rA] & 0xFF;
	ctx->r[rD] = val;
	if(CU) {
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
	}
	return 0;
}

/* Extend Zero of Half-word. */
CORE7_BIG_INSTRUCTION( EXTZH )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	u16 val = ctx->r[rA] & 0xFFFF;
	ctx->r[rD] = val;
	if(CU) {
		ctx->r[38] &= ~(CPUFLAG_N | CPUFLAG_Z);
	}
	return 0;
}

/* Jump (and Link). */
CORE7_BIG_INSTRUCTION( J )
{
	s32 disp24, link;
	GET_J_FORM
	if(link)
		ctx->r[3] = getPC(cpu) + 4;
	ctx->pc &= 0xFE000000;
	ctx->pc |= disp24 << 1;
	_branched = 1;
	return 0;
}

/* Load Combined Word Begin. */
CORE7_BIG_INSTRUCTION( LCB )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	setRegister(cpu, REGISTER_LCR, readWord(cpu, (readRegister(cpu, rA) & 0xFFFFFFFC)));
	setRegister(cpu, rA, readRegister(cpu, rA) + 4);
	return 0;
}

/* Load Combined Word End. */
CORE7_BIG_INSTRUCTION( LCW )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	int byte = readRegister(cpu, rA) & 3;
#if TARGET_ENDIAN == ENDIANNESS_LE
	byte ^= 3;
#endif
	core7Register rDx, LCRx;
	LCRx = readRegister(cpu, REGISTER_LCR);
	switch(byte) {
		case 0:
			rDx = LCRx;
			LCRx = readWord(cpu, (readRegister(cpu, rA) & 0xFFFFFFFC));
			break;
		case 1:
			rDx = LCRx << 8;
			LCRx = readWord(cpu, (readRegister(cpu, rA) & 0xFFFFFFFC));
			rDx |= (LCRx >> 24) & 0xFF;
			break;
		case 2:
			rDx = LCRx << 16;
			LCRx = readWord(cpu, (readRegister(cpu, rA) & 0xFFFFFFFC));
			rDx |= (LCRx >> 16) & 0xFF;
			break;
		case 3:
			rDx = LCRx << 24;
			LCRx = readWord(cpu, (readRegister(cpu, rA) & 0xFFFFFFFC));
			rDx |= (LCRx >> 8) & 0xFF;
			break;
		default:
			rDx = readRegister(cpu, rD);
			break;
	}
	setRegister(cpu, REGISTER_LCR, LCRx);
	setRegister(cpu, rD, rDx);
	setRegister(cpu, rA, readRegister(cpu, rA) + 4);
	return 0;
}

/* Load Combined Word End. */
CORE7_BIG_INSTRUCTION( LCE )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	int b = readRegister(cpu, rA) & 3;
	int byte = b;
#if TARGET_ENDIAN == ENDIANNESS_LE
	byte ^= 3;
#endif
	core7Register rDx, LCRx;
	if(b == 0) {
		rDx = readRegister(cpu, REGISTER_LCR);
	}else{
		LCRx = readRegister(cpu, REGISTER_LCR);
		switch(byte) {
			case 0:
				rDx = LCRx;
				LCRx = readWord(cpu, (readRegister(cpu, rA) & 0xFFFFFFFC));
				break;
			case 1:
				rDx = LCRx << 8;
				LCRx = readWord(cpu, (readRegister(cpu, rA) & 0xFFFFFFFC));
				rDx |= (LCRx >> 24) & 0xFF;
				break;
			case 2:
				rDx = LCRx << 16;
				LCRx = readWord(cpu, (readRegister(cpu, rA) & 0xFFFFFFFC));
				rDx |= (LCRx >> 16) & 0xFF;
				break;
			case 3:
				rDx = LCRx << 24;
				LCRx = readWord(cpu, (readRegister(cpu, rA) & 0xFFFFFFFC));
				rDx |= (LCRx >> 8) & 0xFF;
				break;
			default:
				rDx = readRegister(cpu, rD);
				break;
		}
		setRegister(cpu, REGISTER_LCR, LCRx);
	}
	setRegister(cpu, rD, rDx);
	setRegister(cpu, rA, readRegister(cpu, rA) + 4);
	return 0;
}

/* Load Byte signed. */
CORE7_BIG_INSTRUCTION( LB )
{
	s32 rD, rA;
	s16 imm15;
	GET_LS_FORM
	setRegister(cpu, rD, readByte(cpu, readRegister(cpu, rA) + _signextend(imm15, 15, 16)));
	return 0;
}

/* Load Byte signed (post-index). */
CORE7_BIG_INSTRUCTION( LB2 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	setRegister(cpu, rD, readByte(cpu, readRegister(cpu, rA)));
	setRegister(cpu, rA, readRegister(cpu, rA) + _signextend(imm12, 12, 16));
	return 0;
}

/* Load Byte signed (pre-index). */
CORE7_BIG_INSTRUCTION( LB3 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	core7Register val = readRegister(cpu, rA) + _signextend(imm12, 12, 16);
	setRegister(cpu, rD, readByte(cpu, val));
	setRegister(cpu, rA, val);
	return 0;
}

/* Load Byte Unsigned. */
CORE7_BIG_INSTRUCTION( LBU )
{
	s32 rD, rA;
	s16 imm15;
	GET_LS_FORM
	setRegister(cpu, rD, (u8)readByte(cpu, readRegister(cpu, rA) + _signextend(imm15, 15, 16)));
	return 0;
}

/* Load Byte Unsigned (post-index). */
CORE7_BIG_INSTRUCTION( LBU2 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	setRegister(cpu, rD, (u8)readByte(cpu, readRegister(cpu, rA)));
	setRegister(cpu, rA, readRegister(cpu, rA) + _signextend(imm12, 12, 16));
	return 0;
}

/* Load Byte Unsigned (pre-index). */
CORE7_BIG_INSTRUCTION( LBU3 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	core7Register val = readRegister(cpu, rA) + _signextend(imm12, 12, 16);
	setRegister(cpu, rD, (u8)readByte(cpu, val));
	setRegister(cpu, rA, val);
	return 0;
}

/* Load Coprocessor data register from memory. */
CORE7_BIG_INSTRUCTION( LDCx )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction (%d, %d, %s) is unhandled\n", dasm->table, dasm->instruction, __FUNCTION__);
#endif
	return 0;
}

/* Load Immediate. */
CORE7_BIG_INSTRUCTION( LDI )
{
	s32 rD, CU;
	s16 imm16;
	GET_I_FORM
	setRegister(cpu, rD, imm16);
	return 0;
}

/* Load Immediate Shifted. */
CORE7_BIG_INSTRUCTION( LDIS )
{
	s32 rD, CU;
	s16 imm16;
	GET_I_FORM
	setRegister(cpu, rD, imm16 << 16);
	return 0;
}

/* Load Half-word signed. */
CORE7_BIG_INSTRUCTION( LH )
{
	s32 rD, rA;
	s16 imm15;
	GET_LS_FORM
	setRegister(cpu, rD, readHword(cpu, readRegister(cpu, rA) + _signextend(imm15, 15, 16)));
	return 0;
}

/* Load Half-word signed (post-index). */
CORE7_BIG_INSTRUCTION( LH2 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	setRegister(cpu, rD, readHword(cpu, readRegister(cpu, rA)));
	setRegister(cpu, rA, readRegister(cpu, rA) + _signextend(imm12, 12, 16));
	return 0;
}

/* Load Half-word signed (pre-index). */
CORE7_BIG_INSTRUCTION( LH3 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	core7Register val = readRegister(cpu, rA) + _signextend(imm12, 12, 16);
	setRegister(cpu, rD, readHword(cpu, val));
	setRegister(cpu, rA, val);
	return 0;
}

/* Load Half-word Unsigned. */
CORE7_BIG_INSTRUCTION( LHU )
{
	s32 rD, rA;
	s16 imm15;
	GET_LS_FORM
	setRegister(cpu, rD, (u16)readHword(cpu, readRegister(cpu, rA) + _signextend(imm15, 15, 16)));
	return 0;
}

/* Load Half-word Unsigned (post-index). */
CORE7_BIG_INSTRUCTION( LHU2 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	setRegister(cpu, rD, (u16)readHword(cpu, readRegister(cpu, rA)));
	setRegister(cpu, rA, readRegister(cpu, rA) + _signextend(imm12, 12, 16));
	return 0;
}

/* Load Half-word Unsigned (pre-index). */
CORE7_BIG_INSTRUCTION( LHU3 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	core7Register val = readRegister(cpu, rA) + _signextend(imm12, 12, 16);
	setRegister(cpu, rD, (u16)readHword(cpu, val));
	setRegister(cpu, rA, val);
	return 0;
}

/* Load Word. */
CORE7_BIG_INSTRUCTION( LW )
{
	s32 rD, rA;
	s16 imm15;
	GET_LS_FORM
	setRegister(cpu, rD, readWord(cpu, readRegister(cpu, rA) + _signextend(imm15, 15, 16)));
	return 0;
}

/* Load Word (post-index). */
CORE7_BIG_INSTRUCTION( LW2 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	setRegister(cpu, rD, readWord(cpu, readRegister(cpu, rA)));
	setRegister(cpu, rA, readRegister(cpu, rA) + _signextend(imm12, 12, 16));
	return 0;
}

/* Load Word (pre-index). */
CORE7_BIG_INSTRUCTION( LW3 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	core7Register val = readRegister(cpu, rA) + _signextend(imm12, 12, 16);
	setRegister(cpu, rD, readWord(cpu, val));
	setRegister(cpu, rA, val);
	return 0;
}

/* Move from Custom Engine. */
CORE7_BIG_INSTRUCTION( MFCEx )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	switch(rB) {
		case 1:
			setRegister(cpu, rD, readRegister(cpu, REGISTER_LO));
			break;
		case 3:
			setRegister(cpu, rA, readRegister(cpu, REGISTER_LO));
		case 2:
			setRegister(cpu, rD, readRegister(cpu, REGISTER_HI));
			break;
	}
	return 0;
}

/* Move from Control Register. */
CORE7_BIG_INSTRUCTION( MFCR )
{
	s32 rD, rA, imm15;
	GET_LS_FORM
	setRegister(cpu, rD, readRegister(cpu, REGISTER_CR(rA)));	
	return 0;
}

/* Move from Coprocessor Data Register. */
CORE7_BIG_INSTRUCTION( MFCx )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction (%d, %d, %s) is unhandled\n", dasm->table, dasm->instruction, __FUNCTION__);
#endif
	return 0;
}

/* Move from Coprocessor Control Register. */
CORE7_BIG_INSTRUCTION( MFCCx )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction (%d, %d, %s) is unhandled\n", dasm->table, dasm->instruction, __FUNCTION__);
#endif
	return 0;
}

/* Move from Special Register. */
CORE7_BIG_INSTRUCTION( MFSR )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	setRegister(cpu, rD, readRegister(cpu, REGISTER_SR(rB)));	
	return 0;
}

/* Move from Custom Engine. */
CORE7_BIG_INSTRUCTION( MTCEx )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	switch(rB) {
		case 1:
			setRegister(cpu, REGISTER_LO, readRegister(cpu, rD));
			break;
		case 3:
			setRegister(cpu, REGISTER_LO, readRegister(cpu, rA));
		case 2:
			setRegister(cpu, REGISTER_HI, readRegister(cpu, rD));
			break;
	}
	return 0;
}

/* Move to Control Register. */
CORE7_BIG_INSTRUCTION( MTCR )
{
	s32 rD, rA, imm15;
	GET_LS_FORM
	setRegister(cpu, REGISTER_CR(rA), readRegister(cpu, rD));
	return 0;
}

/* Move to Coprocessor Data Register. */
CORE7_BIG_INSTRUCTION( MTCx )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction (%d, %d, %s) is unhandled\n", dasm->table, dasm->instruction, __FUNCTION__);
#endif
	return 0;
}

/* Move to Coprocessor Control Register. */
CORE7_BIG_INSTRUCTION( MTCCx )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction (%d, %d, %s) is unhandled\n", dasm->table, dasm->instruction, __FUNCTION__);
#endif
	return 0;
}

/* Move to Special Register. */
CORE7_BIG_INSTRUCTION( MTSR )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	setRegister(cpu, REGISTER_SR(rB), readRegister(cpu, rD));	
	return 0;
}

/* Move Conditional. */
CORE7_BIG_INSTRUCTION( MVcc )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	int hit = 0;
	HANDLE_HIT_SWITCH(0, rB)
	if(hit) {
		setRegister(cpu, rD, readRegister(cpu, rA));
	}
	return 0;
}

/* Multiply. */
CORE7_BIG_INSTRUCTION( MUL )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	s64 val = (s64)readRegister(cpu, rA) * (s64)readRegister(cpu, rB);
	setRegister(cpu, REGISTER_HI, (val >> 32) & FIELD_SIZE(32));
	setRegister(cpu, REGISTER_LO, (val >>  0) & FIELD_SIZE(32));
	return 0;
}

/* Multiply Unsigned. */
CORE7_BIG_INSTRUCTION( MULU )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	u64 val = (u64)readRegister(cpu, rA) * (u64)readRegister(cpu, rB);
	setRegister(cpu, REGISTER_HI, (val >> 32) & FIELD_SIZE(32));
	setRegister(cpu, REGISTER_LO, (val >>  0) & FIELD_SIZE(32));
	return 0;
}

/* Negative. */
CORE7_BIG_INSTRUCTION( NEG )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register val = -readRegister(cpu, rB);
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
		s64 tmp1 = -readRegister(cpu, rA);
		setFlag(cpu, CPUFLAG_C, 0);
		setFlag(cpu, CPUFLAG_V, (tmp1 < (s64)-2147483648) || (tmp1 > (s64)2147483647));
	}
	return 0;
}

/* No operation. */
CORE7_BIG_INSTRUCTION( NOP )
{
	return 0;
}

/* Logical NOT. */
CORE7_BIG_INSTRUCTION( NOT )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register val = ~readRegister(cpu, rA);
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
	}
	return 0;
}

/* Logical OR. */
CORE7_BIG_INSTRUCTION( OR )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register val = readRegister(cpu, rA) | readRegister(cpu, rB);
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
	}
	return 0;
}

/* Logical OR with Immediate. */
CORE7_BIG_INSTRUCTION( ORI )
{
	s32 rD, imm16, CU;
	GET_I_FORM
	core7Register val = readRegister(cpu, rD) | imm16;
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
	}
	return 0;
}

/* Logical OR with Immediate Shifted. */
CORE7_BIG_INSTRUCTION( ORIS )
{
	s32 rD, imm16, CU;
	GET_I_FORM
	core7Register val = readRegister(cpu, rD) | (imm16 << 16);
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
	}
	return 0;
}

/* Logical OR Register with Immediate. */
CORE7_BIG_INSTRUCTION( ORRI )
{
	s32 rD, rA, CU;
	s16 imm14;
	GET_RI_FORM
	core7Register val = readRegister(cpu, rA) | _signextend(imm14, 14, 16);
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
	}
	return 0;
}

/* Pipeline Flush. */
CORE7_BIG_INSTRUCTION( PFLUSH )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction (%d, %d, %s) is unhandled\n", dasm->table, dasm->instruction, __FUNCTION__);
#endif
	return 0;
}

/* Rotate Left. */
CORE7_BIG_INSTRUCTION( ROL )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register rBx = readRegister(cpu, rB);
	core7Register rAx = readRegister(cpu, rA);
	if(rBx == 0) {
		setRegister(cpu, rD, rAx);
		if(CU)
			setFlag(cpu, CPUFLAG_N, rAx & (1 << 31));
	}else{
		core7Register val = (rAx << rBx) | (rAx >> (31 - rBx));
		setRegister(cpu, rD, val);
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_C, rAx & (1 << (32 - rBx)));
		}
	}
	return 0;
}

/* Rotate Left Immediate. */
CORE7_BIG_INSTRUCTION( ROLI )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register rAx = readRegister(cpu, rA);
	if(rB == 0) {
		setRegister(cpu, rD, rAx);
		if(CU)
			setFlag(cpu, CPUFLAG_N, rAx & (1 << 31));
	}else{
		core7Register val = (rAx << rB) | (rAx >> (31 - rB));
		setRegister(cpu, rD, val);
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_C, rAx & (1 << (32 - rB)));
		}
	}
	return 0;
}

/* Rotate Left with Carry. */
CORE7_BIG_INSTRUCTION( ROLC )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register rBx = readRegister(cpu, rB);
	core7Register rAx = readRegister(cpu, rA);
	if(rBx == 0) {
		setRegister(cpu, rD, rAx);
		setFlag(cpu, CPUFLAG_N, rAx & (1 << 31));
	}else if(rBx == 1) {
		core7Register val = (rAx << 1) | getFlag(cpu, CPUFLAG_C);
		setRegister(cpu, rD, val);
		setFlag(cpu, CPUFLAG_C, rAx & (1 << 31));
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
	}else{
		core7Register val = (rAx << rBx) | getFlag(cpu, CPUFLAG_C) << (rBx - 1) | (rAx >> (32 - rBx));
		setRegister(cpu, rD, val);
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_C, rAx & (1 << (32 - rBx)));
		}
	}
	return 0;
}

/* Rotate Left Immediate with Carry. */
CORE7_BIG_INSTRUCTION( ROLIC )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register rAx = readRegister(cpu, rA);
	if(rB == 0) {
		setRegister(cpu, rD, rAx);
		setFlag(cpu, CPUFLAG_N, rAx & (1 << 31));
	}else if(rB == 1) {
		core7Register val = (rAx << 1) | getFlag(cpu, CPUFLAG_C);
		setRegister(cpu, rD, val);
		setFlag(cpu, CPUFLAG_C, rAx & (1 << 31));
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
	}else{
		core7Register val = (rAx << rB) | getFlag(cpu, CPUFLAG_C) << (rB - 1) | (rAx >> (32 - rB));
		setRegister(cpu, rD, val);
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_C, rAx & (1 << (32 - rB)));
		}
	}
	return 0;
}

/* Rotate Right. */
CORE7_BIG_INSTRUCTION( ROR )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register rBx = readRegister(cpu, rB);
	core7Register rAx = readRegister(cpu, rA);
	if(rBx == 0) {
		setRegister(cpu, rD, rAx);
		if(CU)
			setFlag(cpu, CPUFLAG_N, rAx & (1 << 31));
	}else{
		core7Register val = (rAx >> rBx) | (rAx << (31 - rBx));
		setRegister(cpu, rD, val);
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_C, rAx & (1 << (rBx - 1)));
		}
	}
	return 0;
}

/* Rotate Right Immediate. */
CORE7_BIG_INSTRUCTION( RORI )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register rAx = readRegister(cpu, rA);
	if(rB == 0) {
		setRegister(cpu, rD, rAx);
		if(CU)
			setFlag(cpu, CPUFLAG_N, rAx & (1 << 31));
	}else{
		core7Register val = (rAx >> rB) | (rAx << (31 - rB));
		setRegister(cpu, rD, val);
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_C, rAx & (1 << (rB - 1)));
		}
	}
	return 0;
}

/* Rotate Right with Carry. */
CORE7_BIG_INSTRUCTION( RORC )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register rBx = readRegister(cpu, rB);
	core7Register rAx = readRegister(cpu, rA);
	if(rBx == 0) {
		setRegister(cpu, rD, rAx);
		setFlag(cpu, CPUFLAG_N, rAx & (1 << 31));
	}else if(rBx == 1) {
		core7Register val = (rAx >> 1) | (getFlag(cpu, CPUFLAG_C) << 31);
		setRegister(cpu, rD, val);
		setFlag(cpu, CPUFLAG_C, rAx & (1 <<  0));
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
	}else{
		core7Register val = (rAx >> rBx) | getFlag(cpu, CPUFLAG_C) << (31 - rBx) | (rAx << (32 - rBx));
		setRegister(cpu, rD, val);
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_C, rAx & (1 << (rBx - 1)));
		}
	}
	return 0;
}

/* Rotate Right Immediate with Carry. */
CORE7_BIG_INSTRUCTION( RORIC )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register rAx = readRegister(cpu, rA);
	if(rB == 0) {
		setRegister(cpu, rD, rAx);
		setFlag(cpu, CPUFLAG_N, rAx & (1 << 31));
	}else if(rB == 1) {
		core7Register val = (rAx >> 1) | (getFlag(cpu, CPUFLAG_C) << 31);
		setRegister(cpu, rD, val);
		setFlag(cpu, CPUFLAG_C, rAx & (1 <<  0));
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
	}else{
		core7Register val = (rAx >> rB) | getFlag(cpu, CPUFLAG_C) << (31 - rB) | (rAx << (32 - rB));
		setRegister(cpu, rD, val);
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_C, rAx & (1 << (rB - 1)));
		}
	}
	return 0;
}

/* Return from Exception. */
CORE7_BIG_INSTRUCTION( RTE )
{
	setRegister(cpu, REGISTER_PSR, (readRegister(cpu, REGISTER_PSR) & ~0x00000001) | cpu->savedPSR);	/* TODO: KU bit */
	setRegister(cpu, REGISTER_CCR, (readRegister(cpu, REGISTER_CCR) & 
					~(CPUFLAG_N | CPUFLAG_Z | CPUFLAG_C | CPUFLAG_V | CPUFLAG_T)) | cpu->savedCCR);
	setPC(cpu, readRegister(cpu, REGISTER_EPC));
	return 0;
}

/* Store Combined word Begin. */
CORE7_BIG_INSTRUCTION( SCB )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction (%d, %d, %s) is unhandled\n", dasm->table, dasm->instruction, __FUNCTION__);
#endif
	return 0;
}

/* Store Combined Word. */
CORE7_BIG_INSTRUCTION( SCW )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction (%d, %d, %s) is unhandled\n", dasm->table, dasm->instruction, __FUNCTION__);
#endif
	return 0;
}

/* Store Combined word End. */
CORE7_BIG_INSTRUCTION( SCE )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction (%d, %d, %s) is unhandled\n", dasm->table, dasm->instruction, __FUNCTION__);
#endif
	return 0;
}

/* Software Debug Breakpoint. */
CORE7_BIG_INSTRUCTION( SDBBP )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	if(!cpu->inDebug) {
		if(cpu->iceEnabled && cpu->sjProbeEnabled) {
			generateExceptionCore(cpu, (EXCEPTION_BREAKPOINT << 18) | rA, 0xFF000000);
		}else{
			setRegister(cpu, REGISTER_DEPC, getPC(cpu));
			generateException(cpu, EXCEPTION_BREAKPOINT, rA);
			cpu->inDebug = 1;
			/*			DBp = 1; */	/* TODO: Find out what that is */
		}
	}
	return 0;
}

/* Shift Left Logical. */
CORE7_BIG_INSTRUCTION( SLL )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register shft = readRegister(cpu, rB) & 0x1F;
	core7Register val = readRegister(cpu, rA);
	if(shft == 0) {
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_Z, val == 0);
		}
		return 0;
	}
	val <<= shft;
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
		setFlag(cpu, CPUFLAG_C, val & (1 << (32 - shft)));
	}
	return 0;
}

/* Shift Left Logical with Immediate. */
CORE7_BIG_INSTRUCTION( SLLI )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register val = readRegister(cpu, rA);
	if(rB == 0) {
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_Z, val == 0);
		}
		return 0;
	}
	val <<= rB;
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
		setFlag(cpu, CPUFLAG_C, val & (1 << (32 - rB)));
	}
	return 0;
}

/* Shift Right Arithmetic. */
CORE7_BIG_INSTRUCTION( SRA )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register val = readRegister(cpu, rB) & 0x1F;
	setRegister(cpu, rD, _signextend(readRegister(cpu, rA) >> val, 32 - val, 32));
	return 0;
}

/* Shift Right Arithmetic with Immediate. */
CORE7_BIG_INSTRUCTION( SRAI )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	setRegister(cpu, rD, _signextend(readRegister(cpu, rA) >> rB, 32 - rB, 32));
	return 0;
}

/* Shift Right Logical. */
CORE7_BIG_INSTRUCTION( SRL )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register shft = readRegister(cpu, rB) & 0x1F;
	core7Register val = readRegister(cpu, rA);
	if(shft == 0) {
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_Z, val == 0);
		}
		return 0;
	}
	val >>= shft;
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
		setFlag(cpu, CPUFLAG_C, val & (1 << (32 - shft)));
	}
	return 0;
}

/* Shift Right Logical with Immediate. */
CORE7_BIG_INSTRUCTION( SRLI )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register val = readRegister(cpu, rA);
	if(rB == 0) {
		if(CU) {
			setFlag(cpu, CPUFLAG_N, val & (1 << 31));
			setFlag(cpu, CPUFLAG_Z, val == 0);
		}
		return 0;
	}
	val >>= rB;
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
		setFlag(cpu, CPUFLAG_C, val & (1 << (32 - rB)));
	}
	return 0;
}

/* Store Byte. */
CORE7_BIG_INSTRUCTION( SB )
{
	s32 rD, rA;
	s16 imm15;
	GET_LS_FORM
	writeByte(cpu, readRegister(cpu, rA) + _signextend(imm15, 15, 16), readRegister(cpu, rD) & 0xFF);
	return 0;
}

/* Store Byte (post-index). */
CORE7_BIG_INSTRUCTION( SB2 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	writeByte(cpu, readRegister(cpu, rA), readRegister(cpu, rD) & 0xFF);
	setRegister(cpu, rA, readRegister(cpu, rA) + _signextend(imm12, 12, 16));
	return 0;
}

/* Store Byte (pre-index). */
CORE7_BIG_INSTRUCTION( SB3 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	core7Register val = readRegister(cpu, rA) + _signextend(imm12, 12, 16);
	writeByte(cpu, val, readRegister(cpu, rD) & 0xFF);
	setRegister(cpu, rA, val);
	return 0;
}

/* Store Half-word. */
CORE7_BIG_INSTRUCTION( SH )
{
	s32 rD, rA;
	s16 imm15;
	GET_LS_FORM
	writeHword(cpu, readRegister(cpu, rA) + _signextend(imm15, 15, 16), readRegister(cpu, rD) & 0xFFFF);
	return 0;
}

/* Store Half-word (post-index). */
CORE7_BIG_INSTRUCTION( SH2 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	writeHword(cpu, readRegister(cpu, rA), readRegister(cpu, rD) & 0xFFFF);
	setRegister(cpu, rA, readRegister(cpu, rA) + _signextend(imm12, 12, 16));
	return 0;
}

/* Store Half-word (pre-index). */
CORE7_BIG_INSTRUCTION( SH3 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	core7Register val = readRegister(cpu, rA) + _signextend(imm12, 12, 16);
	writeHword(cpu, val, readRegister(cpu, rD) & 0xFFFF);
	setRegister(cpu, rA, val);
	return 0;
}

/* Store Word. */
CORE7_BIG_INSTRUCTION( SW )
{
	s32 rD, rA;
	s16 imm15;
	GET_LS_FORM
	writeWord(cpu, readRegister(cpu, rA) + _signextend(imm15, 15, 16), readRegister(cpu, rD));
	return 0;
}

/* Store Word (post-index). */
CORE7_BIG_INSTRUCTION( SW2 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	writeWord(cpu, readRegister(cpu, rA), readRegister(cpu, rD));
	setRegister(cpu, rA, readRegister(cpu, rA) + _signextend(imm12, 12, 16));
	return 0;
}

/* Store Word (pre-index). */
CORE7_BIG_INSTRUCTION( SW3 )
{
	s32 rD, rA;
	s16 imm12;
	GET_RIX_FORM
	core7Register val = (core7Register_u)readRegister(cpu, rA) + (s16)_signextend(imm12, 12, 16);
	printf("%03X == %04X. + %08X = %08X\n", imm12, (s16)_signextend(imm12, 12, 16), (core7Register_u)readRegister(cpu, rA), 
	       val);
	writeWord(cpu, val, readRegister(cpu, rD));
	setRegister(cpu, rA, val);
	return 0;
}

/* Sleep. */
CORE7_BIG_INSTRUCTION( SLEEP )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction (%d, %d, %s) is unhandled\n", dasm->table, dasm->instruction, __FUNCTION__);
#endif
	return 0;
}

/* Store Coprocessor Data Register to Memory. */
CORE7_BIG_INSTRUCTION( STCx )
{
	/* TODO: Fill up this */
#if DEBUG
	printf("Instruction (%d, %d, %s) is unhandled\n", dasm->table, dasm->instruction, __FUNCTION__);
#endif
	return 0;
}

/* Subtract. */
CORE7_BIG_INSTRUCTION( SUB )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register val = readRegister(cpu, rA) - readRegister(cpu, rB);
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
		s64 tmp1 = readRegister(cpu, rA) - readRegister(cpu, rB);
		u64 tmp2 = (u64)readRegister(cpu, rA) - (u64)readRegister(cpu, rB);
		setFlag(cpu, CPUFLAG_C, (tmp2 & 0xFFFFFFFF00000000LL) == 0);
		setFlag(cpu, CPUFLAG_V, (tmp1 < (s64)-2147483648) || (tmp1 > (s64)2147483647));
	}
	return 0;
}

/* Subtract with Carry. */
CORE7_BIG_INSTRUCTION( SUBC )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register val = readRegister(cpu, rA) - readRegister(cpu, rB) - getFlag(cpu, CPUFLAG_C);
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
		s64 tmp1 = readRegister(cpu, rA) - readRegister(cpu, rB) - getFlag(cpu, CPUFLAG_C);
		u64 tmp2 = (u64)readRegister(cpu, rA) - (u64)readRegister(cpu, rB) - (u64)getFlag(cpu, CPUFLAG_C);
		setFlag(cpu, CPUFLAG_C, (tmp2 & 0xFFFFFFFF00000000LL) == 0);
		setFlag(cpu, CPUFLAG_V, (tmp1 < (s64)-2147483648) || (tmp1 > (s64)2147483647));
	}
	return 0;
}

/* System Call Trap. */
CORE7_BIG_INSTRUCTION( SYSCALL )
{
	s32 imm15;
	GET_SYSCALL_FORM
	generateException(cpu, EXCEPTION_SYSCALL, imm15);
	return 0;
}

/* Test and Set Condition Flag T. */
CORE7_BIG_INSTRUCTION( Tcc )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	int hit = 0;
	HANDLE_HIT_SWITCH(2, rB)
	setFlag(cpu, CPUFLAG_T, hit);
	return 0;
}

/* Trap Conditional. */
CORE7_BIG_INSTRUCTION( TRAPcc )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	int hit = 0;
	HANDLE_HIT_SWITCH(0, rB)
	if(hit) {
		generateException(cpu, EXCEPTION_TRAP, rA);
	}
	return 0;
}

/* Logical XOR. */
CORE7_BIG_INSTRUCTION( XOR )
{
	s32 rD, rA, rB, CU;
	GET_SPECIAL_FORM
	core7Register val = readRegister(cpu, rA) ^ readRegister(cpu, rB);
	setRegister(cpu, rD, val);
	if(CU) {
		setFlag(cpu, CPUFLAG_N, val & (1 << 31));
		setFlag(cpu, CPUFLAG_Z, val == 0);
	}
	return 0;
}

core7InstrTbl instructionBig0x34Table[] = {
	BIG_INST_ENTRY( CEINST,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x30Table[] = {
	BIG_INST_ENTRY( CACHE,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x2ETable[] = {
	BIG_INST_ENTRY( SB,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x2CTable[] = {
	BIG_INST_ENTRY( LBU,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x2ATable[] = {
	BIG_INST_ENTRY( SH,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x28Table[] = {
	BIG_INST_ENTRY( SW,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x26Table[] = {
	BIG_INST_ENTRY( LB,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x24Table[] = {
	BIG_INST_ENTRY( LHU,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x22Table[] = {
	BIG_INST_ENTRY( LH,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x20Table[] = {
	BIG_INST_ENTRY( LW,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x1ATable[] = {
	BIG_INST_ENTRY( ORRI,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x18Table[] = {
	BIG_INST_ENTRY( ANDRI,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x10Table[] = {
	BIG_INST_ENTRY( ADDRI,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x0ETable[] = {
	BIG_INST_ENTRY( LW2,		"",		0x00000000,	0,	1 ),
	BIG_INST_ENTRY( LH2,		"",		0x00000001,	0,	1 ),
	BIG_INST_ENTRY( LHU2,		"",		0x00000002,	0,	1 ),
	BIG_INST_ENTRY( LB2,		"",		0x00000003,	0,	1 ),
	BIG_INST_ENTRY( SW2,		"",		0x00000004,	0,	1 ),
	BIG_INST_ENTRY( SH2,		"",		0x00000005,	0,	1 ),
	BIG_INST_ENTRY( LBU2,		"",		0x00000006,	0,	1 ),
	BIG_INST_ENTRY( SB2,		"",		0x00000007,	0,	1 ),
};
core7InstrTbl instructionBig0x0CTable[] = {
	BIG_INST_ENTRY( MTCR,		"",		0x00000000,	0,	1 ),
	BIG_INST_ENTRY( MFCR,		"",		0x00000001,	0,	1 ),
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( MTCx,		"",		0x00000008,	0,	1 ),	/* MTC1 */
	BIG_INST_ENTRY( MFCx,		"",		0x00000009,	0,	1 ),	/* MFC1 */
	BIG_INST_ENTRY( LDCx,		"",		0x0000000A,	0,	1 ),	/* LDC1 */
	BIG_INST_ENTRY( STCx,		"",		0x0000000B,	0,	1 ),	/* STC1 */
	BIG_INST_ENTRY( COPx,		"",		0x0000000C,	0,	1 ),	/* COP1 */
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( MTCCx,		"",		0x0000000E,	0,	1 ),	/* MTCC1 */
	BIG_INST_ENTRY( MFCCx,		"",		0x0000000F,	0,	1 ),	/* MFCC1 */
	BIG_INST_ENTRY( MTCx,		"",		0x00000010,	0,	1 ),	/* MTC2 */
	BIG_INST_ENTRY( MFCx,		"",		0x00000011,	0,	1 ),	/* MFC2 */
	BIG_INST_ENTRY( LDCx,		"",		0x00000012,	0,	1 ),	/* LDC2 */
	BIG_INST_ENTRY( STCx,		"",		0x00000013,	0,	1 ),	/* STC2 */
	BIG_INST_ENTRY( COPx,		"",		0x00000014,	0,	1 ),	/* COP2 */
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( MTCCx,		"",		0x00000016,	0,	1 ),	/* MTCC2 */
	BIG_INST_ENTRY( MFCCx,		"",		0x00000017,	0,	1 ),	/* MFCC2 */
	BIG_INST_ENTRY( MTCx,		"",		0x00000018,	0,	1 ),	/* MTC3 */
	BIG_INST_ENTRY( MFCx,		"",		0x00000019,	0,	1 ),	/* MFC3 */
	BIG_INST_ENTRY( LDCx,		"",		0x0000001A,	0,	1 ),	/* LDC3 */
	BIG_INST_ENTRY( STCx,		"",		0x0000001B,	0,	1 ),	/* STC3 */
	BIG_INST_ENTRY( COPx,		"",		0x0000001C,	0,	1 ),	/* COP3 */
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( MTCCx,		"",		0x0000001E,	0,	1 ),	/* MTCC3 */
	BIG_INST_ENTRY( MFCCx,		"",		0x0000001F,	0,	1 ),	/* MFCC3 */
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( RTE,		"",		0x00000084,	0,	1 ),
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( DRTE,		"",		0x000000A4,	0,	1 ),
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( SLEEP,		"",		0x000000C4,	0,	1 ),
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
};
core7InstrTbl instructionBig0x0ATable[] = {
	BIG_INST_ENTRY( ADDIS,		"",		0x00000000,	0,	1 ),
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( ANDIS,		"",		0x00000004,	0,	1 ),
	BIG_INST_ENTRY( ORIS,		"",		0x00000005,	0,	1 ),
	BIG_INST_ENTRY( LDIS,		"",		0x00000006,	0,	1 ),
	NULL_INST_ENTRY,
};
core7InstrTbl instructionBig0x08Table[] = {
	BIG_INST_ENTRY( Bcc,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x06Table[] = {
	BIG_INST_ENTRY( LW3,		"",		0x00000000,	0,	1 ),
	BIG_INST_ENTRY( LH3,		"",		0x00000001,	0,	1 ),
	BIG_INST_ENTRY( LHU3,		"",		0x00000002,	0,	1 ),
	BIG_INST_ENTRY( LB3,		"",		0x00000003,	0,	1 ),
	BIG_INST_ENTRY( SW3,		"",		0x00000004,	0,	1 ), /* TODO: GET CORRECT FUNC3 (4?) */
	BIG_INST_ENTRY( SH3,		"",		0x00000005,	0,	1 ),
	BIG_INST_ENTRY( LBU3,		"",		0x00000006,	0,	1 ),
	BIG_INST_ENTRY( SB3,		"",		0x00000007,	0,	1 ),
};
core7InstrTbl instructionBig0x04Table[] = {
	BIG_INST_ENTRY( J,		"",		0x00000000,	0,	1 ),
};
core7InstrTbl instructionBig0x02Table[] = {
	BIG_INST_ENTRY( ADDI,		"",		0x00000000,	0,	1 ),
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( CMPI,		"",		0x00000002,	0,	1 ),
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( ANDI,		"",		0x00000004,	0,	1 ),
	BIG_INST_ENTRY( ORI,		"",		0x00000005,	0,	1 ),
	BIG_INST_ENTRY( LDI,		"",		0x00000006,	0,	1 ),
	NULL_INST_ENTRY,
};
core7InstrTbl instructionBig0x00Table[] = {
	BIG_INST_ENTRY( NOP,		"",		0x00000000,	0,	1 ),
	BIG_INST_ENTRY( SYSCALL,	"",		0x00000001,	0,	1 ),
	BIG_INST_ENTRY( TRAPcc,		"",		0x00000002,	0,	1 ),
	BIG_INST_ENTRY( SDBBP,		"",		0x00000003,	0,	1 ),
	BIG_INST_ENTRY( BRcc,		"",		0x00000004,	0,	1 ),
	BIG_INST_ENTRY( PFLUSH,		"",		0x00000005,	0,	1 ),
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( ADD,		"",		0x00000008,	0,	1 ),
	BIG_INST_ENTRY( ADDC,		"",		0x00000009,	0,	1 ), /* TODO: GET CORRECT VALUE (12?)! */
	BIG_INST_ENTRY( SUB,		"",		0x0000000A,	0,	1 ),
	BIG_INST_ENTRY( SUBC,		"",		0x0000000B,	0,	1 ),
	BIG_INST_ENTRY( CMPcc,		"",		0x0000000C,	0,	1 ),
	BIG_INST_ENTRY( CMPZcc,		"",		0x0000000D,	0,	1 ),
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( NEG,		"",		0x0000000F,	0,	1 ),
	BIG_INST_ENTRY( AND,		"",		0x00000010,	0,	1 ),
	BIG_INST_ENTRY( OR,		"",		0x00000011,	0,	1 ),
	BIG_INST_ENTRY( NOT,		"",		0x00000012,	0,	1 ),
	BIG_INST_ENTRY( XOR,		"",		0x00000013,	0,	1 ),
	BIG_INST_ENTRY( BITCLR,		"",		0x00000014,	0,	1 ), /* Stupid SunPlus >:| */
	BIG_INST_ENTRY( BITSET,		"",		0x00000015,	0,	1 ), /* Stupid SunPlus >:| */
	BIG_INST_ENTRY( BITTST,		"",		0x00000016,	0,	1 ),
	BIG_INST_ENTRY( BITTGL,		"",		0x00000017,	0,	1 ), /* Stupid SunPlus >:| */
	BIG_INST_ENTRY( SLL,		"",		0x00000018,	0,	1 ),
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( SRL,		"",		0x0000001A,	0,	1 ),
	BIG_INST_ENTRY( SRA,		"",		0x0000001B,	0,	1 ),
	BIG_INST_ENTRY( ROR,		"",		0x0000001C,	0,	1 ),
	BIG_INST_ENTRY( RORC,		"",		0x0000001D,	0,	1 ),
	BIG_INST_ENTRY( ROL,		"",		0x0000001E,	0,	1 ),
	BIG_INST_ENTRY( ROLC,		"",		0x0000001F,	0,	1 ),
	BIG_INST_ENTRY( MUL,		"",		0x00000020,	0,	1 ),
	BIG_INST_ENTRY( MULU,		"",		0x00000021,	0,	1 ),
	BIG_INST_ENTRY( DIV,		"",		0x00000022,	0,	1 ),
	BIG_INST_ENTRY( DIVU,		"",		0x00000023,	0,	1 ),
	BIG_INST_ENTRY( MFCEx,		"",		0x00000024,	0,	1 ),
	BIG_INST_ENTRY( MTCEx,		"",		0x00000025,	0,	1 ),
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( MFSR,		"",		0x00000028,	0,	1 ),
	BIG_INST_ENTRY( MTSR,		"",		0x00000029,	0,	1 ),
	BIG_INST_ENTRY( Tcc,		"",		0x0000002A,	0,	1 ),
	BIG_INST_ENTRY( MVcc,		"",		0x0000002B,	0,	1 ),
	BIG_INST_ENTRY( EXTSB,		"",		0x0000002C,	0,	1 ),
	BIG_INST_ENTRY( EXTSH,		"",		0x0000002D,	0,	1 ),
	BIG_INST_ENTRY( EXTZB,		"",		0x0000002E,	0,	1 ),
	BIG_INST_ENTRY( EXTZH,		"",		0x0000002F,	0,	1 ),
	BIG_INST_ENTRY( LCB,		"",		0x00000030,	0,	1 ),
	BIG_INST_ENTRY( LCW,		"",		0x00000031,	0,	1 ),
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( LCE,		"",		0x00000033,	0,	1 ),
	BIG_INST_ENTRY( SCB,		"",		0x00000034,	0,	1 ),
	BIG_INST_ENTRY( SCW,		"",		0x00000035,	0,	1 ),
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( SCE,		"",		0x00000037,	0,	1 ),
	BIG_INST_ENTRY( SLLI,		"",		0x00000038,	0,	1 ),
	NULL_INST_ENTRY,
	BIG_INST_ENTRY( SRLI,		"",		0x0000003A,	0,	1 ),
	BIG_INST_ENTRY( SRAI,		"",		0x0000003B,	0,	1 ),
	BIG_INST_ENTRY( RORI,		"",		0x0000003C,	0,	1 ),
	BIG_INST_ENTRY( RORIC,		"",		0x0000003D,	0,	1 ),
	BIG_INST_ENTRY( ROLI,		"",		0x0000003E,	0,	1 ),
	BIG_INST_ENTRY( ROLIC,		"",		0x0000003F,	0,	1 ),
};



core7InstrTbl instructionSmall0x00Table[] = {
	SMALL_INST_ENTRY( NOP,		"",		0x00000000,	0,	1 ),
	SMALL_INST_ENTRY( MLFH,		"",		0x00000001,	0,	1 ),
	SMALL_INST_ENTRY( MHFL,		"",		0x00000002,	0,	1 ),
	SMALL_INST_ENTRY( MV,		"",		0x00000003,	0,	1 ),
	SMALL_INST_ENTRY( BRcc,		"",		0x00000004,	0,	1 ),
	SMALL_INST_ENTRY( Tcc,		"",		0x00000005,	0,	1 ),
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	SMALL_INST_ENTRY( SLL,		"",		0x00000008,	0,	1 ),
	SMALL_INST_ENTRY( ADDC,		"",		0x00000009,	0,	1 ),
	SMALL_INST_ENTRY( SRL,		"",		0x0000000A,	0,	1 ),
	SMALL_INST_ENTRY( SRA,		"",		0x0000000B,	0,	1 ),
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
	NULL_INST_ENTRY,
};

core7InstrTbl instructionSmall0x02Table[] = {
	SMALL_INST_ENTRY( ADD,		"",		0x00000000,	0,	1 ),
	SMALL_INST_ENTRY( SUB,		"",		0x00000001,	0,	1 ),
	SMALL_INST_ENTRY( NEG,		"",		0x00000002,	0,	1 ),
	SMALL_INST_ENTRY( CMP,		"",		0x00000003,	0,	1 ),
	SMALL_INST_ENTRY( AND,		"",		0x00000004,	0,	1 ),
	SMALL_INST_ENTRY( OR,		"",		0x00000005,	0,	1 ),
	SMALL_INST_ENTRY( NOT,		"",		0x00000006,	0,	1 ),
	SMALL_INST_ENTRY( XOR,		"",		0x00000007,	0,	1 ),
	SMALL_INST_ENTRY( LW,		"",		0x00000008,	0,	1 ),
	SMALL_INST_ENTRY( LH,		"",		0x00000009,	0,	1 ),
	SMALL_INST_ENTRY( POP,		"",		0x0000000A,	0,	1 ),
	SMALL_INST_ENTRY( LBU,		"",		0x0000000B,	0,	1 ),
	SMALL_INST_ENTRY( SW,		"",		0x0000000C,	0,	1 ),
	SMALL_INST_ENTRY( SH,		"",		0x0000000D,	0,	1 ),
	SMALL_INST_ENTRY( PUSH,		"",		0x0000000E,	0,	1 ),
	SMALL_INST_ENTRY( SB,		"",		0x0000000F,	0,	1 ),
};

core7InstrTbl instructionSmall0x03Table[] = {
	SMALL_INST_ENTRY( J,		"",		0x00000000,	0,	1 ),
};

core7InstrTbl instructionSmall0x04Table[] = {
	SMALL_INST_ENTRY( Bcc,		"",		0x00000000,	0,	1 ),
};

core7InstrTbl instructionSmall0x05Table[] = {
	SMALL_INST_ENTRY( LDIU,		"",		0x00000000,	0,	1 ),
};

core7InstrTbl instructionSmall0x06Table[] = {
	SMALL_INST_ENTRY( ADDEI,	"",		0x00000000,	0,	1 ),
	SMALL_INST_ENTRY( SLLI,		"",		0x00000001,	0,	1 ),
	SMALL_INST_ENTRY( SDBBP,	"",		0x00000002,	0,	1 ),
	SMALL_INST_ENTRY( SRLI,		"",		0x00000003,	0,	1 ),
	SMALL_INST_ENTRY( BITCLR,	"",		0x00000004,	0,	1 ), /* Stupid SunPlus >:| */
	SMALL_INST_ENTRY( BITSET,	"",		0x00000005,	0,	1 ), /* Stupid SunPlus >:| */
	SMALL_INST_ENTRY( BITTST,	"",		0x00000006,	0,	1 ), /* Stupid SunPlus >:| */
	SMALL_INST_ENTRY( BITTGL,	"",		0x00000007,	0,	1 ), /* Stupid SunPlus >:| */
};

core7InstrTbl instructionSmall0x07Table[] = {
	SMALL_INST_ENTRY( LWP,		"",		0x00000000,	0,	1 ),
	SMALL_INST_ENTRY( LHP,		"",		0x00000001,	0,	1 ),
	NULL_INST_ENTRY,
	SMALL_INST_ENTRY( LBUP,		"",		0x00000003,	0,	1 ),
	SMALL_INST_ENTRY( SWP,		"",		0x00000004,	0,	1 ),
	SMALL_INST_ENTRY( SHP,		"",		0x00000005,	0,	1 ),
	NULL_INST_ENTRY,
	SMALL_INST_ENTRY( SBP,		"",		0x00000007,	0,	1 ),
};


int opShf[] = {	 1, 17,  0,  0,  0, 17,  0,  0,  0, 42, 42, 42,  0,  0, 42, \
		42,  0,  0,  0,  0,  0,  0,  0,  0,  0, 42,  0, 42, 42, 42, \
		42, 42, \
		/* SMALL */ \
		 0, 42,  0,  0,  0,  0,  0,  0, \
};

int opAnd[] = { 
	0x3F, 
	0x07, 
	0x00, 
	0x07, 
	0x00, 
	0x07, 
	0xFF, 
	0x07, 
	0x00, 
	0x42, 
	0x42, 
	0x42, 
	0x00, 
	0x00, 
	0x42, 
	0x42, 
	0x00, 
	0x00, 
	0x00, 
	0x00, 
	0x00, 
	0x00, 
	0x00, 
	0x00, 
	0x00, 
	0x42, 
	0x00, 
	0x42, 
	0x42, 
	0x42, 
	0x42, 
	0x42, 
	/* SMALL */
	0x0F, 
	0x42, 
	0x0F, 
	0x00, 
	0x00, 
	0x00, 
	0x07, 
	0x07, 
};

core7InstrTbl *opTbl[] = {				\
	/*00*/	instructionBig0x00Table,		\
	/*01*/	instructionBig0x02Table,		\
	/*02*/	instructionBig0x04Table,		\
	/*03*/	instructionBig0x06Table,		\
	/*04*/	instructionBig0x08Table,		\
	/*05*/	instructionBig0x0ATable,		\
	/*06*/	instructionBig0x0CTable,		\
	/*07*/	instructionBig0x0ETable,		\
	/*08*/	instructionBig0x10Table,		\
	/*09*/	NULL,					\
	/*10*/	NULL,					\
	/*11*/	NULL,					\
	/*12*/	instructionBig0x18Table,		\
	/*13*/	instructionBig0x1ATable,		\
	/*14*/	NULL,					\
	/*15*/	NULL,					\
	/*16*/	instructionBig0x20Table,		\
	/*17*/	instructionBig0x22Table,		\
	/*18*/	instructionBig0x24Table,		\
	/*19*/	instructionBig0x26Table,		\
	/*20*/	instructionBig0x28Table,		\
	/*21*/	instructionBig0x2ATable,		\
	/*22*/	instructionBig0x2CTable,		\
	/*23*/	instructionBig0x2ETable,		\
	/*24*/	instructionBig0x30Table,		\
	/*25*/	NULL,					\
	/*26*/	instructionBig0x34Table,		\
	/*27*/	NULL,					\
	/*28*/	NULL,					\
	/*29*/	NULL,					\
	/*30*/	NULL,					\
	/*31*/	NULL,					\
	/* SMALL */				\
	/*32*/	instructionSmall0x00Table,		\
	/*33*/	NULL,					\
	/*34*/	instructionSmall0x02Table,		\
	/*35*/	instructionSmall0x03Table,		\
	/*36*/	instructionSmall0x04Table,		\
	/*37*/	instructionSmall0x05Table,		\
	/*38*/	instructionSmall0x06Table,		\
	/*39*/	instructionSmall0x07Table,		\
};

#endif

