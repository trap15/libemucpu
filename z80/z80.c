/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 The Lemon Man (Giuseppe)
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "emucpu.h"

#include "z80.h"
#include "z80ops.h"

EMUCPU_INTERRUPT( z80 )
{
	z80ctx_t* ctx = cpu_ctx->data;
}

EMUCPU_DASM( z80 )
{

}

EMUCPU_EMULATE( z80 )
{
	z80ctx_t* ctx = cpu_ctx->data;
}

EMUCPU_RESET( z80 )
{
	z80ctx_t* ctx = cpu_ctx->data;
}

EMUCPU_SAVE( z80 )
{
	z80ctx_t* ctx = cpu_ctx->data;
}

EMUCPU_RESTORE( z80 )
{
	z80ctx_t* ctx = cpu_ctx->data;
}

EMUCPU_GET_INFO( z80 )
{
	z80ctx_t* ctx = cpu_ctx->data;
	return EMUCPU_INFO_RET_NO_HANDLE;
}

EMUCPU_SET_INFO( z80 )
{
	z80ctx_t* ctx = cpu_ctx->data;
	return EMUCPU_INFO_RET_NO_HANDLE;
}

EMUCPU_INIT( z80 )
{
	cpu_ctx_t* cpu_ctx;
	z80ctx_t* ctx;
	cpu_ctx = malloc(sizeof(cpu_ctx_t));
	if(cpu_ctx == NULL)
		return NULL;
	ctx = malloc(sizeof(z80ctx_t));
	if(ctx == NULL) {
		free(cpu_ctx);
		return NULL;
	}
	cpu_ctx->data = ctx;
	ctx->EMUCPU_READ_NAME( portIn ) = NULL;
	ctx->EMUCPU_WRITE_NAME(portOut) = NULL;
	ctx->EMUCPU_READ_NAME( read8  ) = NULL;
	ctx->EMUCPU_WRITE_NAME(write8 ) = NULL;
	return cpu_ctx;
}

