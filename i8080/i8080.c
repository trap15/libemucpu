/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  Copyright (c)2010 TheLemonMan (Giuseppe)
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "emucpu.h"

#include "i8080.h"
#include "i8080ops.h"

EMUCPU_INTERRUPT( i8080 )
{
	/* TODO: SERIOUSLY FIX THIS, OMFG JESUS */
	i8080ctx_t* ctx = cpu_ctx->data;
	if(ctx->ie) {
		I8080OPCODE_RUN(RST, ctx, trap << 3);
	}
}

EMUCPU_DASM( i8080 )
{

}

EMUCPU_EMULATE( i8080 )
{
	u8 opcode;
	i8080ctx_t* ctx = cpu_ctx->data;
	for(cpu_ctx->cycles = cycles; cpu_ctx->cycles > 0; cpu_ctx->cycles -= opTbl[opcode].cycles) {
		if(cpu_ctx->halt)
			break;
		opcode = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
		opTbl[opcode].execute(ctx, cpu_ctx, opcode);
	}
	return cycles - cpu_ctx->cycles;
}

EMUCPU_RESET( i8080 )
{
	i8080ctx_t* ctx = cpu_ctx->data;
	ctx->sp = 0xF000;
	cpu_ctx->halt = 0;
	ctx->pc = 0;
	ctx->ie = 0;
	ctx->iPending = 0;
	ctx->imask = 0;
	ctx->i7on = 0;
}

EMUCPU_SAVE( i8080 )
{
	i8080ctx_t* ctx = cpu_ctx->data;
	u8* outdata = calloc(sizeof(i8080ctx_t), 1);
	memcpy(outdata, ctx, sizeof(i8080ctx_t));
	return outdata;
}

EMUCPU_RESTORE( i8080 )
{
	i8080ctx_t* ctx = cpu_ctx->data;
	memcpy(ctx, data, sizeof(i8080ctx_t));
}

EMUCPU_GET_INFO( i8080 )
{
	i8080ctx_t* ctx = cpu_ctx->data;
	return EMUCPU_INFO_RET_NO_HANDLE;
}

EMUCPU_SET_INFO( i8080 )
{
	i8080ctx_t* ctx = cpu_ctx->data;
	return EMUCPU_INFO_RET_NO_HANDLE;
}

EMUCPU_INIT( i8080 )
{
	cpu_ctx_t* cpu_ctx;
	i8080ctx_t* ctx;
	cpu_ctx = malloc(sizeof(cpu_ctx_t));
	if(cpu_ctx == NULL)
		return NULL;
	ctx = malloc(sizeof(i8080ctx_t));
	if(ctx == NULL) {
		free(cpu_ctx);
		return NULL;
	}
	cpu_ctx->data = ctx;
	ctx->EMUCPU_READ_NAME( portIn ) = NULL;
	ctx->EMUCPU_WRITE_NAME(portOut) = NULL;
	ctx->EMUCPU_READ_NAME( read8  ) = NULL;
	ctx->EMUCPU_WRITE_NAME(write8 ) = NULL;
	return cpu_ctx;
}

