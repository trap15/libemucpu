/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010~2011 trap15 (Alex Marshall) <trap15@raidenii.net>
 *  Copyright (c)2010 TheLemonMan (Giuseppe)
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "emucpu.h"

#include "v850.h"

#define _signextend(x, b) ((((x) & ((1LL << (b)) - 1)) ^ (1 << ((b) - 1))) - (1 << ((b) - 1)))

#define V850DASMNAME(n)	_v850_dasm_##n
#define V850DASM(n)	static void lol##V850DASMNAME(n)(v850ctx_t* ctx, char *buf, u16 opcode, u16 opcode2)

#define V850FORMAT_I \
	reg1 = opcode & 0x1F; \
	reg2 = (opcode >> 11) & 0x1F;

#define V850FORMAT_II \
	imm = opcode & 0x1F; \
	reg2 = (opcode >> 11) & 0x1F;

#define V850FORMAT_IV \
	disp = opcode & 0x7F; \
	reg2 = (opcode >> 11) & 0x1F;

#define V850FORMAT_V \
	disp = ((opcode & 0x3F) << 16) | (opcode2); \
	reg2 = (opcode >> 11) & 0x1F;

#define V850FORMAT_VI \
	imm = opcode2; \
	reg1 = opcode & 0x1F; \
	reg2 = (opcode >> 11) & 0x1F;

#define V850FORMAT_VII \
	disp = opcode2; \
	reg1 = opcode & 0x1F; \
	reg2 = (opcode >> 11) & 0x1F;

#define V850FORMAT_VIII \
	disp = opcode2; \
	reg1 = opcode & 0x1F; \
	bit = (opcode >> 11) & 0x7;

#define V850FORMAT_IX V850FORMAT_I

#define V850FORMAT_X \
	imm = opcode & 0x1F;

#define V850_OVERFLOWCHK(sa, sb, sc) { \
	if(!(sb & sc) && (sb ^ sa)) \
		ctx->V850_PSW |= V850_FLAG_OVFLW; \
	else \
		ctx->V850_PSW &= ~V850_FLAG_OVFLW; \
}

#define V850_CARRYCHK(a, c) { \
	if((a) > (c)) \
		ctx->V850_PSW |= V850_FLAG_CARRY; \
	else \
		ctx->V850_PSW &= ~V850_FLAG_CARRY; \
}

#define V850_BORROWCHK(b, c) { \
	if((b) < (c)) \
		ctx->V850_PSW |= V850_FLAG_CARRY; \
	else \
		ctx->V850_PSW &= ~V850_FLAG_CARRY; \
}

#define V850_SIGNCHK(a) { \
	if((s32)(a) < 0) \
		ctx->V850_PSW |= V850_FLAG_SIGN; \
	else \
		ctx->V850_PSW &= ~V850_FLAG_SIGN; \
}

#define V850_ZEROCHK(a) { \
	if((a) == 0) \
		ctx->V850_PSW |= V850_FLAG_ZERO; \
	else \
		ctx->V850_PSW &= ~V850_FLAG_ZERO; \
}

#define V850_SATCHK() { \
	if(ctx->V850_PSW & V850_FLAG_OVFLW) \
		ctx->V850_PSW |= V850_FLAG_SAT; \
}

#define V850_CONDITIONCHK(op) { \
	switch(op) { \
		case 0x0: /* BV */ \
			if( (ctx->V850_PSW & V850_FLAG_OVFLW)) \
				break; \
			pass = 0; \
			break; \
		case 0x8: /* BNV */ \
			if(!(ctx->V850_PSW & V850_FLAG_OVFLW)) \
				break; \
			pass = 0; \
			break; \
\
		case 0x1: /* BL */ \
			if( (ctx->V850_PSW & V850_FLAG_CARRY)) \
				break; \
			pass = 0; \
			break; \
		case 0x9: /* BNL */ \
			if(!(ctx->V850_PSW & V850_FLAG_CARRY)) \
				break; \
			pass = 0; \
			break; \
\
		case 0x2: /* BE */ \
			if( (ctx->V850_PSW & V850_FLAG_ZERO)) \
				break; \
			pass = 0; \
			break; \
		case 0xA: /* BNE */ \
			if(!(ctx->V850_PSW & V850_FLAG_ZERO)) \
				break; \
			pass = 0; \
			break; \
\
		case 0x3: /* BNH */ \
			if( ((ctx->V850_PSW & V850_FLAG_ZERO) || (ctx->V850_PSW & V850_FLAG_CARRY))) \
				break; \
			pass = 0; \
			break; \
		case 0xB: /* BH */ \
			if(!((ctx->V850_PSW & V850_FLAG_ZERO) || (ctx->V850_PSW & V850_FLAG_CARRY))) \
				break; \
			pass = 0; \
			break; \
\
		case 0x4: /* BN */ \
			if( (ctx->V850_PSW & V850_FLAG_SIGN)) \
				break; \
			pass = 0; \
			break; \
		case 0xC: /* BP */ \
			if(!(ctx->V850_PSW & V850_FLAG_SIGN)) \
				break; \
			pass = 0; \
			break; \
\
		case 0x5: /* BR */ \
			break; \
		case 0xD: /* BSA */ \
			if( (ctx->V850_PSW & V850_FLAG_SAT)) \
				break; \
			pass = 0; \
			break; \
\
		case 0x6: /* BLT */ \
			if( (!!(ctx->V850_PSW & V850_FLAG_SIGN) ^ !!(ctx->V850_PSW & V850_FLAG_OVFLW))) \
				break; \
			pass = 0; \
			break; \
		case 0xE: /* BGE */ \
			if(!(!!(ctx->V850_PSW & V850_FLAG_SIGN) ^ !!(ctx->V850_PSW & V850_FLAG_OVFLW))) \
				break; \
			pass = 0; \
			break; \
\
		case 0x7: /* BLE */ \
			if( ((!!(ctx->V850_PSW & V850_FLAG_SIGN) ^ !!(ctx->V850_PSW & V850_FLAG_OVFLW)) || (ctx->V850_PSW & V850_FLAG_ZERO))) \
				break; \
			pass = 0; \
			break; \
		case 0xF: /* BGT */ \
			/* Worlds cleanest line of code right here V */ \
			if(!((!!(ctx->V850_PSW & V850_FLAG_SIGN) ^ !!(ctx->V850_PSW & V850_FLAG_OVFLW)) || (ctx->V850_PSW & V850_FLAG_ZERO))) \
				break; \
			pass = 0; \
			break; \
		default: \
			pass = 0; \
			break; \
	} \
}

static const char condname[0x10][3] = {
	"V ", "L ", "E ", "NH",
	"N ", "R ", "LT", "LE",
	"NV", "NL", "NE", "H ",
	"P ", "SA", "GE", "GT",
};

static const char srnames[0x20][6] = {
	"EIPC ", "EIPSW", "FEPC ", "FEPSW",
	"ECR  ", "PSW  ", "SR06 ", "SR07 ",
	"SR08 ", "SR09 ", "SR10 ", "SR11 ",
	"SR12 ", "SR13 ", "SR14 ", "SR15 ",
	"CTPC ", "CTPSW", "DBPC ", "DBPSW",
	"CTBP ", "SR21 ", "SR22 ", "SR23 ",
	"SR24 ", "SR25 ", "SR26 ", "SR27 ",
	"SR28 ", "SR29 ", "SR30 ", "SR31 ",
};

#define V850_ADDCORE(a, b, c) { \
	u32 res; \
	int sa, sb, sc; \
	sb = b >> 31; \
	sc = c >> 31; \
	res = b + c; \
	sa = res >> 31; \
	V850_OVERFLOWCHK(sa, sb, sc) \
	V850_CARRYCHK(res, c) \
	V850_SIGNCHK(res) \
	V850_ZEROCHK(res) \
	a = res; \
}

#define V850_ANDCORE(a, b, c) { \
	a = b & c; \
	ctx->V850_PSW &= ~V850_FLAG_OVFLW; \
	V850_SIGNCHK(a) \
	V850_ZEROCHK(a) \
}

#define V850_CMPCORE(b, c) { \
	int sa, sb, sc; \
	sb = b >> 31; \
	sc = c >> 31; \
	u32 tmp; \
	tmp = b - c; \
	sa = tmp >> 31; \
	V850_OVERFLOWCHK(sa, sb, sc) \
	V850_BORROWCHK(b, c) \
	V850_SIGNCHK(tmp) \
	V850_ZEROCHK(tmp) \
}

#define V850_LDCORE(a, b, d, s) { \
	u32 ea; \
	ea = b + (_signextend(d, 16) & ~(s > 1 ? 1 : 0)); \
	a = _signextend(ctx->EMUCPU_READ_RUN(read32, ea, (1LL << (s << 3)) - 1), s << 3); \
}

#define V850_ORCORE(a, b, c) { \
	a = b | c; \
	ctx->V850_PSW &= ~V850_FLAG_OVFLW; \
	V850_SIGNCHK(a) \
	V850_ZEROCHK(a) \
}

#define V850_SARCORE(a, b, c) { \
	ctx->V850_PSW &= ~(V850_FLAG_OVFLW | V850_FLAG_CARRY); \
	if(c > 0) \
		if(b & (1 << ((c & 0x1F) - 1))) \
			ctx->V850_PSW |= V850_FLAG_CARRY; \
	a = _signextend(b >> (c & 0x1F), 32 - (c & 0x1F)); \
	V850_SIGNCHK(a) \
	V850_ZEROCHK(a) \
}

#define V850_SHLCORE(a, b, c) { \
	ctx->V850_PSW &= ~(V850_FLAG_OVFLW | V850_FLAG_CARRY); \
	if(c > 0) \
		if(b & (1 << (32 - (c & 0x1F)))) \
			ctx->V850_PSW |= V850_FLAG_CARRY; \
	a = b << (c & 0x1F); \
	V850_SIGNCHK(a) \
	V850_ZEROCHK(a) \
}

#define V850_SHRCORE(a, b, c) { \
	ctx->V850_PSW &= ~(V850_FLAG_OVFLW | V850_FLAG_CARRY); \
	if(c > 0) \
		if(b & (1 << ((c & 0x1F) - 1))) \
			ctx->V850_PSW |= V850_FLAG_CARRY; \
	a = b << (c & 0x1F); \
	V850_SIGNCHK(a) \
	V850_ZEROCHK(a) \
}

#define V850_SLDCORE(a, d, s) { \
	u32 ea; \
	ea = ctx->V850_EP + d; \
	a = _signextend(ctx->EMUCPU_READ_RUN(read32, ea, (1LL << (s << 3)) - 1), s << 3); \
}

#define V850_SSTCORE(a, d, s) { \
	u32 ea; \
	ea = ctx->V850_EP + d; \
	ctx->EMUCPU_WRITE_RUN(write32, ea, a, (1LL << (s << 3)) - 1); \
}

#define V850_STCORE(a, b, d, s) { \
	u32 ea; \
	ea = b + (_signextend(d, 16) & ~(s > 1 ? 1 : 0)); \
	ctx->EMUCPU_WRITE_RUN(write32, ea, a, (1LL << (s << 3)) - 1); \
}

#define V850_SUBCORE(a, b, c) { \
	u32 res; \
	int sa, sb, sc; \
	sb = b >> 31; \
	sc = c >> 31; \
	res = b - c; \
	sa = res >> 31; \
	V850_OVERFLOWCHK(sa, sb, sc) \
	V850_BORROWCHK(b, c) \
	V850_SIGNCHK(res) \
	V850_ZEROCHK(res) \
	a = res; \
}

#define V850_XORCORE(a, b, c) { \
	u32 res; \
	res = b ^ c; \
	ctx->V850_PSW &= ~V850_FLAG_OVFLW; \
	V850_SIGNCHK(res) \
	V850_ZEROCHK(res) \
	a = res; \
}

EMUCPU_INTERRUPT( v850 )
{
	v850ctx_t* ctx = cpu_ctx->data;
	switch((trap >> 56) & 0xFF) {
		case 0: /* TRAP */
			ctx->V850_EIPC = ctx->pc; /* compensate */
			ctx->V850_EIPSW = ctx->V850_PSW;
			ctx->V850_ECR &= 0xFFFF0000;
			ctx->V850_ECR |= (trap & 0x1F) | 0x40;
			ctx->V850_PSW |= V850_FLAG_EXCP | V850_FLAG_MIRQA;
			ctx->pc = 0x40;
			if(trap & 0x10)
				ctx->pc += 0x10;
			break;
		case 1: /* Maskable */
			ctx->V850_EIPC = ctx->pc; /* compensate */
			ctx->V850_EIPSW = ctx->V850_PSW;
			ctx->V850_ECR &= 0xFFFF0000;
			ctx->V850_ECR |= trap & 0xFFFF;
			ctx->V850_PSW &= ~V850_FLAG_EXCP;
			ctx->V850_PSW |= V850_FLAG_MIRQA;
			ctx->pc = 0x70; /* I dunno ._. This is sorta model dependant... */
			break;
		case 2: /* NMI */
			ctx->V850_FEPC = ctx->pc; /* compensate */
			ctx->V850_FEPSW = ctx->V850_PSW;
			ctx->V850_ECR &= 0x0000FFFF;
			ctx->V850_ECR |= (trap & 0xFFFF) << 16;
			ctx->V850_PSW &= ~V850_FLAG_EXCP;
			ctx->V850_PSW |= V850_FLAG_NMI | V850_FLAG_MIRQA;
			ctx->pc = 0x10;
			break;
		case 3: /* Exception */
			ctx->V850_EIPC = ctx->pc; /* compensate */
			ctx->V850_EIPSW = ctx->V850_PSW;
			ctx->V850_ECR &= 0xFFFF0000;
			ctx->V850_ECR |= 0x60;
			ctx->V850_PSW |= V850_FLAG_NMI | V850_FLAG_EXCP | V850_FLAG_MIRQA;
			ctx->pc = 0x60;
			break;
	}
}

EMUCPU_DASM( v850 )
{
	u16 opcode, opcode2;
	u32 opc;
	u32 reg1, reg2;
	u32 imm;
	s32 disp;
	u32 bit;
	int i;
	v850ctx_t* ctx = cpu_ctx->data;
	opc = ctx->pc;
	ctx->pc = pc;
	opcode = ctx->EMUCPU_READ_RUN(read32, ctx->pc, 0xFFFF);
	opcode2 = ctx->EMUCPU_READ_RUN(read32, ctx->pc+2, 0xFFFF);
	sprintf(buf, ".dh 0x%04X", opcode);
	switch((opcode >> 5) & 0x3F) {
		case Ob8(00,0000): /* MOV reg1, reg2 */
			V850FORMAT_I
			if((reg1 == 0) && (reg2 == 0))
				sprintf(buf, "NOP");
			else
				sprintf(buf, "MOV     r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,0001): /* NOT reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "NOT     r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,0010): /* DIVH reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "DIVH    r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,0011): /* JMP [reg1] */
			V850FORMAT_I
			sprintf(buf, "JMP     [r%d]", reg1, reg2);
			break;
		case Ob8(00,0100): /* SATSUBR reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "SATSUBR r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,0101): /* SATSUB reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "SATSUB  r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,0110): /* SATADD reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "SATADD  r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,0111): /* MULH reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "MULH    r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,1000): /* OR reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "OR      r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,1001): /* XOR reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "XOR     r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,1010): /* AND reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "AND     r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,1011): /* TST reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "TST     r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,1100): /* SUBR reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "SUBR    r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,1101): /* SUB reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "SUB     r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,1110): /* ADD reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "ADD     r%d, r%d", reg1, reg2);
			break;
		case Ob8(00,1111): /* CMP reg1, reg2 */
			V850FORMAT_I
			sprintf(buf, "CMP     r%d, r%d", reg1, reg2);
			break;
			
		case Ob8(01,0000): /* MOV imm5, reg2 */
			V850FORMAT_II
			imm = _signextend(imm, 5);
			sprintf(buf, "MOV     %s0x%02X, r%d", (s32)imm < 0 ? "-" : "", abs(imm), reg2);
			break;
		case Ob8(01,0001): /* SATADD imm5, reg2 */
			V850FORMAT_II
			imm = _signextend(imm, 5);
			sprintf(buf, "SATADD  %s0x%02X, r%d", (s32)imm < 0 ? "-" : "", abs(imm), reg2);
			break;
		case Ob8(01,0010): /* ADD imm5, reg2 */
			V850FORMAT_II
			imm = _signextend(imm, 5);
			sprintf(buf, "ADD     %s0x%02X, r%d", (s32)imm < 0 ? "-" : "", abs(imm), reg2);
			break;
		case Ob8(01,0011): /* CMP imm5, reg2 */
			V850FORMAT_II
			imm = _signextend(imm, 5);
			sprintf(buf, "CMP     %s0x%02X, r%d", (s32)imm < 0 ? "-" : "", abs(imm), reg2);
			break;
		case Ob8(01,0100): /* SHR imm5, reg2 */
			V850FORMAT_II
			sprintf(buf, "SHR     0x%02X, r%d", imm, reg2);
			break;
		case Ob8(01,0101): /* SAR imm5, reg2 */
			V850FORMAT_II
			sprintf(buf, "SAR     0x%02X, r%d", imm, reg2);
			break;
		case Ob8(01,0110): /* SHL imm5, reg2 */
			V850FORMAT_II
			sprintf(buf, "SHL     0x%02X, r%d", imm, reg2);
			break;
		case Ob8(01,0111): /* MULH imm5, reg2 */
			V850FORMAT_II
			imm = _signextend(imm, 5);
			sprintf(buf, "MULH    %s0x%02X, r%d", (s32)imm < 0 ? "-" : "", abs(imm), reg2);
			break;
			
		case Ob8(01,1000): /* SLD.B disp7[ep], reg2 */
		case Ob8(01,1001):
		case Ob8(01,1010):
		case Ob8(01,1011):
			V850FORMAT_IV
			sprintf(buf, "SLD.B   0x%02X[ep], r%d", disp, reg2);
			break;
		case Ob8(01,1100): /* SST.B reg2, disp7[ep] */
		case Ob8(01,1101):
		case Ob8(01,1110):
		case Ob8(01,1111):
			V850FORMAT_IV
			sprintf(buf, "SST.B   r%d, 0x%02X[ep]", reg2, disp);
			break;
		case Ob8(10,0000): /* SLD.H disp8[ep], reg2 */
		case Ob8(10,0001):
		case Ob8(10,0010):
		case Ob8(10,0011):
			V850FORMAT_IV
			sprintf(buf, "SLD.H   0x%02X[ep], r%d", disp << 1, reg2);
			break;
		case Ob8(10,0100): /* SST.H reg2, disp8[ep] */
		case Ob8(10,0101):
		case Ob8(10,0110):
		case Ob8(10,0111):
			V850FORMAT_IV
			sprintf(buf, "SST.H   r%d, 0x%02X[ep]", reg2, disp << 1);
			break;
		case Ob8(10,1000): /* SLD.W disp8[ep], reg2 */
		case Ob8(10,1001): /* SST.W reg2, disp8[ep] */
		case Ob8(10,1010):
		case Ob8(10,1011):
			if(opcode & 1) {
				V850FORMAT_IV
				sprintf(buf, "SST.W   r%d, 0x%02X[ep]", reg2, (disp & 0x7E) << 1);
			}else{
				V850FORMAT_IV
				sprintf(buf, "SLD.W   0x%02X[ep], r%d", disp << 1, reg2);
			}
			break;
			
		case Ob8(10,1100): /* Bcc disp9 */
		case Ob8(10,1101):
		case Ob8(10,1110):
		case Ob8(10,1111):
			disp = _signextend(((opcode >> 7) & 0x1F0) | ((opcode >> 3) & 0xE), 9);
			sprintf(buf, "B%s     0x%08X", condname[opcode & 0xF], ctx->pc + disp);
			break;
			
		case Ob8(11,0000): /* ADDI imm16, reg1, reg2 */
			V850FORMAT_VI
			imm = _signextend(imm, 16);
			sprintf(buf, "ADDI    %s0x%04X, r%d, r%d", (s32)imm < 0 ? "-" : "", abs(imm), reg1, reg2);
			break;
		case Ob8(11,0001): /* MOVEA imm16, reg1, reg2 */
			V850FORMAT_VI
			imm = _signextend(imm, 16);
			sprintf(buf, "MOVEA   %s0x%04X, r%d, r%d", (s32)imm < 0 ? "-" : "", abs(imm), reg1, reg2);
			break;
		case Ob8(11,0010): /* MOVHI imm16, reg1, reg2 */
			V850FORMAT_VI
			sprintf(buf, "MOVHI   %s0x%04X, r%d, r%d", (s32)imm < 0 ? "-" : "", abs(imm), reg1, reg2);
			break;
		case Ob8(11,0011): /* SATSUBI imm16, reg1, reg2 */
			V850FORMAT_VI
			imm = _signextend(imm, 16);
			sprintf(buf, "SATSUBI %s0x%04X, r%d, r%d", (s32)imm < 0 ? "-" : "", abs(imm), reg1, reg2);
			break;
		case Ob8(11,0100): /* ORI imm16, reg1, reg2 */
			V850FORMAT_VI
			sprintf(buf, "ORI     0x%04X, r%d, r%d", imm, reg1, reg2);
			break;
		case Ob8(11,0101): /* XORI imm16, reg1, reg2 */
			V850FORMAT_VI
			sprintf(buf, "XORI    0x%04X, r%d, r%d", imm, reg1, reg2);
			break;
		case Ob8(11,0110): /* ANDI imm16, reg1, reg2 */
			V850FORMAT_VI
			sprintf(buf, "ANDI    0x%04X, r%d, r%d", imm, reg1, reg2);
			break;
		case Ob8(11,0111): /* MULHI imm16, reg1, reg2 */
			V850FORMAT_VI
			imm = _signextend(imm, 16);
			sprintf(buf, "MULHI   %s0x%04X, r%d, r%d", (s32)imm < 0 ? "-" : "", abs(imm), reg1, reg2);
			break;
			
		case Ob8(11,1000): /* LD.B imm16[reg1], reg2 */
			V850FORMAT_VII
			disp = _signextend(disp, 16);
			sprintf(buf, "LD.B    %s0x%04X[r%d], r%d", disp < 0 ? "-" : "", abs(disp), reg1, reg2);
			break;
		case Ob8(11,1001): /* LD.H imm16[reg1], reg2 */
				   /* LD.W imm16[reg1], reg2 */
			V850FORMAT_VII
			disp = _signextend(disp, 16);
			sprintf(buf, "LD.%s    %s0x%04X[r%d], r%d", (disp & 1) ? "W" : "H", disp < 0 ? "-" : "", abs(disp) & ~1, reg1, reg2);
			break;
		case Ob8(11,1010): /* ST.B reg2, imm16[reg1] */
			V850FORMAT_VII
			disp = _signextend(disp, 16);
			sprintf(buf, "ST.B    r%d, %s0x%04X[r%d]", reg2, disp < 0 ? "-" : "", abs(disp), reg1);
			break;
		case Ob8(11,1011): /* ST.H reg2, imm16[reg1] */
				   /* ST.W reg2, imm16[reg1] */
			V850FORMAT_VII
			disp = _signextend(disp, 16);
			sprintf(buf, "ST.%s    r%d, %s0x%04X[r%d]", (disp & 1) ? "W" : "H", reg2, disp < 0 ? "-" : "", abs(disp & ~1), reg1);
			break;
			
		case Ob8(11,1100): /* JARL disp22, reg2 */
		case Ob8(11,1101): /* JR disp22 */
			V850FORMAT_V
			disp &= ~1;
			if((opcode >> 11) & 0x1F)
				sprintf(buf, "JARL    0x%08X, r%d", (unsigned int)(ctx->pc + _signextend(disp, 22)), reg2);
			else
				sprintf(buf, "JR      0x%08X", (unsigned int)(ctx->pc + _signextend(disp, 22)));
			break;
			
		case Ob8(11,1110): /* SET1 bit#3, disp16[reg1] */
				   /* CLR1 bit#3, disp16[reg1] */
				   /* NOT1 bit#3, disp16[reg1] */
				   /* TST1 bit#3, disp16[reg1] */
			V850FORMAT_VIII
			disp = _signextend(disp, 16);
			switch(opcode >> 14) {
				case 0:
					sprintf(buf, "SET1    %d, %s0x%04X[r%d]", bit, disp < 0 ? "-" : "", abs(disp), reg1);
					break;
				case 1:
					sprintf(buf, "NOT1    %d, %s0x%04X[r%d]", bit, disp < 0 ? "-" : "", abs(disp), reg1);
					break;
				case 2:
					sprintf(buf, "CLR1    %d, %s0x%04X[r%d]", bit, disp < 0 ? "-" : "", abs(disp), reg1);
					break;
				case 3:
					sprintf(buf, "TST1    %d, %s0x%04X[r%d]", bit, disp < 0 ? "-" : "", abs(disp), reg1);
					break;
			}
			break;
		case Ob8(11,1111): /* SETF cccc, reg2 */
				   /* LDSR reg2, regID */
				   /* STSR regID, reg2 */
				   /* SHR reg1, reg2 */
				   /* SAR reg1, reg2 */
				   /* SHL reg1, reg2 */
				   /* TRAP vector */
				   /* HALT */
				   /* RETI */
				   /* DI */
				   /* EI */
				   /* Illegal */
			switch((opcode2 >> 5) & 0x3F) {
				case Ob8(00,0000): /* SETF cccc, reg2 */
					V850FORMAT_IX
					sprintf(buf, "SETF    0x%01X, r%d", reg1, reg2);
					break;
				case Ob8(00,0001): /* LDSR */
					V850FORMAT_IX
					sprintf(buf, "LDSR    r%d, %s", reg1, srnames[reg2]);
					break;
				case Ob8(00,0010): /* STSR */
					V850FORMAT_IX
					sprintf(buf, "STSR    %s, r%d", srnames[reg1], reg2);
					break;
				case Ob8(00,0100): /* SHR */
					V850FORMAT_II
					sprintf(buf, "SHR     0x%02X, r%d", imm, reg2);
					break;
				case Ob8(00,0101): /* SAR */
					V850FORMAT_II
					sprintf(buf, "SAR     0x%02X, r%d", imm, reg2);
					break;
				case Ob8(00,0110): /* SHL */
					V850FORMAT_II
					sprintf(buf, "SHL     0x%02X, r%d", imm, reg2);
					break;
				case Ob8(00,1000): /* TRAP */
					V850FORMAT_X
					sprintf(buf, "TRAP    %d", imm);
					break;
				case Ob8(00,1001): /* HALT */
					sprintf(buf, "HALT");
					break;
				case Ob8(00,1010): /* RETI */
					sprintf(buf, "RETI");
					break;
				case Ob8(00,1011): /* DI */
						   /* EI */
					if(opcode & 0x8000)
						sprintf(buf, "EI");
					else
						sprintf(buf, "DI");
					break;
				case Ob8(00,1100): case Ob8(00,1101): case Ob8(00,1110): case Ob8(00,1111): /* Illegal */
				case Ob8(01,0000): case Ob8(01,0001): case Ob8(01,0010): case Ob8(01,0011):
				case Ob8(01,0100): case Ob8(01,0101): case Ob8(01,0110): case Ob8(01,0111):
				case Ob8(01,1000): case Ob8(01,1001): case Ob8(01,1010): case Ob8(01,1011):
				case Ob8(01,1100): case Ob8(01,1101): case Ob8(01,1110): case Ob8(01,1111):
				case Ob8(10,0000): case Ob8(10,0001): case Ob8(10,0010): case Ob8(10,0011):
				case Ob8(10,0100): case Ob8(10,0101): case Ob8(10,0110): case Ob8(10,0111):
				case Ob8(10,1000): case Ob8(10,1001): case Ob8(10,1010): case Ob8(10,1011):
				case Ob8(10,1100): case Ob8(10,1101): case Ob8(10,1110): case Ob8(10,1111):
				case Ob8(11,0000): case Ob8(11,0001): case Ob8(11,0010): case Ob8(11,0011):
				case Ob8(11,0100): case Ob8(11,0101): case Ob8(11,0110): case Ob8(11,0111):
				case Ob8(11,1000): case Ob8(11,1001): case Ob8(11,1010): case Ob8(11,1011):
				case Ob8(11,1100): case Ob8(11,1101): case Ob8(11,1110): case Ob8(11,1111):
					sprintf(buf, ".dh 0x%04X, 0x%04X", opcode, opcode2);
					break;
			}
			break;
	}
	ctx->pc = opc;
}

#define GPR_LOCK_CHECK(o) do { \
	if(o != 0) { \
		if(ctx->gprstall[o] == -2) { \
			stalled = 1; \
		}else if(ctx->gprstall[o] == -1) { \
			o = ctx->gpr[o]; \
		}else{ \
			o = ctx->gprstall[o]; \
		} \
	} \
} while(0)

#define GPR_LOCK_SET_SHORTPATH() do { \
	if(pipe->writeback != 0) \
		ctx->gprstall[pipe->writeback] = pipe->result; \
	else \
		GPR_UNLOCK(pipe->writeback); \
} while(0)

#define GPR_LOCK(o) do { \
	ctx->gprstall[o] = -2; \
} while(0)

#define GPR_UNLOCK(o) do { \
	ctx->gprstall[o] = -1; \
} while(0)

#define SET_PIPELINE_QUEUE(stge) do { \
	int tmpcond; \
	pipe->inqueue = -1; \
	if((stge != V850_PIPELINE_HAZARD) && (stge != V850_PIPELINE_FETCH)) { \
		tmpcond = (ctx->pipequeue[stge] == NULL); \
	}else{ \
		tmpcond = (ctx->pipequeue[V850_PIPELINE_HAZARD] == NULL) && (ctx->pipequeue[V850_PIPELINE_FETCH] == NULL); \
	} \
	if(tmpcond) { \
		pipe->laststage = pipe->stage; \
		if(pipe->laststage < V850_PIPELINE_ST_COUNT) \
			ctx->pipequeue[pipe->laststage] = NULL; \
		pipe->stage = stge; \
		ctx->pipequeue[stge] = pipe; \
	}else \
		pipe->inqueue = stge; \
} while(0)

#define HANDLE_PIPELINE_QUEUING() do { \
	int queue = pipe->inqueue; \
	if(pipe->inqueue != -1) { \
		SET_PIPELINE_QUEUE(queue); \
		return; \
	} \
} while(0)

#define REMOVE_PIPELINE_ENTRY(p) do { \
	if(ctx->pipequeue[(p)->stage] == (p)) \
		ctx->pipequeue[(p)->stage] = NULL; \
	(p)->stage = V850_PIPELINE_DONE; \
} while(0)

static void _v850_pipe_fetch(cpu_ctx_t* cpu_ctx, v850ctx_t* ctx, _v850pipeline_t* pipe)
{
	if(ctx->pipequeue[V850_PIPELINE_HAZARD] != NULL) /* Hazards take same pipe slot as fetch */
		return;
	HANDLE_PIPELINE_QUEUING();

	pipe->opcode = ctx->EMUCPU_READ_RUN(read32, ctx->pc, 0xFFFF);
	pipe->pc = ctx->pc;
	ctx->pc += 2;
	if((pipe->opcode & (3 << 9)) == (3 << 9)) { /* If bits 9 and 10 are set, it's a 32bit op */
		if(!(ctx->pc & 2) && ctx->branched) /* Alignment hazard */ {
			SET_PIPELINE_QUEUE(V850_PIPELINE_HAZARD);
		}else{
			pipe->opcode2 = ctx->EMUCPU_READ_RUN(read32, ctx->pc, 0xFFFF);
			ctx->pc += 2;
			SET_PIPELINE_QUEUE(V850_PIPELINE_DECODE);
		}
	}else{
		SET_PIPELINE_QUEUE(V850_PIPELINE_DECODE);
	}
	ctx->branched = 0;
}

static void _v850_pipe_hazard(cpu_ctx_t* cpu_ctx, v850ctx_t* ctx, _v850pipeline_t* pipe)
{
	HANDLE_PIPELINE_QUEUING();

	pipe->opcode2 = ctx->EMUCPU_READ_RUN(read32, pipe->pc + 2, 0xFFFF);
	SET_PIPELINE_QUEUE(V850_PIPELINE_DECODE);
}

static void _v850_pipe_decode(cpu_ctx_t* cpu_ctx, v850ctx_t* ctx, _v850pipeline_t* pipe)
{
	int stalled = 0;
	HANDLE_PIPELINE_QUEUING();

	switch((pipe->opcode >> 5) & 0x3F) {
		case Ob8(00,0000): case Ob8(00,0001): case Ob8(00,0010): case Ob8(00,0011):
		case Ob8(00,0100): case Ob8(00,0101): case Ob8(00,0110): case Ob8(00,0111):
		case Ob8(00,1000): case Ob8(00,1001): case Ob8(00,1010): case Ob8(00,1011):
		case Ob8(00,1100): case Ob8(00,1101): case Ob8(00,1110): case Ob8(00,1111):
			/* Format I */
			pipe->oper[0] = pipe->opcode & 0x1F;
			pipe->writeback = (pipe->opcode >> 11) & 0x1F;
			pipe->oper[1] = pipe->writeback;
			GPR_LOCK_CHECK(pipe->oper[0]);
			GPR_LOCK_CHECK(pipe->oper[1]);
			GPR_LOCK(pipe->writeback);
			break;
		case Ob8(01,0000): case Ob8(01,0001): case Ob8(01,0010): case Ob8(01,0011):
		case Ob8(01,0100): case Ob8(01,0101): case Ob8(01,0110): case Ob8(01,0111):
			/* Format II */
			pipe->oper[0] = _signextend(pipe->opcode & 0x1F, 5);
			pipe->writeback = (pipe->opcode >> 11) & 0x1F;
			pipe->oper[1] = pipe->writeback;
			GPR_LOCK_CHECK(pipe->oper[1]);
			GPR_LOCK(pipe->writeback);
			break;
		case Ob8(01,1000): case Ob8(01,1001): case Ob8(01,1010): case Ob8(01,1011):
		case Ob8(01,1100): case Ob8(01,1101): case Ob8(01,1110): case Ob8(01,1111):
		case Ob8(10,0000): case Ob8(10,0001): case Ob8(10,0010): case Ob8(10,0011):
		case Ob8(10,0100): case Ob8(10,0101): case Ob8(10,0110): case Ob8(10,0111):
		case Ob8(10,1000): case Ob8(10,1001): case Ob8(10,1010): case Ob8(10,1011):
			/* Format IV */
			pipe->oper[0] = pipe->opcode & 0x7F;
			pipe->writeback = (pipe->opcode >> 11) & 0x1F;
			GPR_LOCK(pipe->writeback);
			break;
		case Ob8(10,1100): case Ob8(10,1101): case Ob8(10,1110): case Ob8(10,1111):
			/* Format III */
			pipe->oper[0] = pipe->opcode & 0xF;
			pipe->oper[1] = ((pipe->opcode >> 7) & 0x1F0) | ((pipe->opcode >> 3) & 0xE);
			pipe->oper[1] = _signextend(pipe->oper[1], 9);
			pipe->writeback = 0;
			break;
		case Ob8(11,0000): case Ob8(11,0001): case Ob8(11,0010): case Ob8(11,0011):
		case Ob8(11,0100): case Ob8(11,0101): case Ob8(11,0110): case Ob8(11,0111):
			/* Format VI */
			pipe->oper[0] = pipe->opcode & 0x1F;
			pipe->writeback = (pipe->opcode >> 11) & 0x1F;
			pipe->oper[1] = pipe->opcode2;
			GPR_LOCK_CHECK(pipe->oper[0]);
			GPR_LOCK(pipe->writeback);
			break;
		case Ob8(11,1000): case Ob8(11,1001): case Ob8(11,1010): case Ob8(11,1011):
			/* Format VII */
			pipe->oper[0] = pipe->opcode & 0x1F;
			pipe->writeback = (pipe->opcode >> 11) & 0x1F;
			pipe->oper[1] = _signextend(pipe->opcode2, 16);
			GPR_LOCK_CHECK(pipe->oper[0]);
			GPR_LOCK(pipe->writeback);
			break;
		case Ob8(11,1100): case Ob8(11,1101):
			/* Format V */
			pipe->writeback = (pipe->opcode >> 11) & 0x1F;
			pipe->oper[0] = _signextend(((pipe->opcode & 0x3F) << 16) | pipe->opcode2, 22);
			GPR_LOCK(pipe->writeback);
			break;
		case Ob8(11,1110):
			/* Format VIII */
			pipe->oper[0] = (pipe->opcode >> 11) & 0x7;
			pipe->oper[1] = pipe->opcode & 0x1F;
			pipe->oper[2] = _signextend(pipe->opcode2, 16);
			GPR_LOCK_CHECK(pipe->oper[1]);
			pipe->writeback = 0;
			break;
		case Ob8(11,1111):
			/* Format IX/X */
			pipe->oper[0] = pipe->opcode & 0x1F;
			pipe->writeback = 0;
			if((pipe->opcode2 & 0x780) < 0x100) { /* Format IX */
				pipe->writeback = (pipe->opcode >> 11) & 0x1F;
				switch((pipe->opcode2 >> 5) & 0x3F) { /* hax :( */
					case Ob8(00,0000): /* SETF */
					case Ob8(00,1000): /* TRAP */
						break;
					case Ob8(00,0010): /* STSR */
						pipe->oper[0] = ctx->sysr[pipe->oper[0]];
						break;
					default:
						GPR_LOCK_CHECK(pipe->oper[0]);
						break;
				}
				GPR_LOCK(pipe->writeback);
			}
			break;
	}
	if(!stalled)
		SET_PIPELINE_QUEUE(V850_PIPELINE_EXEC);
}

static void _v850_pipe_exec(cpu_ctx_t* cpu_ctx, v850ctx_t* ctx, _v850pipeline_t* pipe)
{
	int i;
	int pass = 1;
	int stalled = 0;
	HANDLE_PIPELINE_QUEUING();

	switch((pipe->opcode >> 5) & 0x3F) {
		case Ob8(00,0000): /* MOV reg1, reg2 */
			pipe->result = pipe->oper[0];
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,0001): /* NOT reg1, reg2 */
			pipe->result = ~(pipe->oper[0]);
			V850_SIGNCHK(pipe->result)
			V850_ZEROCHK(pipe->result)
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,0010): /* DIVH reg1, reg2 */
			/* TODO: Actually implement this properly to use all cycles */
			if(pipe->stageprog == 0) {
				int sa, sb, sc;
				s16 c = pipe->oper[0] & 0xFFFF;
				sb = pipe->oper[1] >> 31;
				sc = c >> 15;
				ctx->V850_PSW &= ~V850_FLAG_OVFLW;
				if((c == 0) || ((c == -1) && (pipe->oper[1] == 0x80000000))) {
					if(c == -1)
						pipe->result = pipe->oper[1];
					ctx->V850_PSW |= V850_FLAG_OVFLW;
				}else{
					pipe->result = pipe->oper[1] / c;
					sa = pipe->result >> 31;
					V850_OVERFLOWCHK(sa, sb, sc)
				}
				pipe->stageprog++;
			}else if(pipe->stageprog == 35) {
				V850_SIGNCHK(pipe->result)
				V850_ZEROCHK(pipe->result)
				SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			}else{
				pipe->stageprog++;
			}
			break;
		case Ob8(00,0011): /* JMP [reg1] */
			ctx->pc = pipe->oper[0];
			for(i = 0; i < 2; i++) {
				REMOVE_PIPELINE_ENTRY(pipe + i);
			}
			pipe->mask = 0;
			GPR_UNLOCK(pipe->writeback);
			pipe->writeback = 0;
			ctx->branched = 1;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,0100): /* SATSUBR reg1, reg2 */
			V850_SUBCORE(pipe->result, pipe->oper[0], pipe->oper[1])
			V850_SATCHK()
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,0101): /* SATSUB reg1, reg2 */
			V850_SUBCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			V850_SATCHK()
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,0110): /* SATADD reg1, reg2 */
			V850_ADDCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			V850_SATCHK()
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,0111): /* MULH reg1, reg2 */
			/* TODO: Actually implement this properly to use both cycles */
			if(pipe->stageprog == 0) {
				pipe->result = (pipe->oper[1] & 0xFFFF) * (pipe->oper[0] & 0xFFFF);
				pipe->stageprog++;
			}else if(pipe->stageprog == 1) {
				GPR_LOCK_SET_SHORTPATH();
				SET_PIPELINE_QUEUE(V850_PIPELINE_WRITE);
			}
			pipe->mask = 0;
			break;
		case Ob8(00,1000): /* OR reg1, reg2 */
			V850_ORCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,1001): /* XOR reg1, reg2 */
			V850_XORCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,1010): /* AND reg1, reg2 */
			V850_ANDCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,1011): /* TST reg1, reg2 */
			V850_ANDCORE(i, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			GPR_UNLOCK(pipe->writeback);
			pipe->writeback = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,1100): /* SUBR reg1, reg2 */
			V850_SUBCORE(pipe->result, pipe->oper[0], pipe->oper[1])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,1101): /* SUB reg1, reg2 */
			V850_SUBCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,1110): /* ADD reg1, reg2 */
			V850_ADDCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(00,1111): /* CMP reg1, reg2 */
			V850_SUBCORE(i, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			GPR_UNLOCK(pipe->writeback);
			pipe->writeback = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;

		case Ob8(01,0000): /* MOV imm5, reg2 */
			pipe->result = pipe->oper[0];
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(01,0001): /* SATADD imm5, reg2 */
			V850_ADDCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			V850_SATCHK()
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(01,0010): /* ADD imm5, reg2 */
			V850_ADDCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(01,0011): /* CMP imm5, reg2 */
			V850_SUBCORE(i, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			GPR_UNLOCK(pipe->writeback);
			pipe->writeback = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(01,0100): /* SHR imm5, reg2 */
			V850_SHRCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(01,0101): /* SAR imm5, reg2 */
			V850_SARCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(01,0110): /* SHL imm5, reg2 */
			V850_SHLCORE(pipe->result, pipe->oper[1], pipe->oper[0])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(01,0111): /* MULH imm5, reg2 */
			/* TODO: Hack up the memory cycle handler to do the second stage instead
			 *       of eating up another cycle >_> */
			/* TODO: Actually implement this properly to use both cycles */
			if(pipe->stageprog == 0) {
				pipe->result = (pipe->oper[1] & 0xFFFF) * (pipe->oper[0] & 0xFFFF);
				pipe->stageprog++;
			}else if(pipe->stageprog == 1) {
				GPR_LOCK_SET_SHORTPATH();
				pipe->stageprog = 0;
				SET_PIPELINE_QUEUE(V850_PIPELINE_WRITE);
			}
			break;

		case Ob8(01,1000): /* SLD.B disp7[ep], reg2 */
		case Ob8(01,1001):
		case Ob8(01,1010):
		case Ob8(01,1011):
			pipe->oper[3] = 30; /* EP */
			GPR_LOCK_CHECK(pipe->oper[3]);
			if(!stalled) {
				pipe->ea = pipe->oper[0] + pipe->oper[3];
				pipe->dir = 0;
				pipe->mask = 0xFF;
				GPR_UNLOCK(pipe->writeback);
				pipe->writeback = 0;
				SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			}
			break;
		case Ob8(01,1100): /* SST.B reg2, disp7[ep] */
		case Ob8(01,1101):
		case Ob8(01,1110):
		case Ob8(01,1111):
			pipe->oper[3] = 30; /* EP */
			GPR_LOCK_CHECK(pipe->oper[3]);
			if(!stalled) {
				pipe->ea = pipe->oper[0] + pipe->oper[3];
				pipe->dir = 1;
				pipe->mask = 0xFF;
				SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			}
			break;
		case Ob8(10,0000): /* SLD.H disp8[ep], reg2 */
		case Ob8(10,0001):
		case Ob8(10,0010):
		case Ob8(10,0011):
			pipe->oper[3] = 30; /* EP */
			GPR_LOCK_CHECK(pipe->oper[3]);
			if(!stalled) {
				pipe->ea = (pipe->oper[0] << 1) + pipe->oper[3];
				pipe->dir = 0;
				pipe->mask = 0xFFFF;
				GPR_UNLOCK(pipe->writeback);
				pipe->writeback = 0;
				SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			}
			break;
		case Ob8(10,0100): /* SST.H reg2, disp8[ep] */
		case Ob8(10,0101):
		case Ob8(10,0110):
		case Ob8(10,0111):
			pipe->oper[3] = 30; /* EP */
			GPR_LOCK_CHECK(pipe->oper[3]);
			if(!stalled) {
				pipe->ea = (pipe->oper[0] << 1) + pipe->oper[3];
				pipe->dir = 1;
				pipe->mask = 0xFFFF;
				SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			}
			break;
		case Ob8(10,1000): /* SLD.W disp8[ep], reg2 */
		case Ob8(10,1001): /* SST.W reg2, disp8[ep] */
		case Ob8(10,1010):
		case Ob8(10,1011):
			pipe->oper[3] = 30; /* EP */
			GPR_LOCK_CHECK(pipe->oper[3]);
			if(!stalled) {
				pipe->ea = ((pipe->oper[0] & 0x7E) << 1) + pipe->oper[3];
				pipe->dir = pipe->oper[0] & 1;
				pipe->mask = 0xFFFFFFFF;
				pipe->writeback = -!(pipe->oper[0] & 1);
				SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			}
			break;

		case Ob8(10,1100): /* Bcc disp9 */
		case Ob8(10,1101):
		case Ob8(10,1110):
		case Ob8(10,1111):
			V850_CONDITIONCHK(pipe->opcode & 0xF)
			if(pass) {
				ctx->pc = pipe->pc + pipe->oper[1];
				for(i = 0; i < 2; i++) {
					REMOVE_PIPELINE_ENTRY(pipe + i);
				}
				ctx->branched = 1;
			}
			pipe->mask = 0;
			GPR_UNLOCK(pipe->writeback);
			pipe->writeback = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;

		case Ob8(11,0000): /* ADDI imm16, reg1, reg2 */
			V850_ADDCORE(pipe->result, pipe->oper[0], pipe->oper[1])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(11,0001): /* MOVEA imm16, reg1, reg2 */
			pipe->result = pipe->oper[0] + pipe->oper[1];
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(11,0010): /* MOVHI imm16, reg1, reg2 */
			pipe->result = pipe->oper[0] + (pipe->oper[1] << 16);
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(11,0011): /* SATSUBI imm16, reg1, reg2 */
			V850_SUBCORE(pipe->result, pipe->oper[0], pipe->oper[1])
			V850_SATCHK()
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(11,0100): /* ORI imm16, reg1, reg2 */
			V850_ORCORE(pipe->result, pipe->oper[0], pipe->oper[1])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(11,0101): /* XORI imm16, reg1, reg2 */
			V850_XORCORE(pipe->result, pipe->oper[0], pipe->oper[1])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(11,0110): /* ANDI imm16, reg1, reg2 */
			V850_ANDCORE(pipe->result, pipe->oper[0], pipe->oper[1])
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(11,0111): /* MULHI imm16, reg1, reg2 */
			/* TODO: Hack up the memory cycle handler to deal with the second
                         *       cycle instead. */
			/* TODO: Actually implement this properly to use both cycles */
			if(pipe->stageprog == 0) {
				pipe->result = (pipe->oper[1] & 0xFFFF) * (pipe->oper[0] & 0xFFFF);
				pipe->stageprog++;
			}else if(pipe->stageprog == 1) {
				GPR_LOCK_SET_SHORTPATH();
				pipe->stageprog = 0;
				SET_PIPELINE_QUEUE(V850_PIPELINE_WRITE);
			}
			pipe->mask = 0;
			break;

		case Ob8(11,1000): /* LD.B imm16[reg1], reg2 */
			pipe->ea = pipe->oper[0] + pipe->oper[1];
			pipe->dir = 0;
			pipe->mask = 0xFF;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(11,1001): /* LD.H imm16[reg1], reg2 */
				   /* LD.W imm16[reg1], reg2 */
			pipe->ea = pipe->oper[0] + (pipe->oper[1] & ~1);
			pipe->dir = 0;
			pipe->mask = (pipe->oper[1] & 1) ? 0xFFFFFFFF : 0xFFFF;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(11,1010): /* ST.B reg2, imm16[reg1] */
			pipe->ea = pipe->oper[0] + pipe->oper[1];
			pipe->dir = 1;
			pipe->mask = 0xFF;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
		case Ob8(11,1011): /* ST.H reg2, imm16[reg1] */
				   /* ST.W reg2, imm16[reg1] */
			pipe->ea = pipe->oper[0] + (pipe->oper[1] & ~1);
			pipe->dir = 1;
			pipe->mask = (pipe->oper[1] & 1) ? 0xFFFFFFFF : 0xFFFF;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
			
		case Ob8(11,1100): /* JARL disp22, reg2 */
		case Ob8(11,1101): /* JR disp22 */
			ctx->pc = pipe->pc + pipe->oper[0];
			pipe->result = pipe->pc + 4;
			for(i = 0; i < 2; i++) {
				REMOVE_PIPELINE_ENTRY(pipe + i);
			}
			ctx->branched = 1;
			pipe->mask = 0;
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;

		case Ob8(11,1110): /* SET1 bit#3, disp16[reg1] */
				   /* CLR1 bit#3, disp16[reg1] */
				   /* NOT1 bit#3, disp16[reg1] */
				   /* TST1 bit#3, disp16[reg1] */
			/* TODO: Cycles are a guess! Please verify! */
			if(pipe->stageprog == 0) {
				pipe->mask = 1 << pipe->oper[0];
				pipe->ea = pipe->oper[1] + pipe->oper[2];
				pipe->stageprog++;
			}else if(pipe->stageprog == 1) {
				pipe->oper[3] = ctx->EMUCPU_READ_RUN(read32, pipe->ea, pipe->mask);
				pipe->stageprog++;
			}else if(pipe->stageprog == 2) {
				V850_ZEROCHK(pipe->oper[3])
				switch((pipe->opcode >> 14) & 3) {
					case Ob4(00): /* SET1 */
						pipe->result = 0xFF;
						break;
					case Ob4(01): /* NOT1 */
						pipe->result = ~pipe->oper[3];
						break;
					case Ob4(10): /* CLR1 */
						pipe->result = 0;
						break;
					case Ob4(11): /* TST1 */
						pipe->mask = 0;
						break;
				}
				pipe->stageprog++;
			}else if(pipe->stageprog == 3) {
				pipe->stageprog = 0;
				SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			}
			break;
		case Ob8(11,1111): /* SETF cccc, reg2 */
				   /* LDSR reg2, regID */
				   /* STSR regID, reg2 */
				   /* SHR reg1, reg2 */
				   /* SAR reg1, reg2 */
				   /* SHL reg1, reg2 */
				   /* TRAP vector */
				   /* HALT */
				   /* RETI */
				   /* DI */
				   /* EI */
				   /* Illegal */
			switch((pipe->opcode2 >> 5) & 0x3F) {
				case Ob8(00,0000): /* SETF cccc, reg2 */
					V850_CONDITIONCHK(pipe->oper[0])
					pipe->result = pass;
					pipe->mask = 0;
					break;
				case Ob8(00,0001): /* LDSR */
					switch(pipe->writeback) {
						case 0:
							ctx->V850_EIPC = pipe->oper[0] & ~0xFF000000;
							break;
						case 1:
							ctx->V850_EIPSW = pipe->oper[0] & ~0xFFFFFF00;
							break;
						case 2:
							ctx->V850_FEPC = pipe->oper[0] & ~0xFF000000;
							break;
						case 3:
							ctx->V850_FEPSW = pipe->oper[0] & ~0xFFFFFF00;
							break;
						case 5:
							ctx->V850_PSW = pipe->oper[0] & ~0xFFFFFF00;
							break;
						default:
							/* Freak out */
							break;
					}
					GPR_UNLOCK(pipe->writeback);
					pipe->writeback = 0;
					pipe->mask = 0;
					break;
				case Ob8(00,0010): /* STSR */
					switch(pipe->oper[0]) {
						case 0:
						case 1:
						case 2:
						case 3:
						case 4:
						case 5:
							pipe->result = ctx->sysr[pipe->oper[0]];
							break;
						default:
							/* Freak out */
							break;
					}
					pipe->mask = 0;
					break;
				case Ob8(00,0100): /* SHR */
					V850_SHRCORE(pipe->result, pipe->oper[1], pipe->oper[0])
					pipe->mask = 0;
					break;
				case Ob8(00,0101): /* SAR */
					V850_SARCORE(pipe->result, pipe->oper[1], pipe->oper[0])
					pipe->mask = 0;
					break;
				case Ob8(00,0110): /* SHL */
					V850_SHLCORE(pipe->result, pipe->oper[1], pipe->oper[0])
					pipe->mask = 0;
					break;
				case Ob8(00,1000): /* TRAP */
					EMUCPU_INTERRUPT_RUN( v850, cpu_ctx, pipe->oper[0] | (0LL << 56));
					pipe->mask = 0;
					GPR_UNLOCK(pipe->writeback);
					pipe->writeback = 0;
					break;
				case Ob8(00,1001): /* HALT */
					cpu_ctx->halt = 1;
					pipe->mask = 0;
					GPR_UNLOCK(pipe->writeback);
					pipe->writeback = 0;
					break;
				case Ob8(00,1010): /* RETI */
					if((ctx->V850_PSW & V850_FLAG_NMI) && !(ctx->V850_PSW & V850_FLAG_EXCP)) {
						ctx->pc = ctx->V850_FEPC;
						ctx->V850_PSW = ctx->V850_FEPSW;
					}else{
						ctx->pc = ctx->V850_EIPC;
						ctx->V850_PSW = ctx->V850_EIPSW;
					}
					pipe->mask = 0;
					GPR_UNLOCK(pipe->writeback);
					pipe->writeback = 0;
					break;
				case Ob8(00,1011): /* DI */
						   /* EI */
					if(pipe->opcode & 0x8000) {
						ctx->V850_PSW &= ~V850_FLAG_MIRQA;
					}else{
						ctx->V850_PSW |=  V850_FLAG_MIRQA;
					}
					break;
				case Ob8(00,1100): case Ob8(00,1101): case Ob8(00,1110): case Ob8(00,1111): /* Illegal */
				case Ob8(01,0000): case Ob8(01,0001): case Ob8(01,0010): case Ob8(01,0011):
				case Ob8(01,0100): case Ob8(01,0101): case Ob8(01,0110): case Ob8(01,0111):
				case Ob8(01,1000): case Ob8(01,1001): case Ob8(01,1010): case Ob8(01,1011):
				case Ob8(01,1100): case Ob8(01,1101): case Ob8(01,1110): case Ob8(01,1111):
				case Ob8(10,0000): case Ob8(10,0001): case Ob8(10,0010): case Ob8(10,0011):
				case Ob8(10,0100): case Ob8(10,0101): case Ob8(10,0110): case Ob8(10,0111):
				case Ob8(10,1000): case Ob8(10,1001): case Ob8(10,1010): case Ob8(10,1011):
				case Ob8(10,1100): case Ob8(10,1101): case Ob8(10,1110): case Ob8(10,1111):
				case Ob8(11,0000): case Ob8(11,0001): case Ob8(11,0010): case Ob8(11,0011):
				case Ob8(11,0100): case Ob8(11,0101): case Ob8(11,0110): case Ob8(11,0111):
				case Ob8(11,1000): case Ob8(11,1001): case Ob8(11,1010): case Ob8(11,1011):
				case Ob8(11,1100): case Ob8(11,1101): case Ob8(11,1110): case Ob8(11,1111):
					EMUCPU_INTERRUPT_RUN( v850, cpu_ctx, 3LL << 56);
					break;
			}
			SET_PIPELINE_QUEUE(V850_PIPELINE_MEM);
			break;
	}
}

static void _v850_pipe_mem(cpu_ctx_t* cpu_ctx, v850ctx_t* ctx, _v850pipeline_t* pipe)
{
	HANDLE_PIPELINE_QUEUING();

	if(pipe->mask != 0) {
		if(pipe->dir) {
			ctx->EMUCPU_WRITE_RUN(write32, pipe->ea, pipe->result, pipe->mask);
		}else{
			pipe->result = ctx->EMUCPU_READ_RUN(read32, pipe->ea, pipe->mask);
			GPR_LOCK_SET_SHORTPATH();
		}
	}
	SET_PIPELINE_QUEUE(V850_PIPELINE_WRITE);
}

static void _v850_pipe_write(cpu_ctx_t* cpu_ctx, v850ctx_t* ctx, _v850pipeline_t* pipe)
{
	HANDLE_PIPELINE_QUEUING();

	GPR_UNLOCK(pipe->writeback);
	if(pipe->writeback != 0) {
		ctx->gpr[pipe->writeback] = pipe->result;
	}
	REMOVE_PIPELINE_ENTRY(pipe);
}

static void _v850_pipe_done(cpu_ctx_t* cpu_ctx, v850ctx_t* ctx, _v850pipeline_t** pipes)
{
	int p;
	_v850pipeline_t* pipe;
	for(p = 0; p < V850_PIPELINE_LENGTH; p++) {
		if(pipes[p]->stage == V850_PIPELINE_DONE) {
			if(p != V850_PIPELINE_LENGTH - 1) {
				pipes[V850_PIPELINE_LENGTH] = pipes[p];
				memmove(pipes + p, pipes + p + 1, sizeof(_v850pipeline_t*) * (V850_PIPELINE_LENGTH - p));
			}
			pipes[V850_PIPELINE_LENGTH - 1]->stage = V850_PIPELINE_DONE;
		}
	}
	for(p = 0; p < V850_PIPELINE_LENGTH; p++) {
		if(pipes[p]->stage == V850_PIPELINE_DONE) {
			pipe = pipes[p];
			bzero(pipe, sizeof(_v850pipeline_t));
			pipe->stage = V850_PIPELINE_DONE;
			SET_PIPELINE_QUEUE(V850_PIPELINE_FETCH);
			if(pipe->inqueue == -1) {
				pipe->stageprog = 0;
				break;
			}
		}
	}
}

EMUCPU_EMULATE( v850 )
{
	int i;
	v850ctx_t* ctx = cpu_ctx->data;
	_v850pipeline_t** pipes = ctx->pipelining;
	for(cpu_ctx->cycles = cycles; cpu_ctx->cycles > 0; cpu_ctx->cycles--) {
		if(cpu_ctx->halt) {
			break;
		}
		_v850_pipe_done(cpu_ctx, ctx, pipes);
		if(ctx->pipequeue[V850_PIPELINE_WRITE] != NULL)
			_v850_pipe_write(cpu_ctx, ctx, ctx->pipequeue[V850_PIPELINE_WRITE]);
		if(ctx->pipequeue[V850_PIPELINE_MEM] != NULL)
			_v850_pipe_mem(cpu_ctx, ctx, ctx->pipequeue[V850_PIPELINE_MEM]);
		if(ctx->pipequeue[V850_PIPELINE_EXEC] != NULL)
			_v850_pipe_exec(cpu_ctx, ctx, ctx->pipequeue[V850_PIPELINE_EXEC]);
		if(ctx->pipequeue[V850_PIPELINE_DECODE] != NULL)
			_v850_pipe_decode(cpu_ctx, ctx, ctx->pipequeue[V850_PIPELINE_DECODE]);
		if(ctx->pipequeue[V850_PIPELINE_HAZARD] != NULL)
			_v850_pipe_hazard(cpu_ctx, ctx, ctx->pipequeue[V850_PIPELINE_HAZARD]);
		if(ctx->pipequeue[V850_PIPELINE_FETCH] != NULL)
			_v850_pipe_fetch(cpu_ctx, ctx, ctx->pipequeue[V850_PIPELINE_FETCH]);
	}
	return cycles - cpu_ctx->cycles;
}

EMUCPU_RESET( v850 )
{
	int i;
	v850ctx_t* ctx = cpu_ctx->data;
	_v850pipeline_t** pipes = ctx->pipelining;
	cpu_ctx->halt = 0;
	for(i = 0; i < 32; i++) {
		ctx->gpr[i] = 0;
		GPR_UNLOCK(i);
	}
	/* All lines are free */
	for(i = 0; i < V850_PIPELINE_LENGTH; i++) {
		pipes[i]->stage = V850_PIPELINE_DONE;
	}
	for(i = 0; i < V850_PIPELINE_ST_COUNT; i++)
		ctx->pipequeue[i] = NULL;
	ctx->branched    = 0;
	ctx->pc          = 0;
	ctx->V850_EIPC  &= ~0xFF000000;
	ctx->V850_EIPSW &= ~0xFFFFFF00;
	ctx->V850_FEPC  &= ~0xFF000000;
	ctx->V850_FEPSW &= ~0xFFFFFF00;
	ctx->V850_CTPC  &= ~0xFF000000;
	ctx->V850_CTPSW &= ~0xFFFFFF00;
	ctx->V850_DBPC  &= ~0xFF000000;
	ctx->V850_DBPSW &= ~0xFFFFFF00;
	ctx->V850_CTBP  &= ~0xFF000000;
	ctx->V850_ECR    = 0;
	ctx->V850_PSW    = V850_FLAG_MIRQA;
}

EMUCPU_SAVE( v850 )
{
	v850ctx_t* ctx = cpu_ctx->data;
	u8* outdata = calloc(sizeof(v850ctx_t), 1);
	memcpy(outdata, ctx, sizeof(v850ctx_t));
	return outdata;
}

EMUCPU_RESTORE( v850 )
{
	v850ctx_t* ctx = cpu_ctx->data;
	memcpy(ctx, data, sizeof(v850ctx_t));
}

EMUCPU_GET_INFO( v850 )
{
	v850ctx_t* ctx = cpu_ctx->data;
	switch(type) {
		case EMUCPU_INFO_TYPE_REG_COUNT:
			info->i = 65;
			break;
		case EMUCPU_INFO_TYPE_REG_SIZE:
			info->i = 32;
			break;
		case EMUCPU_INFO_TYPE_ENDIANNESS:
			info->i = 1;
			break;
		case EMUCPU_INFO_TYPE_NAME:
			strcpy(info->ptr, "V850");
			break;
		case EMUCPU_INFO_TYPE_FAMILY:
			strcpy(info->ptr, "NEC V850");
			break;
		case EMUCPU_INFO_TYPE_VERSION:
			strcpy(info->ptr, "1.0");
			break;
		case EMUCPU_INFO_TYPE_CREDITS:
			strcpy(info->ptr, "trap15 (Alex Marshall)");
			break;
		case EMUCPU_INFO_TYPE_REG_VALUE + 0:
			info->i = ctx->pc;
			break;
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 0:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 1:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 2:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 3:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 4:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 5:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 6:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 7:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 8:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 9:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 10:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 11:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 12:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 13:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 14:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 15:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 16:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 17:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 18:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 19:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 20:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 21:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 22:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 23:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 24:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 25:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 26:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 27:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 28:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 29:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 30:
		case EMUCPU_INFO_TYPE_REG_VALUE + 1 + 31:
			info->i = ctx->gpr[type - EMUCPU_INFO_TYPE_REG_VALUE - 1];
			break;
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 0:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 1:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 2:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 3:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 4:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 5:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 6:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 7:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 8:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 9:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 10:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 11:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 12:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 13:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 14:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 15:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 16:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 17:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 18:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 19:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 20:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 21:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 22:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 23:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 24:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 25:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 26:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 27:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 28:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 29:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 30:
		case EMUCPU_INFO_TYPE_REG_VALUE + 32 + 1 + 31:
			info->i = ctx->sysr[type - EMUCPU_INFO_TYPE_REG_VALUE - 32 - 1];
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 0:
			strcpy(info->ptr, "PC   ");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 0:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 1:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 2:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 3:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 4:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 5:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 6:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 7:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 8:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 9:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 10:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 11:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 12:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 13:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 14:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 15:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 16:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 17:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 18:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 19:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 20:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 21:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 22:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 23:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 24:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 25:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 26:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 27:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 28:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 29:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 30:
		case EMUCPU_INFO_TYPE_REG_NAME + 1 + 31:
			sprintf(info->ptr, "GPR%02d", type - EMUCPU_INFO_TYPE_REG_NAME - 1);
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 0:
			strcpy(info->ptr, "EIPC ");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 1:
			strcpy(info->ptr, "EIPSW");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 2:
			strcpy(info->ptr, "FEPC ");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 3:
			strcpy(info->ptr, "FEPSW");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 4:
			strcpy(info->ptr, "ECR  ");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 5:
			strcpy(info->ptr, "PSW  ");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 6:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 7:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 8:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 9:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 10:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 11:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 12:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 13:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 14:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 15:
			sprintf(info->ptr, "SR%02d ", type - EMUCPU_INFO_TYPE_REG_NAME - 1 - 32);
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 16:
			strcpy(info->ptr, "CTPC ");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 17:
			strcpy(info->ptr, "CTPSW");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 18:
			strcpy(info->ptr, "DBPC ");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 19:
			strcpy(info->ptr, "DBPSW");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 20:
			strcpy(info->ptr, "CTBP ");
			break;
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 21:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 22:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 23:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 24:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 25:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 26:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 27:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 28:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 29:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 30:
		case EMUCPU_INFO_TYPE_REG_NAME + 32 + 1 + 31:
			sprintf(info->ptr, "SR%02d ", type - EMUCPU_INFO_TYPE_REG_NAME - 1 - 32);
			break;
		default:
			return EMUCPU_INFO_RET_NO_HANDLE;
	}
	return EMUCPU_INFO_RET_SUCCESS;
}

EMUCPU_SET_INFO( v850 )
{
	v850ctx_t* ctx = cpu_ctx->data;
	return EMUCPU_INFO_RET_NO_HANDLE;
}

EMUCPU_INIT( v850 )
{
	int i;
	cpu_ctx_t* cpu_ctx;
	v850ctx_t* ctx;
	cpu_ctx = malloc(sizeof(cpu_ctx_t));
	if(cpu_ctx == NULL)
		return NULL;
	ctx = malloc(sizeof(v850ctx_t));
	if(ctx == NULL) {
		free(cpu_ctx);
		return NULL;
	}
	cpu_ctx->data = ctx;
	for(i = 0; i < V850_PIPELINE_LENGTH + 2; i++)
		ctx->pipelining[i] = malloc(sizeof(_v850pipeline_t));
	ctx->EMUCPU_READ_NAME( read32 ) = NULL;
	ctx->EMUCPU_WRITE_NAME(write32) = NULL;
	return cpu_ctx;
}

