/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef M68KOPS_H
#define M68KOPS_H

#include "emucpu.h"

#include "m68k.h"

#define M68KCATEGORY(cat)	INLINE void m68kcat_##cat(i8080ctx_t* ctx, u16 opcode)
#define M68KENTRY(cat)		{ m68kcat_##cat }

typedef struct {
	void (*execute)(m68kctx_t* ctx, u16 opcode);
} m68kops_t;

#if 0

BTST,		Ob(1111,0001,1100,0000), Ob(0000,0001,0000,0000)
BCHG,		Ob(1111,0001,1100,0000), Ob(0000,0001,0100,0000)
BCLR,		Ob(1111,0001,1100,0000), Ob(0000,0001,1000,0000)
BSET,		Ob(1111,0001,1100,0000), Ob(0000,0001,1100,0000)

ORI,		Ob(1111,1111,0000,0000), Ob(0000,0000,0000,0000)
ANDI,		Ob(1111,1111,0000,0000), Ob(0000,0010,0000,0000)
SUBI,		Ob(1111,1111,0000,0000), Ob(0000,0100,0000,0000)
ADDI,		Ob(1111,1111,0000,0000), Ob(0000,0110,0000,0000)
EORI,		Ob(1111,1111,0000,0000), Ob(0000,1010,0000,0000)
CMPI,		Ob(1111,1111,0000,0000), Ob(0000,1100,0000,0000)

BTST,		Ob(1111,1111,1100,0000), Ob(0000,1000,0000,0000)
BCHG,		Ob(1111,1111,1100,0000), Ob(0000,1000,0100,0000)
BCLR,		Ob(1111,1111,1100,0000), Ob(0000,1000,1000,0000)
BSET,		Ob(1111,1111,1100,0000), Ob(0000,1000,1100,0000)

MOVE,		Ob(1111,0000,0000,0000), Ob(0001,0000,0000,0000)
MOVE,		Ob(1111,0000,0000,0000), Ob(0010,0000,0000,0000)
MOVE,		Ob(1111,0000,0000,0000), Ob(0011,0000,0000,0000)

MOVEfSR,	Ob(1111,1111,1100,0000), Ob(0100,0000,1100,0000)
MOVEtCCR,	Ob(1111,1111,1100,0000), Ob(0100,0100,1100,0000)
MOVEtSR,	Ob(1111,1111,1100,0000), Ob(0100,0110,1100,0000)

NEGX,		Ob(1111,1111,1100,0000), Ob(0100,0000,0000,0000)
NEGX,		Ob(1111,1111,1100,0000), Ob(0100,0000,0100,0000)
NEGX,		Ob(1111,1111,1100,0000), Ob(0100,0000,1000,0000)
CLR,		Ob(1111,1111,1100,0000), Ob(0100,0010,0000,0000)
CLR,		Ob(1111,1111,1100,0000), Ob(0100,0010,0100,0000)
CLR,		Ob(1111,1111,1100,0000), Ob(0100,0010,1000,0000)
NEG,		Ob(1111,1111,1100,0000), Ob(0100,0100,0000,0000)
NEG,		Ob(1111,1111,1100,0000), Ob(0100,0100,0100,0000)
NEG,		Ob(1111,1111,1100,0000), Ob(0100,0100,1000,0000)
NOT,		Ob(1111,1111,1100,0000), Ob(0100,0110,0000,0000)
NOT,		Ob(1111,1111,1100,0000), Ob(0100,0110,0100,0000)
NOT,		Ob(1111,1111,1100,0000), Ob(0100,0110,1000,0000)

EXT,		Ob(1111,1111,1111,1000), Ob(0100,1000,1000,0000)
EXT,		Ob(1111,1111,1111,1000), Ob(0100,1000,1100,0000)
NBCD,		Ob(1111,1111,1100,0000), Ob(0100,1000,0000,0000)
SWAP,		Ob(1111,1111,1111,1000), Ob(0100,1000,0100,0000)
PEA,		Ob(1111,1111,1100,0000), Ob(0100,1000,0100,0000)

ILLEGAL0,	Ob(1111,1111,1111,1111), Ob(0100,1010,1111,1100)
TAS,		Ob(1111,1111,1100,0000), Ob(0100,1010,1100,0000)
TST,		Ob(1111,1111,1100,0000), Ob(0100,1010,0000,0000)
TST,		Ob(1111,1111,1100,0000), Ob(0100,1010,0100,0000)
TST,		Ob(1111,1111,1100,0000), Ob(0100,1010,1000,0000)

TRAP,		Ob(1111,1111,1111,0000), Ob(0100,1110,0100,0000)
LINK,		Ob(1111,1111,1111,1000), Ob(0100,1110,0101,0000)
UNLK,		Ob(1111,1111,1111,1000), Ob(0100,1110,0101,1000)
MOVEUSP,	Ob(1111,1111,1111,0000), Ob(0100,1110,0110,0000)

RESET,		Ob(1111,1111,1111,1111), Ob(0100,1110,0111,0000)
NOP,		Ob(1111,1111,1111,1111), Ob(0100,1110,0111,0001)
STOP,		Ob(1111,1111,1111,1111), Ob(0100,1110,0111,0010)
RTE,		Ob(1111,1111,1111,1111), Ob(0100,1110,0111,0011)
RTS,		Ob(1111,1111,1111,1111), Ob(0100,1110,0111,0101)
TRAPV,		Ob(1111,1111,1111,1111), Ob(0100,1110,0111,0110)
RTR,		Ob(1111,1111,1111,1111), Ob(0100,1110,0111,0111)

JSR,		Ob(1111,1111,1100,0000), Ob(0100,1110,1000,0000)
JMP,		Ob(1111,1111,1100,0000), Ob(0100,1110,1100,0000)
MOVEM,		Ob(1111,1011,1000,0000), Ob(0100,1000,1000,0000)
LEA,		Ob(1111,0001,1100,0000), Ob(0100,0001,1100,0000)
CHK,		Ob(1111,0001,1100,0000), Ob(0100,0001,1000,0000)

ADDQ,		Ob(1111,0001,1100,0000), Ob(0101,0000,0000,0000)
ADDQ,		Ob(1111,0001,1100,0000), Ob(0101,0000,0100,0000)
ADDQ,		Ob(1111,0001,1100,0000), Ob(0101,0000,1000,0000)
SUBQ,		Ob(1111,0001,1100,0000), Ob(0101,0001,0000,0000)
SUBQ,		Ob(1111,0001,1100,0000), Ob(0101,0001,0100,0000)
SUBQ,		Ob(1111,0001,1100,0000), Ob(0101,0001,1000,0000)
Scc,		Ob(1111,0000,1100,0000), Ob(0101,0000,1100,0000)

Bcc,		Ob(1111,0000,0000,0000), Ob(0110,0000,0000,0000)

MOVEQ,		Ob(1111,0000,0000,0000), Ob(0111,0000,0000,0000)

DIVU,		Ob(1111,0001,1100,0000), Ob(1000,0000,1100,0000)
DIVS,		Ob(1111,0001,1100,0000), Ob(1000,0001,1100,0000)
SBCD,		Ob(1111,0001,1111,0000), Ob(1000,0001,0000,0000)
OR,		Ob(1111,0001,1100,0000), Ob(1000,0000,0000,0000)
OR,		Ob(1111,0001,1100,0000), Ob(1000,0000,0100,0000)
OR,		Ob(1111,0001,1100,0000), Ob(1000,0000,1000,0000)
OR,		Ob(1111,0001,1100,0000), Ob(1000,0001,0100,0000)
OR,		Ob(1111,0001,1100,0000), Ob(1000,0001,1000,0000)
OR,		Ob(1111,0001,1111,0000), Ob(1000,0001,0011,0000)
OR,		Ob(1111,0001,1111,0000), Ob(1000,0001,0010,0000)
OR,		Ob(1111,0001,1111,0000), Ob(1000,0001,0001,0000)


static m68kops_t opTbl[] = {
	M68KENTRY(ORDIVSBCD),
	M68KENTRY(SUB),
	M68KENTRY(RESERVED),
	M68KENTRY(CMPEOR),
	M68KENTRY(ANDMULABCDEXG),
	M68KENTRY(ADD),
	M68KENTRY(RLWINM),
	M68KENTRY(COPINTRF),
}

#endif

#endif

