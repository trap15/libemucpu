/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010~2011 trap15 (Alex Marshall) <trap15@raidenii.net>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef _LIBEMU_V850_H
#define _LIBEMU_V850_H

#include "emucpu.h"

#define V850_ZR			gpr[ 0]
#define V850_AR			gpr[ 1]		/* Assembler reserved */
#define V850_SP			gpr[ 3]
#define V850_GP			gpr[ 4]		/* Global Pointer */
#define V850_TP			gpr[ 5]		/* Text Pointer */
#define V850_EP			gpr[30]		/* Element Pointer */
#define V850_LP			gpr[31]		/* Link Pointer */

#define V850_EIPC		sysr[ 0]	/* Interrupt status saving register */
#define V850_EIPSW		sysr[ 1]	/* Interrupt status saving register */
#define V850_FEPC		sysr[ 2]	/* NMI status saving register */
#define V850_FEPSW		sysr[ 3]	/* NMI status saving register */
#define V850_ECR		sysr[ 4]	/* Interrupt source register */
#define V850_PSW		sysr[ 5]	/* Program status word */
#define V850_CTPC		sysr[16]	/* CALLT execution status saving register */
#define V850_CTPSW		sysr[17]	/* CALLT execution status saving register */
#define V850_DBPC		sysr[18]	/* Exception/Debug trap status saving register */
#define V850_DBPSW		sysr[19]	/* Exception/Debug trap status saving register */
#define V850_CTBP		sysr[20]	/* CALLT base pointer */

#define V850_FLAG_ZERO		(1 << 0)
#define V850_FLAG_SIGN		(1 << 1)
#define V850_FLAG_OVFLW		(1 << 2)	/* Overflow */
#define V850_FLAG_CARRY		(1 << 3)
#define V850_FLAG_SAT		(1 << 4)	/* Saturated overflow */
#define V850_FLAG_MIRQA		(1 << 5)	/* Maskable IRQ acknowledgment */
#define V850_FLAG_EXCP		(1 << 6)	/* Exception processing in progress */
#define V850_FLAG_NMI		(1 << 7)	/* NMI servicing in progress */

#define V850_PIPELINE_FETCH		(0)	/* Instruction Fetch */
#define V850_PIPELINE_HAZARD		(1)	/* Hazard cycle-waster */
#define V850_PIPELINE_DECODE		(2)	/* Instruction Decode */
#define V850_PIPELINE_EXEC		(3)	/* Execution of ALU/mulitplier/barrel shifter */
#define V850_PIPELINE_MEM		(4)	/* Memory Access */
#define V850_PIPELINE_WRITE		(5)	/* Write back */
#define V850_PIPELINE_ST_COUNT		(6)	/* +1 the highest real stage */
#define V850_PIPELINE_DONE		(6)	/* Instruction complete. Remove from list */
#define V850_PIPELINE_LENGTH		(7)	/* Maximum instructions in line at once */


typedef struct {
	int		stage;
	int		stageprog;
	int		laststage;
	int		inqueue;
	u16		opcode;		/* IF */
	u16		opcode2;	/* IF */
	u32		pc;		/* IF */
	u32		oper[4];	/* EX */
	u32		result;		/* EX */
	u32		ea;		/* MEM */
	u32		mask;		/* MEM */ /* Write mask */
	int		dir;		/* MEM */ /* Direction the memory access goes (0: read 1: write) */
	int		writeback;	/* WB */ /* Which reg to write-back to */
} _v850pipeline_t;

typedef struct {
	u32			gpr[32];
	u32			pc;
	u32			sysr[32];
	int			branched;
	_v850pipeline_t*	pipequeue[V850_PIPELINE_LENGTH + 2];
	_v850pipeline_t*	pipelining[V850_PIPELINE_LENGTH + 2];
	s64			gprstall[32];	/* Holds pipeline register status */
						/* -2 means stall, -1 means no stall, */
						/* Anything else is a "carry over" value. */
	/* I/O */
	EMUCPU_READ_CB( read32,  32, 32);
	EMUCPU_WRITE_CB(write32, 32, 32);
} v850ctx_t;

EMUCPU_EMULATE( v850 );
EMUCPU_INTERRUPT( v850 );
EMUCPU_RESET( v850 );
EMUCPU_INIT( v850 );
EMUCPU_SAVE( v850 );
EMUCPU_RESTORE( v850 );
EMUCPU_GET_INFO( v850 );
EMUCPU_SET_INFO( v850 );
EMUCPU_DASM( v850 );

#endif

