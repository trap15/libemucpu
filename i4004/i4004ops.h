/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 The Lemon Man (Giuseppe)
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 
 
#ifndef I4004OPS_H
#define I4004OPS_H

#include "emucpu.h"

#include "i4004.h"

#define I4004OPCODE(op)			INLINE void i4004op_##op(i4004ctx_t* ctx, u8 opcode)
#define I4004OPCODE_RUN(op, ctx, code)	i4004op_##op(ctx, code)

#define GET_OPCODE_HI(op) 		(op >> 4) & 0xF
#define GET_OPCODE_LO(op)		(op >> 0) & 0xF

#define PIPELINE_FETCH8(ctx)		ctx->EMUCPU_READ_RUN(read8, ctx->PC++, 0xFF)

I4004OPCODE ( NOP )
{
	
}

I4004OPCODE ( JCN )
{
	if (CONDITION_TRUE(GET_OPCODE_LO(opcode))) {
		ctx->PC = (ctx->PC & 0xFF00) | PIPELINE_FETCH8(ctx);
	} else {
		ctx->PC++;
	}
}

I4004OPCODE ( FIM )
{
	ctx->GPR[GET_OPCODE_LO(opcode) & 0xE] = ctx->EMUCPU_READ_RUN(read8, PIPELINE_FETCH8(ctx), 0xFF);
}

I4004OPCODE ( FIN )
{
	ctx->GPR[GET_OPCODE_LO(opcode) & 0xE] = ctx->EMUCPU_READ_RUN(read8, ctx->GPR[0], 0xFF);
}

I4004OPCODE ( JIN )
{
	ctx->PC = ctx->GPR[GET_OPCODE_LO(opcode) & 0xE];
}

I4004OPCODE ( JUN )
{
	ctx->PC  = GET_OPCODE_LO(opcode) << 8;
	ctx->PC |= PIPELINE_FETCH8(ctx);
}

I4004OPCODE ( JMS )
{
	STACK_PUSH(ctx, ctx->PC);
	ctx->PC  = GET_OPCODE_LO(opcode) << 8;
	ctx->PC |= PIPELINE_FETCH8(ctx);
}

I4004OPCODE ( INC )
{
	ctx->GPR[GET_OPCODE_LO(opcode) & 0xE]++;
}

I4004OPCODE ( ISZ )
{
	if (!(++ctx->GPR[GET_OPCODE_LO(opcode) & 0xE])) {
		ctx->PC = PIPELINE_FETCH8(ctx);
	} else {
		ctx->PC++;
	}
}

I4004OPCODE ( ADD )
{
	u16 tmp = ctx->A + ctx->GPR[GET_OPCODE_LO(opcode)] + ctx->CARRY;
	if (tmp > 0xFF) {
		ctx->CARRY = 1;
	}
	ctx->A = (tmp & 0xFF);
}

I4004OPCODE ( SUB )
{
	u16 tmp = ctx->A - ctx->GPR[GET_OPCODE_LO(opcode)] - ctx->CARRY;
	// TODO : Check borrow
	ctx->A = (tmp & 0xFF);
}

I4004OPCODE ( LD )
{
	ctx->A = ctx->GPR[GET_OPCODE_LO(opcode)];
}

I4004OPCODE ( XCH )
{
	FAST_EXCHANGE(ctx->A, ctx->GPR[GET_OPCODE_LO(opcode)]);
}

I4004OPCODE ( BBL )
{
	ctx->A = GET_OPCODE_LO(opcode);
	ctx->PC = STACK_POP(ctx, ctx->PC);
}

I4004OPCODE ( LDM )
{
	ctx->A = GET_OPCODE_LO(opcode);
}

I4004OPCODE ( CLB )
{
	ctx->A = ctx->CARRY = 0x00;
}

I4004OPCODE ( CLC )
{
	ctx->CARRY = 0x00;
}

I4004OPCODE ( IAC )
{
	ctx->A++;
}

I4004OPCODE ( CMC )
{
	ctx->CARRY ^= 1;
}

I4004OPCODE ( RAL )
{
	ctx->CARRY = ctx->A & (1 << 7);
	ctx->A <<= 1;
	ctx->A |= ctx->CARRY;
}

I4004OPCODE ( RAR )
{
	ctx->CARRY = ctx->A & 1;
	ctx->A >>= 1;
	ctx->A |= (ctx->CARRY << 7);
}

I4004OPCODE ( TCC )
{
	// todo
}

I4004OPCODE ( DAC )
{
	ctx->A--;
}

I4004OPCODE ( TCS )
{
	// todo
}

I4004OPCODE ( STC )
{
	ctx->CARRY = 1;
}

I4004OPCODE ( DAA )
{
	// todo
}

I4004OPCODE ( KBP )
{
	switch (ctx->A) {
		case 0x00:
			ctx->A = 0x00; break;
		case 0x01:
			ctx->A = 0x01; break;
		case 0x02:
			ctx->A = 0x02; break;
		case 0x04:
			ctx->A = 0x03; break;
		case 0x08:
			ctx->A = 0x04; break;
		default:
			ctx->A = 0x0F; break;
	}
}

I4004OPCODE ( DCL )
{
	// todo
}

I4004OPCODE ( SRC )
{
	// todo
}

I4004OPCODE ( WRM )
{
	// todo
}

I4004OPCODE ( WMP )
{
	// todo
}

I4004OPCODE ( WRR )
{
	// todo
}

I4004OPCODE ( WPM )
{
	// todo
}

I4004OPCODE ( WR0 )
{
	// todo
}

I4004OPCODE ( WR1 )
{
	// todo
}

I4004OPCODE ( WR2 )
{
	// todo
}

I4004OPCODE ( WR3 )
{
	// todo
}

I4004OPCODE ( SBM )
{
	// todo
}

I4004OPCODE ( RDM )
{
	// todo
}

I4004OPCODE ( RDR )
{
	// todo
}

I4004OPCODE ( ADM )
{
	// todo
}

I4004OPCODE ( RD0 )
{
	// todo
}

I4004OPCODE ( RD1 )
{
	// todo
}

I4004OPCODE ( RD2 )
{
	// todo
}

I4004OPCODE ( RD3 )
{
	// todo
}

I4004OPCODE ( CMA )
{
	ctx->A = ~(ctx->A);
}

typedef struct {
	void (*execute)(i4004ctx_t* ctx, u8 opcode);
	int cycles;
} i4004ops_t;

#define I4004ENTRY(op, cyc)		{ i4004op_##op , cyc }

i4004ops_t opTbl [] = {
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( JCN, 2 ), // 0x10
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( JCN, 2 ),
	I4004ENTRY ( FIM, 2 ), // 0x20
	I4004ENTRY ( SRC, 1 ),
	I4004ENTRY ( FIM, 2 ),
	I4004ENTRY ( SRC, 1 ),
	I4004ENTRY ( FIM, 2 ),
	I4004ENTRY ( SRC, 1 ),	
	I4004ENTRY ( FIM, 2 ),
	I4004ENTRY ( SRC, 1 ),
	I4004ENTRY ( FIM, 2 ),
	I4004ENTRY ( SRC, 1 ),
	I4004ENTRY ( FIM, 2 ),
	I4004ENTRY ( SRC, 1 ),
	I4004ENTRY ( FIM, 2 ),
	I4004ENTRY ( SRC, 1 ),
	I4004ENTRY ( FIM, 2 ),
	I4004ENTRY ( SRC, 1 ),
	I4004ENTRY ( FIN, 2 ), // 0x30
	I4004ENTRY ( JIN, 1 ), 	
	I4004ENTRY ( FIN, 2 ),
	I4004ENTRY ( JIN, 1 ),
	I4004ENTRY ( FIN, 2 ),
	I4004ENTRY ( JIN, 1 ),
	I4004ENTRY ( FIN, 2 ),
	I4004ENTRY ( JIN, 1 ),
	I4004ENTRY ( FIN, 2 ),
	I4004ENTRY ( JIN, 1 ),
	I4004ENTRY ( FIN, 2 ),
	I4004ENTRY ( JIN, 1 ),
	I4004ENTRY ( FIN, 2 ),
	I4004ENTRY ( JIN, 1 ),
	I4004ENTRY ( FIN, 2 ),
	I4004ENTRY ( JIN, 1 ),	
	I4004ENTRY ( JUN, 2 ), // 0x40
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JUN, 2 ),
	I4004ENTRY ( JMS, 1 ), // 0x50
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( JMS, 1 ),
	I4004ENTRY ( INC, 1 ), // 0x60
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( INC, 1 ),
	I4004ENTRY ( ISZ, 2 ), // 0x70
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ISZ, 2 ),
	I4004ENTRY ( ADD, 1 ), // 0x80
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( ADD, 1 ),
	I4004ENTRY ( SUB, 1 ), // 0x90
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( SUB, 1 ),
	I4004ENTRY ( LD , 1 ),  // 0xA0
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( LD , 1 ),
	I4004ENTRY ( XCH, 1 ), // 0xB0
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( XCH, 1 ),
	I4004ENTRY ( BBL, 1 ), // 0xC0
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( BBL, 1 ),
	I4004ENTRY ( LDM, 1 ), // 0xD0
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ),
	I4004ENTRY ( LDM, 1 ), // 0xE0
	I4004ENTRY ( WRM, 1 ),	
	I4004ENTRY ( WMP, 1 ),
	I4004ENTRY ( WRR, 1 ),
	I4004ENTRY ( WPM, 1 ),
	I4004ENTRY ( WR0, 1 ),
	I4004ENTRY ( WR1, 1 ),
	I4004ENTRY ( WR2, 1 ),
	I4004ENTRY ( WR3, 1 ),
	I4004ENTRY ( SBM, 1 ),
	I4004ENTRY ( RDM, 1 ),
	I4004ENTRY ( RDR, 1 ),
	I4004ENTRY ( ADM, 1 ),
	I4004ENTRY ( RD0, 1 ),
	I4004ENTRY ( RD1, 1 ),
	I4004ENTRY ( RD2, 1 ),
	I4004ENTRY ( RD3, 1 ),
	I4004ENTRY ( CLB, 1 ), // 0xF0
	I4004ENTRY ( CLC, 1 ),
	I4004ENTRY ( IAC, 1 ),
	I4004ENTRY ( CMC, 1 ),
	I4004ENTRY ( CMA, 1 ),
	I4004ENTRY ( RAL, 1 ),
	I4004ENTRY ( RAR, 1 ),
	I4004ENTRY ( TCC, 1 ),
	I4004ENTRY ( DAC, 1 ),
	I4004ENTRY ( TCS, 1 ),
	I4004ENTRY ( STC, 1 ),
	I4004ENTRY ( DAA, 1 ),
	I4004ENTRY ( KBP, 1 ),
	I4004ENTRY ( DCL, 1 ),
	I4004ENTRY ( NOP, 1 ),
	I4004ENTRY ( NOP, 1 )
};
	
#endif

