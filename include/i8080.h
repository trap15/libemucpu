/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  Copyright (c)2010 TheLemonMan (Giuseppe)
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef _LIBEMU_I8080_H
#define _LIBEMU_I8080_H

#include "emucpu.h"

typedef struct {
	/* 8080 registers */
	union {
		struct {
#if LITTLE_ENDIAN
			u8	F;
			u8	A;
			u8	C;
			u8	B;
			u8	E;
			u8	D;
			u8	L;
			u8	H;
#else
			u8	A;
			u8	F;
			u8	B;
			u8	C;
			u8	D;
			u8	E;
			u8	H;
			u8	L;
#endif
		} r8;
		struct {
			u16	AF;
			u16	BC;
			u16	DE;
			u16	HL;
		} r16;
	} regs;
	/* Program Counter & Stack Pointer */
	u16	pc;
	u16	sp;
	/* Interrupts */
	u8	ie;
	u8	iPending;
	u8	imask;
	u8	i7on;
	/* I/O */
	EMUCPU_READ_CB( portIn,    8, 8);
	EMUCPU_WRITE_CB(portOut,   8, 8);
	EMUCPU_READ_CB( read8,     8, 16);
	EMUCPU_WRITE_CB(write8,    8, 16);
#if EMU8085
	/* This actually has only a single value and address width, but we 
	 * have to use standard word sizes for this */
	EMUCPU_READ_CB( serialIn,  8, 8);
	EMUCPU_WRITE_CB(serialOut, 8, 8);
#endif
} i8080ctx_t;

EMUCPU_EMULATE( i8080 );
EMUCPU_INTERRUPT( i8080 );
EMUCPU_RESET( i8080 );
EMUCPU_INIT( i8080 );
EMUCPU_SAVE( i8080 );
EMUCPU_RESTORE( i8080 );
EMUCPU_GET_INFO( i8080 );
EMUCPU_SET_INFO( i8080 );
EMUCPU_DASM( i8080 );

#endif

