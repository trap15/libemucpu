/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef _LIBEMU_SCORE_H
#define _LIBEMU_SCORE_H

#include "emucpu.h"

typedef struct {
	/* Program Counter & Stack Pointer */
	u32	pc;
	/* Registers */
	s32	r[69];
	/* Exception stuff */
	u32	savedPSR;
	u32	savedCCR;
	/* Debugging stuff */
	int	inDebug;
	int	iceEnabled;
	int	sjProbeEnabled;
	/* I/O */
	EMUCPU_READ_CB( read32,  32, 32);
	EMUCPU_WRITE_CB(write32, 32, 32);
} scorectx_t;

EMUCPU_EMULATE( score );
EMUCPU_INTERRUPT( score );
EMUCPU_RESET( score );
EMUCPU_INIT( score );
EMUCPU_SAVE( score );
EMUCPU_RESTORE( score );
EMUCPU_DUMP_REGS( score );

#endif

