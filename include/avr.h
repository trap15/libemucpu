/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef _LIBEMU_AVR_H
#define _LIBEMU_AVR_H

#include "emucpu.h"

typedef struct {
	/* Registers */
	u8	sreg;
	u16	sp;
	u16	pc;
	u8	intctrl;
	union {
		struct {
			u8	r0;
			u8	r1;
			u8	r2;
			u8	r3;
			u8	r4;
			u8	r5;
			u8	r6;
			u8	r7;
			u8	r8;
			u8	r9;
			u8	r10;
			u8	r11;
			u8	r12;
			u8	r13;
			u8	r14;
			u8	r15;
			u8	r16;
			u8	r17;
			u8	r18;
			u8	r19;
			u8	r20;
			u8	r21;
			u8	r22;
			u8	r23;
			u8	r24;
			u8	r25;
			union {
				struct {
#if LITTLE_ENDIAN
					u8	r26;
					u8	r27;
#else
					u8	r27;
					u8	r26;
#endif
				} b8;
				u16	b16;
			} X;
			union {
				struct {
#if LITTLE_ENDIAN
					u8	r28;
					u8	r29;
#else
					u8	r29;
					u8	r28;
#endif
				} b8;
				u16	b16;
			} Y;
			union {
				struct {
#if LITTLE_ENDIAN
					u8	r30;
					u8	r31;
#else
					u8	r31;
					u8	r30;
#endif
				} b8;
				u16	b16;
			} Z;
		} rs;
		u8	r[32];
	} r;
	/* I/O */
	EMUCPU_READ_CB( portAIn,   8, 8);
	EMUCPU_WRITE_CB(portAOut,  8, 8);
	EMUCPU_READ_CB( portBIn,   8, 8);
	EMUCPU_WRITE_CB(portBOut,  8, 8);
	EMUCPU_READ_CB( portCIn,   8, 8);
	EMUCPU_WRITE_CB(portCOut,  8, 8);
	EMUCPU_READ_CB( portDIn,   8, 8);
	EMUCPU_WRITE_CB(portDOut,  8, 8);
	EMUCPU_READ_CB( portEIn,   8, 8);
	EMUCPU_WRITE_CB(portEOut,  8, 8);
	EMUCPU_READ_CB( read16,   16, 16);
	EMUCPU_WRITE_CB(write16,  16, 16);
} avrctx_t;

EMUCPU_EMULATE( avr );
EMUCPU_INTERRUPT( avr );
EMUCPU_RESET( avr );
EMUCPU_INIT( avr );
EMUCPU_SAVE( avr );
EMUCPU_RESTORE( avr );
EMUCPU_GET_INFO( avr );
EMUCPU_SET_INFO( avr );
EMUCPU_DASM( avr );

#endif

