/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "emucpu.h"

#include "avr.h"
#include "avrops.h"

/* First CPU initialization takes a bit longer because we build the opTbl and
 * the cycTbl. */
static void (*opTbl[65536])(avrctx_t* ctx, u16 opcode);
static int cycTbl[65536];
static int ops_inited = 0;

EMUCPU_INTERRUPT( avr )
{
	avrctx_t* ctx = cpu_ctx->data;
	/* TODO: Implement. */
}

EMUCPU_DASM( avr )
{

}

EMUCPU_EMULATE( avr )
{
	u16 opcode;
	avrctx_t* ctx = cpu_ctx->data;
	for(cpu_ctx->cycles = cycles; cpu_ctx->cycles > 0; cpu_ctx->cycles -= cycTbl[opcode]) {
		if(cpu_ctx->halt)
			break;
		opcode = ctx->EMUCPU_READ_RUN(read16, ctx->pc, 0xFFFF);
		ctx->pc += 2;
		opTbl[opcode](ctx, opcode);
	}
	return cycles - cpu_ctx->cycles;
}

EMUCPU_RESET( avr )
{
	avrctx_t* ctx = cpu_ctx->data;
	/* TODO: Implement. */
}

EMUCPU_SAVE( avr )
{
	avrctx_t* ctx = cpu_ctx->data;
	u8* outdata = calloc(sizeof(avrctx_t), 1);
	memcpy(outdata, ctx, sizeof(avrctx_t));
	return outdata;
}

EMUCPU_RESTORE( avr )
{
	avrctx_t* ctx = cpu_ctx->data;
	memcpy(ctx, data, sizeof(avrctx_t));
}

EMUCPU_GET_INFO( avr )
{
	avrctx_t* ctx = cpu_ctx->data;
	return EMUCPU_INFO_RET_NO_HANDLE;
}

EMUCPU_SET_INFO( avr )
{
	avrctx_t* ctx = cpu_ctx->data;
	return EMUCPU_INFO_RET_NO_HANDLE;
}

EMUCPU_INIT( avr )
{
	cpu_ctx_t* cpu_ctx;
	avrctx_t* ctx;
	int i, l;
	if(!ops_inited) {
		for(i = 0; i <= Ob(1111,1111,1111,1111); i++) {
			for(l = 0; l < sizeof srcTbl; l++) {
				if((i & srcTbl[l].mask) == srcTbl[l].value) {
					opTbl[i] = srcTbl[l].execute;
					cycTbl[i] = srcTbl[l].cycles;
					break;
				}
			}
		}
		ops_inited = 1;
	}
	cpu_ctx = malloc(sizeof(cpu_ctx_t));
	if(cpu_ctx == NULL)
		return NULL;
	ctx = malloc(sizeof(avrctx_t));
	if(ctx == NULL) {
		free(cpu_ctx);
		return NULL;
	}
	ctx->EMUCPU_READ_NAME(portAIn) = NULL;
	ctx->EMUCPU_READ_NAME(portBIn) = NULL;
	ctx->EMUCPU_READ_NAME(portCIn) = NULL;
	ctx->EMUCPU_READ_NAME(portDIn) = NULL;
	ctx->EMUCPU_READ_NAME(portEIn) = NULL;
	ctx->EMUCPU_READ_NAME(read16)  = NULL;
	ctx->EMUCPU_WRITE_NAME(portAOut) = NULL;
	ctx->EMUCPU_WRITE_NAME(portBOut) = NULL;
	ctx->EMUCPU_WRITE_NAME(portCOut) = NULL;
	ctx->EMUCPU_WRITE_NAME(portDOut) = NULL;
	ctx->EMUCPU_WRITE_NAME(portEOut) = NULL;
	ctx->EMUCPU_WRITE_NAME(write16)  = NULL;
	cpu_ctx->data = ctx;
	return cpu_ctx;
}

