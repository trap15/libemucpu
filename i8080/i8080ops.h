/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  Copyright (c)2010 TheLemonMan (Giuseppe)
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef I8080OPS_H
#define I8080OPS_H

#include "emucpu.h"

#include "i8080.h"

#define I8080OPCODE(op)			INLINE void i8080op_##op(i8080ctx_t* ctx, cpu_ctx_t* cpu_ctx, u8 opcode)
#define I8080OPCODE_RUN(op, ctx, code)	i8080op_##op(ctx, cpu_ctx, code)

#define FLAG_SIGN	(1 << 7)
#define FLAG_ZERO	(1 << 6)
#define FLAG_ACARRY	(1 << 4)
#define FLAG_PARITY	(1 << 2)
#define FLAG_CARRY	(1 << 0)

INLINE void setFlags(i8080ctx_t* ctx, u32 reg, int fMask)
{
	ctx->regs.r8.F &= ~(fMask);
	if(fMask & FLAG_SIGN)
		if(reg & (1 << 7))
			ctx->regs.r8.F |= FLAG_SIGN;
	
	if(fMask & FLAG_ZERO)
		if(reg == 0)
			ctx->regs.r8.F |= FLAG_ZERO;
	
	if(fMask & FLAG_CARRY)
		if(reg & 0x100)
			ctx->regs.r8.F |= FLAG_CARRY;
	
	if(fMask & FLAG_ACARRY)
		if((ctx->regs.r8.A & 0xF) > (reg & 0xF))
			ctx->regs.r8.F |= FLAG_ACARRY;
	
	if(fMask & FLAG_PARITY)
		if(!(reg & 0x1))
			ctx->regs.r8.F |= FLAG_PARITY;
}

INLINE void writeReg(i8080ctx_t* ctx, u8 value, int index)
{
	switch (index) {
		case 0x00:
			ctx->regs.r8.B = value; break;
		case 0x01:
			ctx->regs.r8.C = value; break;
		case 0x02:
			ctx->regs.r8.D = value; break;
		case 0x03:
			ctx->regs.r8.E = value; break;
		case 0x04:
			ctx->regs.r8.H = value; break;
		case 0x05:
			ctx->regs.r8.L = value; break;
		case 0x06:
			ctx->EMUCPU_WRITE_RUN(write8, ctx->regs.r16.HL, value, 0xFF);
			break;
		case 0x07:
			ctx->regs.r8.A = value; break;
	}
}

INLINE u8 readReg(i8080ctx_t* ctx, int index)
{
	switch (index) {
		case 0x00:
			return ctx->regs.r8.B; break;
		case 0x01:
			return ctx->regs.r8.C; break;
		case 0x02:
			return ctx->regs.r8.D; break;
		case 0x03:
			return ctx->regs.r8.E; break;
		case 0x04:
			return ctx->regs.r8.H; break;
		case 0x05:
			return ctx->regs.r8.L; break;
		case 0x06:
			return ctx->EMUCPU_READ_RUN(read8, ctx->regs.r16.HL, 0xFF); break;
		case 0x07:
			return ctx->regs.r8.A; break;
	}
	return 0xFF;
}

I8080OPCODE( CMC )
{
	ctx->regs.r8.F ^= FLAG_CARRY;
}

I8080OPCODE( STC )
{
	ctx->regs.r8.F |= FLAG_CARRY;
}

I8080OPCODE( DAA )
{
	int top4 = (ctx->regs.r8.A >> 4) & 0xF;
	int bot4 = (ctx->regs.r8.A & 0xF);
	
	if((bot4 > 9) || (ctx->regs.r8.F & FLAG_ACARRY)) {
		setFlags(ctx, (u32)ctx->regs.r8.A + 6, FLAG_ZERO | FLAG_SIGN | FLAG_PARITY | FLAG_CARRY | FLAG_ACARRY);
		ctx->regs.r8.A += 6;
		top4 = (ctx->regs.r8.A >> 4) & 0xF;
		bot4 = (ctx->regs.r8.A & 0xF);
	}
	
	if ((top4 > 9) || (ctx->regs.r8.F & FLAG_CARRY)) {
		top4 += 6;
		ctx->regs.r8.A = (top4 << 4) | bot4;
	}
}

I8080OPCODE( INR )
{
	int dst = (opcode >> 3) & 0x7;
	u32 tmp = readReg(ctx, dst);
	tmp++;
	setFlags(ctx, tmp, FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY);
	writeReg(ctx, tmp, dst);
}

I8080OPCODE( DCR )
{
	int dst = (opcode >> 3) & 0x7;
	u32 tmp = readReg(ctx, dst);
	tmp--;
	setFlags(ctx, tmp, FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY);
	writeReg(ctx, tmp, dst);
}

I8080OPCODE( CMA )
{
	ctx->regs.r8.A = ~(ctx->regs.r8.A);
}

I8080OPCODE( NOP )
{
}

I8080OPCODE( MOV )
{
	int dst = (opcode >> 3) & 0x7;
	int src = (opcode & 0x7);
	u8 srcVal = readReg(ctx, src);
	writeReg(ctx, srcVal, dst);
}

I8080OPCODE( STAX )
{
	int src = (opcode >> 4) & 1;
	u16 addr = (!src) ? ctx->regs.r16.BC : ctx->regs.r16.DE;
	ctx->EMUCPU_WRITE_RUN(write8, addr, ctx->regs.r8.A, 0xFF);
}

I8080OPCODE( LDAX )
{
	int src = (opcode >> 4) & 1;
	u16 addr = (!src) ? ctx->regs.r16.BC : ctx->regs.r16.DE;
	ctx->regs.r8.A = ctx->EMUCPU_READ_RUN(read8, addr, 0xFF);
}

I8080OPCODE( ADD )
{
	int src = (opcode & 0x7);
	u32 srcVal = readReg(ctx, src);
	setFlags(ctx, (u32)ctx->regs.r8.A + srcVal, 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A += srcVal;
}

I8080OPCODE( ADC )
{
	int src = (opcode & 0x7);
	u32 srcVal = readReg(ctx, src);
	setFlags(ctx, (u32)ctx->regs.r8.A + srcVal + (ctx->regs.r8.F & FLAG_CARRY), 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A += srcVal + (ctx->regs.r8.F & FLAG_CARRY);
}

I8080OPCODE( SUB )
{
	int src = (opcode & 0x7);
	u32 srcVal = readReg(ctx, src);
	setFlags(ctx, (u32)ctx->regs.r8.A - srcVal, 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A -= srcVal;
}

I8080OPCODE( SBB )
{
	int src = (opcode & 0x7);
	u32 srcVal = readReg(ctx, src);
	setFlags(ctx, (u32)ctx->regs.r8.A - (srcVal + (ctx->regs.r8.F & FLAG_CARRY)), 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A -= (srcVal + (ctx->regs.r8.F & FLAG_CARRY));
}

I8080OPCODE( ANA )
{
	int src = (opcode & 0x7);
	u32 srcVal = readReg(ctx, src);
	setFlags(ctx, (u32)ctx->regs.r8.A & srcVal, 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A &= srcVal;
}

I8080OPCODE( XRA )
{
	int src = (opcode & 0x7);
	u32 srcVal = readReg(ctx, src);
	setFlags(ctx, (u32)ctx->regs.r8.A ^ srcVal, 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A ^= srcVal;
}

I8080OPCODE( ORA )
{
	int src = (opcode & 0x7);
	u32 srcVal = readReg(ctx, src);
	setFlags(ctx, (u32)ctx->regs.r8.A | srcVal, 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A |= srcVal;
}

I8080OPCODE( CMP )
{
	int src = (opcode & 0x7);
	u32 srcVal = readReg(ctx, src);
	setFlags(ctx, (u32)ctx->regs.r8.A - srcVal, 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
}

I8080OPCODE( RLC )
{
	ctx->regs.r8.F = ~(FLAG_CARRY);
	if(ctx->regs.r8.A & (1 << 7))
		ctx->regs.r8.F |= FLAG_CARRY;
	ctx->regs.r8.A = (ctx->regs.r8.A << 1) | (ctx->regs.r8.F & FLAG_CARRY);
}

I8080OPCODE( RRC )
{
	ctx->regs.r8.F = ~(FLAG_CARRY);
	if(ctx->regs.r8.A & 1)
		ctx->regs.r8.F |= FLAG_CARRY;
	ctx->regs.r8.A = (ctx->regs.r8.A >> 1) | ((ctx->regs.r8.F & FLAG_CARRY) << 7);
}

I8080OPCODE( RAL )
{
	u8 c = (ctx->regs.r8.A & (1 << 7));
	ctx->regs.r8.A = (ctx->regs.r8.A << 1) | (ctx->regs.r8.F & FLAG_CARRY);
	ctx->regs.r8.F = c;
}

I8080OPCODE( RAR )
{
	u8 c = (ctx->regs.r8.A & 1);
	ctx->regs.r8.A = (ctx->regs.r8.A >> 1) | ((ctx->regs.r8.F & FLAG_CARRY) << 7);
	ctx->regs.r8.F = c;
}

I8080OPCODE( PUSH )
{
	u16 val = 0;
	int src = (opcode >> 4) & 3;
	switch(src) {
		case 0x00:
			val = ctx->regs.r16.BC; break;
		case 0x01:
			val = ctx->regs.r16.DE; break;
		case 0x02:
			val = ctx->regs.r16.HL; break;
		case 0x03:
			val = ctx->regs.r16.AF; break;
		default:
			/* Shouldn't happen */
			break;
	}
	ctx->EMUCPU_WRITE_RUN(write8, --ctx->sp, val >> 8, 0xFF);
	ctx->EMUCPU_WRITE_RUN(write8, --ctx->sp, val, 0xFF);
}

I8080OPCODE( POP )
{
	int src = (opcode >> 4) & 3;
	u16 val = ctx->EMUCPU_READ_RUN(read8, --ctx->sp, 0xFF);
	val    |= ctx->EMUCPU_READ_RUN(read8, --ctx->sp, 0xFF) << 8;
	switch(src) {
		case 0x00:
			ctx->regs.r16.BC = val;
			break;
		case 0x01:
			ctx->regs.r16.DE = val;
			break;
		case 0x02:
			ctx->regs.r16.HL = val;
			break;
		case 0x03:
			ctx->regs.r16.AF = val;
			break;
		default:
			/* Shouldn't happen */
			break;			
	}
}

I8080OPCODE( DAD )
{
	int src = (opcode >> 4) & 3;
	u32 rVal = 0;
	switch(src) {
		case 0x00:
			rVal = ctx->regs.r16.BC; break;
		case 0x01:
			rVal = ctx->regs.r16.DE; break;
		case 0x02:
			rVal = ctx->regs.r16.HL; break;
		case 0x03:
			rVal = ctx->regs.r16.AF; break;			
	}
	setFlags(ctx, (u32)ctx->regs.r16.HL + rVal, FLAG_CARRY);
	ctx->regs.r16.HL += rVal;
}

I8080OPCODE( INX )
{
	int src = (opcode >> 4) & 3;
	switch(src) {
		case 0x00:
			ctx->regs.r16.BC++;
			break;
		case 0x01:
			ctx->regs.r16.DE++;
			break;
		case 0x02:
			ctx->regs.r16.HL++;
			break;
		case 0x03:
			ctx->sp++; 
			break;
		default:
			/* Shouldn't happen */
			break;
	}
}

I8080OPCODE( DCX )
{
	int src = (opcode >> 4) & 3;
	switch(src) {
		case 0x00:
			ctx->regs.r16.BC--;
			break;
		case 0x01:
			ctx->regs.r16.DE--;
			break;
		case 0x02:
			ctx->regs.r16.HL--;
			break;
		case 0x03:
			ctx->sp--; 
			break;
		default:
			/* Shouldn't happen */
			break;			
	}
}

I8080OPCODE( XCHG )
{
	/* Some leet haxx by booto */
	if(ctx->regs.r16.DE == ctx->regs.r16.HL)
		return;
	FAST_EXCHANGE(ctx->regs.r16.DE, ctx->regs.r16.HL);
}

I8080OPCODE( XTHL )
{
	ctx->regs.r8.H = ctx->EMUCPU_READ_RUN(read8, ctx->sp, 0xFF);
	ctx->regs.r8.L = ctx->EMUCPU_READ_RUN(read8, ctx->sp + 1, 0xFF);
}

I8080OPCODE( SPHL )
{
	ctx->sp = ctx->regs.r16.HL;
}

I8080OPCODE( LXI )
{
	u16 val;
	int dst = (opcode >> 4) & 3;
	val = ctx->EMUCPU_READ_RUN(read8, ctx->pc, 0xFF) | (ctx->EMUCPU_READ_RUN(read8, ctx->pc + 1, 0xFF) << 8);
	ctx->pc += 2;
	switch(dst) {
		case 0x00:
			ctx->regs.r16.BC = val;
			break;
		case 0x01:
			ctx->regs.r16.DE = val;
			break;
		case 0x02:
			ctx->regs.r16.HL = val;
			break;
		case 0x03:
			ctx->sp = val;
			break;			
		default:
			/* Shouldn't happen */
			break;			
	}
}

I8080OPCODE( MVI )
{
	int dst = (opcode >> 3) & 0x7;
	writeReg(ctx, ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF), dst);
}

I8080OPCODE( ADI )
{
	u32 imm = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	setFlags(ctx, (u32)ctx->regs.r8.A + imm,
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A += imm;
}

I8080OPCODE( ACI )
{
	u32 imm = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	setFlags(ctx, (u32)ctx->regs.r8.A + imm + (ctx->regs.r8.F & FLAG_CARRY), 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A += (imm + (ctx->regs.r8.F & FLAG_CARRY));
}

I8080OPCODE( SUI )
{
	u32 imm = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	setFlags(ctx, (u32)ctx->regs.r8.A - imm, 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A -= imm;
}

I8080OPCODE( SBI )
{
	u32 imm = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	setFlags(ctx, (u32)ctx->regs.r8.A - (imm + (ctx->regs.r8.F & FLAG_CARRY)), 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A -= (imm + (ctx->regs.r8.F & FLAG_CARRY));
}

I8080OPCODE( ANI )
{
	u32 imm = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	setFlags(ctx, (u32)ctx->regs.r8.A & imm, 
		 FLAG_SIGN | FLAG_ZERO | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A &= imm;
}

I8080OPCODE( XRI )
{
	u32 imm = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	setFlags(ctx, (u32)ctx->regs.r8.A ^ imm, 
		 FLAG_SIGN | FLAG_ZERO | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A ^= imm;
}

I8080OPCODE( ORI )
{
	u32 imm = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	setFlags(ctx, (u32)ctx->regs.r8.A | imm, 
		 FLAG_SIGN | FLAG_ZERO | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A |= imm;
}

I8080OPCODE( CPI )
{
	u32 imm = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	setFlags(ctx, (u32)ctx->regs.r8.A - imm, 
		 FLAG_SIGN | FLAG_ZERO | FLAG_ACARRY | FLAG_PARITY | FLAG_CARRY);
	ctx->regs.r8.A -= imm;
}

I8080OPCODE( STA )
{
	u16 addr = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	addr    |= ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF) << 8;
	ctx->EMUCPU_WRITE_RUN(write8, addr, ctx->regs.r8.A, 0xFF);
}

I8080OPCODE( LDA )
{
	u16 addr = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	addr    |= ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF) << 8;
	ctx->regs.r8.A = ctx->EMUCPU_READ_RUN(read8, addr, 0xFF);
}

I8080OPCODE( SHLD )
{
	u16 addr = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	addr    |= ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF) << 8;
	ctx->EMUCPU_WRITE_RUN(write8, addr,     ctx->regs.r8.H, 0xFF);
	ctx->EMUCPU_WRITE_RUN(write8, addr + 1, ctx->regs.r8.L, 0xFF);
}

I8080OPCODE( LHLD )
{
	u16 addr = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	addr    |= ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF) << 8;
	ctx->regs.r8.H = ctx->EMUCPU_READ_RUN(read8, addr, 0xFF);
	ctx->regs.r8.L = ctx->EMUCPU_READ_RUN(read8, addr + 1, 0xFF);
}

I8080OPCODE( PCHL )
{
	ctx->pc = ctx->regs.r16.HL;
}

I8080OPCODE( JMP )
{
	u16 pc = ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF);
	pc    |= ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF) << 8;
	ctx->pc = pc;
}

I8080OPCODE( JC )
{
	if(ctx->regs.r8.F & FLAG_CARRY)
		I8080OPCODE_RUN(JMP, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( JNC )
{
	if(!(ctx->regs.r8.F & FLAG_CARRY))
		I8080OPCODE_RUN(JMP, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( JZ )
{
	if(ctx->regs.r8.F & FLAG_ZERO)
		I8080OPCODE_RUN(JMP, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( JNZ )
{
	if(!(ctx->regs.r8.F & FLAG_ZERO))
		I8080OPCODE_RUN(JMP, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( JM )
{
	if(ctx->regs.r8.F & FLAG_SIGN)
		I8080OPCODE_RUN(JMP, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( JP )
{
	if(!(ctx->regs.r8.F & FLAG_SIGN))
		I8080OPCODE_RUN(JMP, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( JPE )
{
	if(ctx->regs.r8.F & FLAG_PARITY)
		I8080OPCODE_RUN(JMP, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( JPO )
{
	if(!(ctx->regs.r8.F & FLAG_PARITY))
		I8080OPCODE_RUN(JMP, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( CALL )
{
	/* Simple hack so I don't have to put it in all the other call
	 * handlers. compensated in the cycles table */
	cpu_ctx->cycles -= 6;
	ctx->EMUCPU_WRITE_RUN(write8, --ctx->sp, ctx->pc >> 8, 0xFF);
	ctx->EMUCPU_WRITE_RUN(write8, --ctx->sp, ctx->pc, 0xFF);
	I8080OPCODE_RUN(JMP, ctx, opcode);
}

I8080OPCODE( CC )
{
	if(ctx->regs.r8.F & FLAG_CARRY)
		I8080OPCODE_RUN(CALL, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( CNC )
{
	if(!(ctx->regs.r8.F & FLAG_CARRY))
		I8080OPCODE_RUN(CALL, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( CZ )
{
	if(ctx->regs.r8.F & FLAG_ZERO)
		I8080OPCODE_RUN(CALL, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( CNZ )
{
	if(!(ctx->regs.r8.F & FLAG_ZERO))
		I8080OPCODE_RUN(CALL, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( CM )
{
	if(ctx->regs.r8.F & FLAG_SIGN)
		I8080OPCODE_RUN(CALL, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( CP )
{
	if(!(ctx->regs.r8.F & FLAG_SIGN))
		I8080OPCODE_RUN(CALL, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( CPE )
{
	if(ctx->regs.r8.F & FLAG_PARITY)
		I8080OPCODE_RUN(CALL, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( CPO )
{
	if(!(ctx->regs.r8.F & FLAG_PARITY))
		I8080OPCODE_RUN(CALL, ctx, opcode);
	else
		ctx->pc += 2;
}

I8080OPCODE( RET )
{
	/* Simple hack so I don't have to put it in all the other ret
	 * handlers. compensated in the cycles table */
	cpu_ctx->cycles -= 6;
	ctx->pc  = ctx->EMUCPU_READ_RUN(read8, ctx->sp++, 0xFF);
	ctx->pc |= ctx->EMUCPU_READ_RUN(read8, ctx->sp++, 0xFF) << 8;
}

I8080OPCODE( RC )
{
	if(ctx->regs.r8.F & FLAG_CARRY)
		I8080OPCODE_RUN(RET, ctx, opcode);
}

I8080OPCODE( RNC )
{
	if(!(ctx->regs.r8.F & FLAG_CARRY))
		I8080OPCODE_RUN(RET, ctx, opcode);
}

I8080OPCODE( RZ )
{
	if(ctx->regs.r8.F & FLAG_ZERO)
		I8080OPCODE_RUN(RET, ctx, opcode);
}

I8080OPCODE( RNZ )
{
	if(!(ctx->regs.r8.F & FLAG_ZERO))
		I8080OPCODE_RUN(RET, ctx, opcode);
}

I8080OPCODE( RM )
{
	if(ctx->regs.r8.F & FLAG_SIGN)
		I8080OPCODE_RUN(RET, ctx, opcode);
}

I8080OPCODE( RP )
{
	if(!(ctx->regs.r8.F & FLAG_SIGN))
		I8080OPCODE_RUN(RET, ctx, opcode);
}

I8080OPCODE( RPE )
{
	if(ctx->regs.r8.F & FLAG_PARITY)
		I8080OPCODE_RUN(RET, ctx, opcode);
}

I8080OPCODE( RPO )
{
	if(!(ctx->regs.r8.F & FLAG_PARITY))
		I8080OPCODE_RUN(RET, ctx, opcode);
}

I8080OPCODE( RST )
{
	ctx->EMUCPU_WRITE_RUN(write8, --ctx->sp, ctx->pc >> 8, 0xFF);
	ctx->EMUCPU_WRITE_RUN(write8, --ctx->sp, ctx->pc, 0xFF);
	ctx->pc = opcode & 0x38;
}

I8080OPCODE( EI )
{
	ctx->ie = 1;
}

I8080OPCODE( DI )
{
	ctx->ie = 0;
}

I8080OPCODE( IN )
{
	ctx->regs.r8.A = ctx->EMUCPU_READ_RUN(portIn, ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF), 0xFF);
}

I8080OPCODE( OUT )
{
	ctx->EMUCPU_WRITE_RUN(portOut, ctx->EMUCPU_READ_RUN(read8, ctx->pc++, 0xFF), ctx->regs.r8.A, 0xFF);
}

I8080OPCODE( HLT )
{
	cpu_ctx->halt = 1;
}

#if EMU8085
I8080OPCODE( SIM )
{
	if(ctx->regs.r8.A & 8)
		ctx->imask = ctx->regs.r8.A & 7;
	if(ctx->regs.r8.A & 16)
		ctx->i7on = 0;
	if(ctx->regs.r8.A & 64)
		ctx->EMUCPU_WRITE_RUN(serialOut, 0, (ctx->regs.r8.A & 128) ? 1 : 0);
}

I8080OPCODE( RIM )
{
	ctx->regs.r8.A  = ctx->imask;
	ctx->regs.r8.A |= ctx->ie << 3;
	ctx->regs.r8.A |= ctx->iPending << 4;
	ctx->regs.r8.A |= ctx->EMUCPU_READ_RUN(serialIn, 0) << 7;
}
#endif

typedef struct 
{
	void (*execute)(i8080ctx_t* ctx, cpu_ctx_t* cpu_ctx, u8 opcode);
	int cycles;
} i8080ops_t;

#define I8080ENTRY(op, cyc)		{ i8080op_##op , cyc }
static i8080ops_t opTbl[0x100] = {
	I8080ENTRY(NOP,   4 ),
	I8080ENTRY(LXI,  10 ),
	I8080ENTRY(STAX,  7 ),
	I8080ENTRY(INX,   5 ),
	I8080ENTRY(INR,   5 ),
	I8080ENTRY(DCR,   5 ),
	I8080ENTRY(MVI,   7 ),
	I8080ENTRY(RLC,   4 ),
	I8080ENTRY(NOP,   4 ),
	I8080ENTRY(DAD,  10 ),
	I8080ENTRY(LDAX,  7 ), 
	I8080ENTRY(DCX,   5 ),
	I8080ENTRY(INR,   5 ),
	I8080ENTRY(DCR,   5 ),
	I8080ENTRY(MVI,   7 ),
	I8080ENTRY(RRC,   4 ), /* 0x10 */
	I8080ENTRY(NOP,   4 ),
	I8080ENTRY(LXI,  10 ),
	I8080ENTRY(STAX,  7 ),
	I8080ENTRY(INX,   5 ),
	I8080ENTRY(INR,   5 ),
	I8080ENTRY(DCR,   5 ),
	I8080ENTRY(MVI,   7 ),
	I8080ENTRY(RAL,   4 ),
	I8080ENTRY(NOP,   4 ),
	I8080ENTRY(DAD,  10 ),
	I8080ENTRY(LDAX,  7 ),
	I8080ENTRY(DCX,   5 ),
	I8080ENTRY(INR,   5 ),
	I8080ENTRY(DCR,   5 ),
	I8080ENTRY(MVI,   7 ),
	I8080ENTRY(RAR,   4 ),
#if EMU8085
	I8080ENTRY(RIM,   4 ), /* 0x20 */
#else
	I8080ENTRY(NOP,   4 ), /* 0x20 */
#endif
	I8080ENTRY(LXI,  10 ),
	I8080ENTRY(SHLD, 16 ),
	I8080ENTRY(INX,   5 ),
	I8080ENTRY(INR,   5 ),
	I8080ENTRY(DCR,   5 ),
	I8080ENTRY(MVI,   7 ),
	I8080ENTRY(DAA,   4 ),
	I8080ENTRY(NOP,   4 ),
	I8080ENTRY(DAD,  10 ),
	I8080ENTRY(LHLD, 16 ),
	I8080ENTRY(DCX,   5 ),
	I8080ENTRY(INR,   5 ),
	I8080ENTRY(DCR,   5 ),
	I8080ENTRY(MVI,   7 ),
	I8080ENTRY(CMA,   4 ),
#if EMU8085
	I8080ENTRY(SIM,   4 ), /* 0x30 */
#else
	I8080ENTRY(NOP,   4 ), /* 0x30 */
#endif
	I8080ENTRY(LXI,  10 ),
	I8080ENTRY(STA,  13 ),
	I8080ENTRY(INX,   5 ),
	I8080ENTRY(INR,  10 ),
	I8080ENTRY(DCR,  10 ),
	I8080ENTRY(MVI,  10 ),
	I8080ENTRY(STC,   4 ),
	I8080ENTRY(NOP,   4 ),
	I8080ENTRY(DAD,  10 ),
	I8080ENTRY(LDA,  13 ),
	I8080ENTRY(DCX,   5 ),
	I8080ENTRY(INR,   5 ),
	I8080ENTRY(DCR,   5 ),
	I8080ENTRY(MVI,   7 ),
	I8080ENTRY(CMC,   4 ),
	I8080ENTRY(MOV,   5 ), /* 0x40 */
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   7 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   7 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ), /* 0x50 */
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   7 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   7 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ), /* 0x60 */
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   7 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   7 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ), /* 0x70 */
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(HLT,   7 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(MOV,   7 ),
	I8080ENTRY(MOV,   5 ),
	I8080ENTRY(ADD,   4 ), /* 0x80 */
	I8080ENTRY(ADD,   4 ),
	I8080ENTRY(ADD,   4 ),
	I8080ENTRY(ADD,   4 ),
	I8080ENTRY(ADD,   4 ),
	I8080ENTRY(ADD,   4 ),
	I8080ENTRY(ADD,   7 ),
	I8080ENTRY(ADD,   4 ),
	I8080ENTRY(ADC,   4 ),
	I8080ENTRY(ADC,   4 ),
	I8080ENTRY(ADC,   4 ),
	I8080ENTRY(ADC,   4 ),
	I8080ENTRY(ADC,   4 ),
	I8080ENTRY(ADC,   4 ),
	I8080ENTRY(ADC,   7 ),
	I8080ENTRY(ADC,   4 ),
	I8080ENTRY(SUB,   4 ),
	I8080ENTRY(SUB,   4 ),
	I8080ENTRY(SUB,   4 ),
	I8080ENTRY(SUB,   4 ),
	I8080ENTRY(SUB,   4 ),
	I8080ENTRY(SUB,   4 ),
	I8080ENTRY(SUB,   7 ),
	I8080ENTRY(SUB,   4 ),
	I8080ENTRY(SBB,   4 ),
	I8080ENTRY(SBB,   4 ),
	I8080ENTRY(SBB,   4 ),
	I8080ENTRY(SBB,   4 ),
	I8080ENTRY(SBB,   4 ),
	I8080ENTRY(SBB,   4 ),
	I8080ENTRY(SBB,   7 ),
	I8080ENTRY(SBB,   4 ),
	I8080ENTRY(ANA,   4 ),
	I8080ENTRY(ANA,   4 ),
	I8080ENTRY(ANA,   4 ),
	I8080ENTRY(ANA,   4 ),
	I8080ENTRY(ANA,   4 ),
	I8080ENTRY(ANA,   4 ),
	I8080ENTRY(ANA,   7 ),
	I8080ENTRY(ANA,   4 ),
	I8080ENTRY(XRA,   4 ),
	I8080ENTRY(XRA,   4 ),
	I8080ENTRY(XRA,   4 ),
	I8080ENTRY(XRA,   4 ),
	I8080ENTRY(XRA,   4 ),
	I8080ENTRY(XRA,   4 ),
	I8080ENTRY(XRA,   7 ),
	I8080ENTRY(XRA,   4 ),
	I8080ENTRY(ORA,   4 ),
	I8080ENTRY(ORA,   4 ),
	I8080ENTRY(ORA,   4 ),
	I8080ENTRY(ORA,   4 ),
	I8080ENTRY(ORA,   4 ),
	I8080ENTRY(ORA,   4 ),
	I8080ENTRY(ORA,   7 ),
	I8080ENTRY(ORA,   4 ),
	I8080ENTRY(CMP,   4 ),
	I8080ENTRY(CMP,   4 ),
	I8080ENTRY(CMP,   4 ),
	I8080ENTRY(CMP,   4 ),
	I8080ENTRY(CMP,   4 ),
	I8080ENTRY(CMP,   4 ),
	I8080ENTRY(CMP,   7 ),
	I8080ENTRY(CMP,   4 ),
	I8080ENTRY(RNZ,   5 ),
	I8080ENTRY(POP,  10 ),
	I8080ENTRY(JNZ,  10 ),
	I8080ENTRY(JMP,  10 ),
	I8080ENTRY(CNZ,  11 ),
	I8080ENTRY(PUSH, 11 ),
	I8080ENTRY(ADI,   7 ),
	I8080ENTRY(RST,  11 ),
	I8080ENTRY(RZ,    5 ),
	I8080ENTRY(RET,   4 ),
	I8080ENTRY(JZ,   10 ),
	I8080ENTRY(JMP,  10 ),
	I8080ENTRY(CZ,   11 ),
	I8080ENTRY(CALL, 11 ),
	I8080ENTRY(ACI,   7 ),
	I8080ENTRY(RST,  11 ),
	I8080ENTRY(RNC,   5 ),
	I8080ENTRY(POP,  10 ),
	I8080ENTRY(JNC,  10 ),
	I8080ENTRY(OUT,  10 ),
	I8080ENTRY(CNC,  11 ),
	I8080ENTRY(PUSH, 11 ),
	I8080ENTRY(SUI,   7 ),
	I8080ENTRY(RST,  11 ),
	I8080ENTRY(RC,    5 ),
	I8080ENTRY(RET,   4 ),
	I8080ENTRY(JC,   10 ),
	I8080ENTRY(IN,   10 ),
	I8080ENTRY(CC,   11 ),
	I8080ENTRY(CALL, 11 ),
	I8080ENTRY(SBI,   7 ),
	I8080ENTRY(RST,  11 ),
	I8080ENTRY(RPO,   5 ),
	I8080ENTRY(POP,  10 ),
	I8080ENTRY(JPO,  10 ),
	I8080ENTRY(XTHL, 18 ),
	I8080ENTRY(CPO,  11 ),
	I8080ENTRY(PUSH, 11 ),
	I8080ENTRY(ANI,   7 ),
	I8080ENTRY(RST,  11 ),
	I8080ENTRY(RPE,   5 ),
	I8080ENTRY(PCHL,  5 ),
	I8080ENTRY(JPE,  10 ),
	I8080ENTRY(XCHG,  5 ),
	I8080ENTRY(CPE,  11 ),
	I8080ENTRY(CALL, 11 ),
	I8080ENTRY(XRI,   7 ),
	I8080ENTRY(RST,  11 ),
	I8080ENTRY(RP,    5 ),
	I8080ENTRY(POP,  10 ),
	I8080ENTRY(JP,   10 ),
	I8080ENTRY(DI,    4 ),
	I8080ENTRY(CP,   11 ),
	I8080ENTRY(PUSH, 11 ),
	I8080ENTRY(ORI,   7 ),
	I8080ENTRY(RST,  11 ),
	I8080ENTRY(RM,    5 ),
	I8080ENTRY(SPHL,  5 ),
	I8080ENTRY(JM,   10 ),
	I8080ENTRY(EI,    4 ),
	I8080ENTRY(CM,   11 ),
	I8080ENTRY(CALL, 11 ),
	I8080ENTRY(CPI,   7 ),
	I8080ENTRY(RST,  11 ),
};

#endif

