/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 TheLemonMan (Giuseppe)
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef _LIBEMU_I4004_H
#define _LIBEMU_I4004_H

#include "emucpu.h"

typedef struct {
	/* The accumulator and his very own carry & test registers */
	u8 A;
	u8 CARRY;
	u8 TEST;
	/* 16 4-bit GPR, multiplexed into 12bit registers. */
	u8 GPR[0x10];
	/* The famous PC! */
	u16 PC;
	/* And last but not least teh memory accessors */
	EMUCPU_READ_CB (read8,     8, 16);
	EMUCPU_WRITE_CB(write8,    8, 16);	
} i4004ctx_t;

EMUCPU_EMULATE( i4004 );
EMUCPU_INTERRUPT( i4004 );
EMUCPU_RESET( i4004 );
EMUCPU_INIT( i4004 );
EMUCPU_SAVE( i4004 );
EMUCPU_RESTORE( i4004 );
EMUCPU_GET_INFO( i4004 );
EMUCPU_SET_INFO( i4004 );
EMUCPU_DASM( i4004 );

#endif

