/*
 *  libemucpu -- a CPU emulator collection with a unified API.
 *  Copyright (c)2010 trap15 (Alex Marshall) <SquidMan72@gmail.com>
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */ 

#ifndef _LIBEMU_M68K_H
#define _LIBEMU_M68K_H

#include "emucpu.h"

typedef struct {
	u32	pc;
	/* I/O */
	/* The address size is only 24 bits, but we have to work in 8, 16, 32,
	 * or 64 bits. */
	EMUCPU_READ_CB( read32,    32, 32);
	EMUCPU_WRITE_CB(write32,   32, 32);
} m68kctx_t;

EMUCPU_EMULATE( m68k );
EMUCPU_INTERRUPT( m68k );
EMUCPU_RESET( m68k );
EMUCPU_INIT( m68k );
EMUCPU_SAVE( m68k );
EMUCPU_RESTORE( m68k );
EMUCPU_GET_INFO( m68k );
EMUCPU_SET_INFO( m68k );
EMUCPU_DASM( m68k );

#endif

